#!/usr/bin/make

UTILDIR=utilities

SRCDIR=fit_logs
POS_FITS=$(wildcard $(SRCDIR)/material.????.?.fit)
NEG_FITS=$(wildcard $(SRCDIR)/material.????.?.neg.fit)
BOTH_FITS=$(POS_FITS) $(NEG_FITS)

PROGDIR=charts/progress
PROGRESS=$(addprefix $(PROGDIR)/,$(notdir $(BOTH_FITS:.fit=.svg)))

SCATDIR=charts/scatter
SCATTER=$(addprefix $(SCATDIR)/,$(notdir $(BOTH_FITS:.fit=.png)))

SHACDIR=osl_code/chromatic
SHACHRO=$(addprefix $(SHACDIR)/,$(notdir $(POS_FITS:.fit=.osl)))
SHAADIR=osl_code/achromatic
SHAACHR=$(addprefix $(SHAADIR)/,$(notdir $(POS_FITS:.fit=.osl)))

COMPDIR=charts/compare
NUMBERS=$(shell ls -1 originals/material.????.tsv | sed -e 's/.*\.\([0-9][0-9][0-9][0-9]\)\..*/\1/' | sort -u)
COMPPOS=$(addsuffix .svg,$(addprefix $(COMPDIR)/convex/material.,$(NUMBERS)))
COMPNEG=$(addsuffix .svg,$(addprefix $(COMPDIR)/both/material.,$(NUMBERS)))

CHRODIR=charts/chroma
CHROPOS=$(subst $(SRCDIR),$(CHRODIR)/convex,$(POS_FITS:.fit=.svg))
CHRONEG=$(subst $(SRCDIR),$(CHRODIR)/both,$(NEG_FITS:.neg.fit=.svg))

all:	charts shaders
charts:		chart_progress chart_scatter chart_compare chart_chroma
chart_progress:	$(PROGRESS)
chart_scatter:	$(SCATTER)
chart_compare:	$(COMPPOS) $(COMPNEG)
chart_chroma:	$(CHROPOS) $(CHRONEG)
shaders:	$(SHACHRO) $(SHAACHR)

$(PROGDIR)/%.svg: $(SRCDIR)/%.fit
	perl utilities/fit_files/fit2progress.perl $< | gnuplot > $@

$(SCATDIR)/%.png: $(SRCDIR)/%.fit
	perl utilities/fit_files/fit2scatter.perl $< | gnuplot > $@

$(SHACDIR)/%.osl: $(SRCDIR)/%.fit
	perl utilities/fit_files/fit2shader.perl $< > $@
$(SHAADIR)/%.osl: $(SRCDIR)/%.fit
	perl utilities/fit_files/fit2shader.perl $< achromatic > $@

$(COMPDIR)/convex/material.%.svg: originals/material.%.tsv  $(SRCDIR)/material.%.1.fit $(SRCDIR)/material.%.2.fit $(SRCDIR)/material.%.4.fit $(SRCDIR)/material.%.8.fit
	perl utilities/fit_files/chart_comparison.perl $^ | gnuplot > $@
$(COMPDIR)/convex/material.%.svg: originals/material.%.tsv  $(SRCDIR)/material.%.1.fit $(SRCDIR)/material.%.2.fit $(SRCDIR)/material.%.4.fit
	perl utilities/fit_files/chart_comparison.perl $^ | gnuplot > $@
$(COMPDIR)/convex/material.%.svg: originals/material.%.tsv  $(SRCDIR)/material.%.1.fit $(SRCDIR)/material.%.2.fit
	perl utilities/fit_files/chart_comparison.perl $^ | gnuplot > $@
$(COMPDIR)/convex/material.%.svg: originals/material.%.tsv  $(SRCDIR)/material.%.1.fit
	perl utilities/fit_files/chart_comparison.perl $^ | gnuplot > $@

$(COMPDIR)/both/material.%.svg: originals/material.%.tsv  $(SRCDIR)/material.%.2.neg.fit $(SRCDIR)/material.%.4.neg.fit $(SRCDIR)/material.%.8.neg.fit
	perl utilities/fit_files/chart_comparison.perl $^ | gnuplot > $@
$(COMPDIR)/both/material.%.svg: originals/material.%.tsv  $(SRCDIR)/material.%.2.neg.fit $(SRCDIR)/material.%.4.neg.fit
	perl utilities/fit_files/chart_comparison.perl $^ | gnuplot > $@
$(COMPDIR)/both/material.%.svg: originals/material.%.tsv  $(SRCDIR)/material.%.2.neg.fit
	perl utilities/fit_files/chart_comparison.perl $^ | gnuplot > $@

$(CHRODIR)/convex/material.%.1.svg: originals/material.%.tsv $(SRCDIR)/material.%.1.fit
	perl utilities/fit_files/chart_chromatic.perl $^ | gnuplot > $@
$(CHRODIR)/convex/material.%.2.svg: originals/material.%.tsv $(SRCDIR)/material.%.2.fit
	perl utilities/fit_files/chart_chromatic.perl $^ | gnuplot > $@
$(CHRODIR)/convex/material.%.4.svg: originals/material.%.tsv $(SRCDIR)/material.%.4.fit
	perl utilities/fit_files/chart_chromatic.perl $^ | gnuplot > $@
$(CHRODIR)/convex/material.%.8.svg: originals/material.%.tsv $(SRCDIR)/material.%.8.fit
	perl utilities/fit_files/chart_chromatic.perl $^ | gnuplot > $@

$(CHRODIR)/both/material.%.2.svg: originals/material.%.tsv $(SRCDIR)/material.%.2.neg.fit
	perl utilities/fit_files/chart_chromatic.perl $^ | gnuplot > $@
$(CHRODIR)/both/material.%.4.svg: originals/material.%.tsv $(SRCDIR)/material.%.4.neg.fit
	perl utilities/fit_files/chart_chromatic.perl $^ | gnuplot > $@
$(CHRODIR)/both/material.%.8.svg: originals/material.%.tsv $(SRCDIR)/material.%.8.neg.fit
	perl utilities/fit_files/chart_chromatic.perl $^ | gnuplot > $@
