#!/usr/bin/python3 

##########
# MCMC FIT: Calculate a linear combination of phase functions to approximate an arbitrary scattering function.
# Author: Haysn Hornbeck
# CC BY-SA 4.0

########## INCLUDES

import argparse
import emcee  
import multiprocessing as mp
import numpy as np
from operator import itemgetter
import scipy.optimize as op
import signal as sig
import sys


########## FUNCTIONS

# calculate a Henyey-Greenstein value
def hengreen(x, g):
    return (1 - g*g) / ( (1 + g*g - 2*g*np.cos(x))**1.5 )

# calculate Schlick's approximation
def schlick(x, g):
    return (1 - g*g) / ( (1 - g*np.cos(x))**2 )

# use the Cornette-Shanks phase function
def cornShanks(x, g):
    cosTh = np.cos(x)
    return (1 - g*g)*(1 + cosTh*cosTh) / ( (2 + g*g) * ((1 + g*g - 2*g*cosTh)**1.5) )


# make finding the index of the first octave easier
def fOI( o ):
    return varsPerOctave*o + 2      # theta's format: sigma, offset, (g, scalar(s))+


# the log likelihood function
def lnlike( theta, data ): 

    if len(theta) != nDim:
        return -np.inf

    retVal = 0			# leave blank, to save calculations

    for c in range(colours): 

        model = []              # temp row storage

        for o in range(octaves):   # for each variable
               index  = fOI(o)
               temp  = theta[index+c+1]*phaseFunction( data[0], theta[index] )
               if len(model) == 0:
                  model = temp
               else:
                  model += temp

        if np.any(model < 0):     # catch impossible model values
              return -np.inf

        metric = (data[c+1] - theta[1]) / model      # compare in log space

        if np.any(metric <= 0):        # catch impossible metrics
              return -np.inf

        diff = np.log( metric )         # compare in log space
        retVal -= np.sum(.5 * diff * diff)
    
				# catch up on a division and subtraction
    retVal = retVal / (theta[0]*theta[0]) - len(data[0])*colours*np.log(theta[0])

    return retVal


# the log prior
def lnprior( theta ): 

    if len(theta) != nDim:
        return -np.inf

    if theta[0] <= 0:           # weed out impossible values
        return -np.inf
    if floatOffset and ((theta[1] > Dbound[0]) or (theta[1] < (Dbound[0] - .31830988618379067154))):
        return -np.inf

    retVal = 0                  # set this up for later

    for o in range( octaves ):   # for each variable

        index = fOI( o )

        if (theta[index] <= -1) or (theta[index] >= 1):   # check on g
            return -np.inf

        if np.abs(theta[index]) < minG:                 # enforce a minimum g value
            return -np.inf

        if not negativeWeight:
            for col in range( colours ):        # as well as each colour, if asked for
                if theta[index+col+1] < 0:
                    return -np.inf
                if (maxW != 0) and np.abs(theta[index+col+1]) > maxW:
                    return -np.inf

        if o != 0:              # now specialized checks
            if theta[index] < theta[index-varsPerOctave]:
                return -np.inf

        if biasedPrior:
            retVal += np.log( np.abs(theta[index]) - minG + 0.000001 )                # favor big g values
            retVal -= np.abs(np.log( np.abs(theta[index+1]) ))                        #  and weights ~= 1

    if biasedPrior:
        retVal -= 6*np.log( Dbound[0] - theta[1] + 0.000001 )                               #  and offsets close to zero

    return retVal


# consolidate the likelihood and prior
def lnprob( theta, data ):
    retVal = lnprior( theta )
    if retVal == -np.inf:
        return retVal
    return retVal + lnlike( theta, data )

# wrap lnprob for use in DE
def lnwrap( theta ):
    return -lnprob( fixOctaves(theta), D )


# fix the order of each octave, so they go in ascending order
def fixOctaves( theta ):

    newTheta = theta.copy()
    gs = []			# phase 1: extract all the "g" values
    for o in range( octaves ):
        gs.append( (theta[fOI( o ):fOI( o+1 )]) )

    gs.sort( key=itemgetter(0) )
    for o in range( octaves ):	# phase 2: sort and replace in ascending order
        index = fOI( o )
        for i in range( varsPerOctave ):
            newTheta[index + i] = gs[o][i]

    return newTheta
    

# a quick wrapper to handle SIGINTs
def sigint(sig, frame):
    global caughtSigInt
    caughtSigInt = True

########## VARIABLES

phaseString = {			# handy for selecting a phase function
          "henyey-greenstein": 0,
          "schlick": 1,
          "cornette-shanks": 2
}

caughtSigInt = False		# used to trap SIGINT

########## MAIN

# handle the command line
parser = argparse.ArgumentParser(description='Fit volume scattering functions to an arbitrary phase function.')
parser.add_argument('-i','--input',dest='filename',type=open,required=True,help='The TSV file containing the phase function')
parser.add_argument('-o','--octaves',dest='octaves',type=int,default=4,help='The number of functions to use (default = %(default)s)')
parser.add_argument('-n','--negative',dest='negativeWeight',default=False,action='store_true',help='Allow negative weights')
parser.add_argument('-S','--MLSeed',dest='MLSeed',type=open,metavar="FILENAME",help='Provide a seed maximal likelihood via a TSV file')
parser.add_argument('-E','--EvaluateSeed',dest='eval',default=False,action='store_true',help="Don't do any fitting; instead, calculate some basic stats for the seed and quit")
parser.add_argument('-v','--variant',dest='variant',default="henyey-greenstein",help="The underlying function to fit to (default = %(default)s)")

parser.add_argument('-m','--minimumG',dest='minG',type=float,default=0.0,metavar="g",help="Enforce a minimum 'g' value")
parser.add_argument('-x','--maximumWeight',dest='maxW',type=float,default=0,metavar="FLOAT",help="Enforce a maximum weight magnitude")
parser.add_argument('-P','--biasedPrior',dest='biasedPrior',default=False,action='store_true',help="Bias the prior to favor large g values and weights ~= 1")
parser.add_argument('-O','--floatingOffset',dest='floatOffset',default=False,action='store_true',help='Add an offset to the model. Handy if the baseline is unknown')

parser.add_argument('-t','--threads',dest='threadCount',type=int,default=-1,help='The number of threads to use. -1 = system count (default = %(default)s)')
parser.add_argument('-b','--burnStep',dest='nBurnSteps',type=int,help='The number of steps to burn-in for MCMC. If omitted, a heuristic calculates this')
parser.add_argument('-p','--postStep',dest='nPostSteps',type=int,default=2,help='The number of steps used to build the posterior (default = %(default)s)')
parser.add_argument('-s','--stepSize',dest='nStepSize',type=int,default=40,help='The size of each MCMC step. Larger = faster, smaller = more responsive (default = %(default)s)')
parser.add_argument('-W','--Walkers',dest='walkers',type=int,default=192,help='The number of MCMC walkers. Should be greater than twice the number of variables (default = %(default)s)')
parser.add_argument('-D','--dumpPosterior',dest='dumpPost',default=False,action='store_true',help="Dump the contents of the posterior. Useful for debugging")

parser.add_argument('-B','--noBootstrap',dest='noBootstrap',default=False,action='store_true',help="Don't use Differential Evolution to bootstrap MCMC")
parser.add_argument('-c','--bootCutoff',dest='bootCut',type=float,default=-140,help='The log likelihood to switch from using DE to MCMC (default = %(default)s)')


args = parser.parse_args()

# adjust the threadcount
if args.threadCount == -1:
    args.threadCount = mp.cpu_count()

# sanity-check the evaluate option
if args.eval and not args.MLSeed:
    args.eval = False

# also check the chosen phase function
phaseChoice = phaseString.get( args.variant.lower(), -1 )
if phaseChoice == -1:       # no string match? try a numeric one
    try:
        phaseChoice = int(args.variant)
    except ValueError:
        phaseChoice = -1

if (phaseChoice < 0) or (phaseChoice >= len(phaseString)):
    print("ERROR: Invalid phase function selected, quitting.\nThe following are supported: ", end='')
    for i,v in phaseString.items():
        print("{} ({})  ".format(i,v), end='')
    print("")
    sys.exit(1)

# now assign phaseFunction, based on number
phaseFunction = hengreen
if phaseChoice == 1:
    phaseFunction = schlick
elif phaseChoice == 2:
    phaseFunction = cornShanks

# transfer some variables out, as they're used in functions
octaves         = args.octaves
negativeWeight  = args.negativeWeight
bootCut         = args.bootCut
minG            = args.minG
maxW            = args.maxW
biasedPrior     = args.biasedPrior
floatOffset     = args.floatOffset

# install the signal handler
sig.signal( sig.SIGINT, sigint )


# announce ourselves
print("MCMC_FIT: Calculate a linear combination of phase functions to approximate an arbitrary scattering function.")
print()


# load up the data, if possible
D      = np.loadtxt(args.filename, unpack=True)
Dbound = [np.amin(np.amin(D,axis=1)[1:]), np.amax(np.amax(D,axis=1)[1:]) ]      # useful in the prior

# check that we have at least two columns
if len( D[0] ) < 2:
    print("ERROR: We need at least two columns in the dataset, the left-most being")
    print(" the angle and the remainder being intensities at each colour. Quitting.")
    sys.exit(2)


# set up other important variables
varsPerOctave = len( D )            # total incoming variables
colours = varsPerOctave - 1         # number of colour channels to match
sizeD = np.asarray(D).size          # total length of array

nDim = fOI(octaves)
nWalkers = args.walkers		    # number of MCMC walkers

if not args.nBurnSteps:
    args.nBurnSteps = max(50, int(sizeD/15 * octaves**2))      # heuristic, tends to fail for >5 octaves


# read in the seed, if given
if args.MLSeed:
  tempStorage = np.loadtxt( args.MLSeed )
  if len(tempStorage) == nDim:          # sanity check the size
      args.noBootstrap = True
      results = tempStorage
      if args.eval:
          print( "Evaluating seed, as requested, then quitting." )
          lp = lnprior( results )
          ll = lnlike( results, D )
          print( "log(prior) = {}".format( lp ) )
          print( "log(likelihood) = {}".format( ll ) )
          print( "log(probability) = {}".format( ll + lp ) )
          sys.exit(0)
  else:
      print("WARNING: Given seed with dimension {}, when we needed {}. Ignoring seed.".format(len(tempStorage),nDim))

# a little prep work for DE
bounds = [(0.00000001,10)]
if floatOffset:
    bounds.append( (Dbound[0]-(1.0/np.pi),Dbound[0]) )
else:
    bounds.append( (0,0) )
for i in range(octaves):
    bounds.append( (-1,1) )
    for col in range( colours ):
        bounds.append( (-Dbound[1],Dbound[1]) )

DEsteps = args.nBurnSteps
DEcount = 0

# use a callback to update the status and check for early termination
def DEcallback( theta, convergence ):
    global DEcount      # don't display every update
    global caughtSigInt

    DEcount += 1
    if ((DEcount % args.nStepSize) != 1) and (not caughtSigInt):	# only do periodic updates, unless SIGINT
            return False

    likelihood = -lnwrap(theta)
    print( "DE {0:d}/{1:d}: ML = {2} (".format(DEcount,DEsteps,likelihood), end='' )
    print("{0:.5f}".format( theta[0] ), end='')
    for o in range( octaves ):
        index = fOI(o)
        print(", {0:.4e}".format( theta[index] ), end='')
    print(")")
    sys.stdout.flush()

    if caughtSigInt or (likelihood > bootCut):	# exit if requested
        return True
    else:
        return False

# if requested, bootstrap
if not args.noBootstrap:
    rngSeed = int( np.random.random() * (1 << 31) )     # fiddle with the RNG seed
    results = op.differential_evolution( lnwrap, bounds, maxiter=DEsteps, tol=0.0001, seed=rngSeed, callback=DEcallback )
    temp = -lnwrap( results.x )
    while temp < bootCut:

        if caughtSigInt:		# special handling of SIGINT
           caughtSigInt = False
           if temp == -np.inf:
              print("")
              print("INTERRUPTED. Quitting with no fit found.")
              sys.exit(3)
           else:
              break			# if we have a solution, just move on

        DEsteps *= 2
        DEcount = 0
        if temp == -np.inf:         # discard the old seed if nothing came back
            rngSeed = int( np.random.random() * (1 << 31) )
        results = op.differential_evolution( lnwrap, bounds, maxiter=DEsteps, tol=0.0001, seed=rngSeed, callback=DEcallback )
        temp = -lnwrap( results.x )
    results = fixOctaves(results.x)     # ignore the rest

# now use the results to build an MCMC seed
print("Gen ", end='')
sys.stdout.flush()


initSeed = np.array([])             # storage for the MCMC seed
genCount = nWalkers                 # ensure we don't loop infinitely
bestML = -np.inf		    # since MCMC can bounce away from the ML
bestRes = []
while len(initSeed) < nWalkers:

    candidate = []
    if 'results' in locals():       # if a result exists, use it
        for i,v in enumerate( results ):
            if v != 0:              # catch zeros
               candidate.append( (np.random.random()*.0001 + 1.00005)*v )
            elif i == 1:            # unless they're offsets
               candidate.append( 0 )
            else:
               candidate.append( np.random.random()*.0001 - 0.00005 )
    else:
        candidate.append( 3*np.random.random() + .5 )
        if floatOffset:
            candidate.append( Dbound[0] - np.random.random()/np.pi )
        else:
            candidate.append( 0 )
        fracG = 1. / octaves
        baseG = np.random.random() * fracG
        for i in range( octaves ):   # for each variable
            candidate.append( 2*(baseG + i*fracG) - 1 )
            for col in range( colours ):
                candidate.append( np.random.random() )

    ML = lnprob(candidate, D)
    if ML > -np.inf:
        if initSeed.size == 0:
            initSeed = np.array(candidate)
        else:
            initSeed = np.vstack( (initSeed, np.array(candidate)) )
        print(".", end='')
        sys.stdout.flush()
        if ML > bestML:
            bestML = ML
            bestRes = candidate
        genCount = nWalkers
    else:
        genCount -= 1

    if genCount < 0:
        print("ERROR: Could not generate a candidate! Quitting.")
        sys.exit(1)


print("")


# set up the sampler
sampler = emcee.EnsembleSampler(nWalkers, nDim, lnprob, args=[D], threads=args.threadCount)
pos, lp, rstate                = sampler.run_mcmc(initSeed, args.nStepSize, storechain=False)


# start the burn in
for i in range( args.nBurnSteps ):
    pos, lp, rstate                = sampler.run_mcmc(pos, args.nStepSize, lnprob0=lp, rstate0=rstate, storechain=False)
    
    max_i, max_v = max(enumerate(lp), key=lambda p: p[1])
    print("Burn {0:d}/{1:d}: ML = {2:.3f} (".format(i+1, args.nBurnSteps, max_v), end='')
    print("{0:.5f}".format( pos[max_i][0] ), end='')
    for o in range( octaves ):
        index = fOI(o)
        print(", {0:.4e}".format( pos[max_i][index] ), end='')
    print(")")
    sys.stdout.flush()
    if max_v > bestML:
        bestML = max_v
        bestRes = pos[max_i]

    if caughtSigInt:		# exit early if SIGINT
       caughtSigInt = False
       break

# now collect data
sampler.reset()

for i in range(args.nPostSteps):
    pos, lp, rstate                = sampler.run_mcmc(pos, args.nStepSize, lnprob0=lp, rstate0=rstate)
    max_i, max_v = max(enumerate(lp), key=lambda p: p[1])
    print("Post {0:d}/{1:d}: ML = {2:.3f} (".format(i+1, args.nPostSteps, max_v), end='')
    print("{0:.5f}".format( pos[max_i][0] ), end='')
    for o in range( octaves ):
        index = fOI(o)
        print(", {0:.4e}".format( pos[max_i][index] ), end='')
    print(")")
    sys.stdout.flush()
    if max_v > bestML:
        bestML = max_v
        bestRes = pos[max_i]

    if caughtSigInt:		# exit early if SIGINT
       caughtSigInt = False
       break

# extract the 16/84 credible intervals
sample = sampler.chain.reshape(-1, nDim)
allParams = list( map(lambda v: (v[1], v[2]-v[1], v[1]-v[0]), 
    zip(*np.percentile(sample, [16, 50, 84], axis=0))) )


print("")
print("RESULTS, median + 16/84 credibility")
print(" sigma   = {0} (+{1} -{2})".format( *allParams[0] ))
print(" offset  = {0} (+{1} -{2})".format( *allParams[1] ))
for o in range( octaves ):
    index = fOI(o)
    print(" g{0:d}     = {1} (+{2} -{3})".format( (o+1), *allParams[index] ))
    print(" scale{0:d} = [".format( o+1 ), end='')
    for col in range( colours ):
        if col != 0:
            print( ", ", end='' )
        print( "{0} (+{1} -{2})".format( *allParams[index + col + 1] ), end='')
    print("]")

print("")
print("RESULTS, ML")

print(" log(likelihood) = {0}".format( bestML ))
print(" sigma  = {0}".format( bestRes[0] ))
print(" offset = {0}".format( bestRes[1] ))
for o in range( octaves ):
    index = fOI(o)
    print(" g{0:d}     = {1}".format( (o+1), bestRes[index] ))
    print(" scale{0:d} = (".format( (o+1) ), end='')
    for c in range( colours ):
        if c != 0:
            print( ", ", end='' )
        print("{}".format( bestRes[index+c+1] ), end='')
    print(")")

print("")
print("RESULTS, ML, NORMALIZED")

# this requires some pre-calculations, like the total of each colour column
intraColourTotal = [0 for c in range(colours)]
for c in range( colours ):
    for o in range( octaves ):
        index = fOI(o) + c
        intraColourTotal[c] += bestRes[index + 1]

# apply the above, to make the next step easier
adjustedResults = [[0 for c in range(colours)] for o in range(octaves)]
for o in range( octaves ):
    index = fOI(o)
    for c in range( colours ):
        adjustedResults[o][c] = bestRes[index + c + 1] / intraColourTotal[c]

# and the index of the "maximum" value in each octave
octaveMax = [0 for o in range( octaves )]
for o in range( octaves ):
    for c in range( colours ):
        if np.abs(adjustedResults[o][c]) > np.abs(adjustedResults[o][octaveMax[o]]):
           octaveMax[o] = c


for o in range( octaves ):
    index = fOI(o)
    print(" g{0:d}     = {1}".format( (o+1), bestRes[index] ))
    print(" scale{0:d} = {1} * (".format( (o+1), adjustedResults[o][octaveMax[o]] ), end='')
    for c in range( colours ):
        if c != 0:
            print( ", ", end='' )
        print("{}".format( adjustedResults[o][c] / adjustedResults[o][octaveMax[o]] ), end='')
    print(")")

print("")
print("BOOTSTRAP")
for i,v in enumerate( bestRes ):
    if i > 0:
        print("\t", end='')
    print("{}".format( v ), end='')
print("")


if args.dumpPost:           # if asked for, dump the posterior
    print("")
    print("POSTERIOR")
    for point in sampler.flatchain:
        for i,v in enumerate( point ):
            if i > 0:
               print("\t", end='')
            print("{}".format( v ), end='')
        print("")

