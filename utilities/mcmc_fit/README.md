The Volume Scattering Function Database
=======================================

# MCMC_Fit
[This script](mcmc_fit.py) takes a scattering function in TSV format and fits a linear combination of scattering functions.

## Requirements
This Python 3 script depends on [emcee](http://dfm.io/emcee/current/) and numpy/scipy.

## Usage
For a good summary of the options this script accepts,

```
python3 mcmc_fit.py --help
```

This script was designed to work fairly well by default, so in many cases

```
python3 mcmc_fit.py -i [INPUT] -o [OCTAVES] | tee results.txt
```

is sufficient. Nonethelss, there are ample options to tune how the script behaves, which allow an experienced user to get better results in less time, or handle scattering functions which are difficult to fit. All users of this script are encouraged to read this README in full. 

### Fitting Model

As implied by the use of emcee, this script does Bayesian fitting. While other approaches are quicker, Bayesian statistics offers more ways to assess the quality of fit. If a wide variety of parameter values lead to the same results, the posterior distribution will be diffuse; if only a narrow range gives reasonable results, it will cluster into a number of small peaks. The total peak count provides a hint as to how many fitting functions are necessary. If for instance you perform a fit with a combination of four functions, yet only three peaks are visible in the posterior, it's a safe bet that three functions are sufficient.

The likelihood of the input scattering function, $`VSF(\theta)`$, given one angle and a specific set of parameters, is

```math
\mathcal{L}(\sigma,G,W|\theta) = \mathcal{N}(VSF(\theta) - o, ~ \sum_{k=1}^n w_k \cdot FIT(\theta,g_k), ~ \sigma), \\
\mathcal{N}(\mu,x,\sigma) = \sigma^{-1}e^{-\frac 1 2 \log(\frac{x}{\mu})^2\sigma^{-2}}
```

where $`\sigma`$ is the standard deviation, $`FIT`$ the fitting function, $`n`$ is the number of fitting functions used, $`o`$ an offset value, $`G`$ a collection of $`g_1, g_2, \ldots, g_n`$ anisotropy values, and $`W`$ a collection of $`w_1, w_2, \ldots, w_n`$ matching weights. The standard deviation is a good metric of how well the model fits the data; larger deviations imply a poorer fit. The default prior is flat, provided the parameters are valid (eg. -1 < g < 1). Likelihoods for multiple angles are combined by multiplying them together. Note that the Gaussian penalty function computes the distance in log-space instead of linear-space.

### Result Format

The output can be divided into three phases. The first and optional one uses differential evolution[^1] to bootstrap the MCMC algorithm. This tends to find a reasonable answer faster than MCMC, initially. The second is the MCMC burn-in phase, which takes over once the first phase has reached a certain likelihood, and the third phase generates the actual posterior distribution.

In all of these phases, the display is periodically updated with the maximal likelihood within the current pool (that may be _less_ likely than the previous maximum, when using MCMC), the standard deviation associated with it, and the anisotropy values. You can advance from the first to the second, as well as the second to third, by pressing Control-C. This is useful if differential evolution is struggling to improve the fit, or the number of samples was set too high.

Once the third phase is done, the results are printed. The first RESULTS section gives medians and credible intervals, which give a quick summary of the posterior. The next two RESULTS sections focus on the maximal likelihood found, with the first best suited to graphing the fit while the second is easier to use within a shader.

The BOOTSTRAP section is the maximal likelihood found in [tab-separated form](https://en.wikipedia.org/wiki/Tab-separated_values). This can be used to create a seed file for resuming or improving an existing fit. The [doubleBootstrap.perl](../fit_files/doubleBootstrap.perl) script also uses this. The underlying format is

```math
\sigma	~ o ~ (g_1 ~ w_1 ~ w_2 \ldots w_c) ~ (g_2 ~ w_1 ~ w_2 \ldots w_c) ~ \ldots
```

without any brackets. Values of g are constrained so that $`g_1 < g_2 < \ldots < g_n`$.

Finally, the POSTERIOR section prints the posterior distribution found with MCMC, using the same format as BOOTSTRAP, if the user supplies the `-D` option. This gives a wealth of information about the quality of fit, although some assembly is required. This data is used by the database to generate [per-material scatterplots](../../charts/scatter/), for instance.


### Basic Options

`-i` is the only non-optional paramater, as it specifies the input file. This must be in [tab-separated format](https://en.wikipedia.org/wiki/Tab-separated_values), with the first column representing the sampled angle in radians, and subseqent columns representing the proportion of scattering at that angle for a given wavelength. For example:

```
# theta                 0.6             0.55            0.45
0                       9.785E+03       1.138E+04       1.672E+04
0.00174532925199433     9.768E+03       1.136E+04       1.668E+04
0.00349065850398866     9.718E+03       1.129E+04       1.652E+04
0.00523598775598299     9.633E+03       1.117E+04       1.628E+04
# ...
```

Lines beginning with "#" are ignored. There is no restriction on the number of wavelengths, nor do the rows need to be sorted, nor is exponential notation necessary, nor do angles even need to be evenly spaced or cover the full range from 0 to $`\pi`$.

`-o` specifies the number of fitting components or 'octaves' to use in the linear combination. More components might lead to a better fit, but also require more search time to zero in on a fit.

By default, all the weights for these fitting components are positive or "convex," as many renderers only support combinations in that format. Adding the `-n` option will allow negative weights, which usually lead to a better fit.

### Bootstrapping

The MCMC algorithm needs an initial state within the parameter space in order to properly explore it. While not every parameter combination must be valid, there must be enough to form a gradient along every parameter. By default this seed is randomly generated and spread across the entire parameter space, but there are situations where it makes sense to generate a very clustered seed. `-S` takes the name of a one-line text file, formatted the same as the BOOTSTRAP section mentioned above, which contains the basis for a seed. If this switch is provided, the differential evolution phase is skipped.

As mentioned earlier, the default prior is flat for valid parameters. Sometimes the sampler needs to be guided towards a solution by introducing a bias to the prior. The `-P` option does exactly that, swapping the flat prior for

```math
\begin{aligned}
p(\sigma,G,W) &= \prod_{k=1}^n \frac{ g_k - g_{\text{min}} + 10^{-6} } { q( w_k ) } \\
q( w ) &= \begin{cases} \frac 1 {|w|}, & |w| > 1 \\ |w|, & \text{otherwise} \end{cases}
\end{aligned}
```

This favors values of $`g`$ which differ significantly from 0, and weight values which approximately equal 1. $`g_{\text{min}}`$ corresponds to the value supplied by the `-m` parameter, discussed later.

The bootstrap format doesn't contain any information on how likely that parameter value is. This can be annoying if you have multiple bootstrap files without proper labels. It also creates problems if you use the biased prior for some of your runs and the flat prior for others, as the relative likelihoods cannot be compared.

The `-E` parameter was created for the above situation: when you also supply a seed file with `-S`, it evaluates the seed with the likelihood and prior functions, prints the results, and quits without engaging the MCMC sampler.

When developing this script, we discovered that differential evolution would usually speed convergence during the early exploration phase, so it is enabled by default. "Usually" is not the same as "always," however, so if you find DE is having difficulty you can switch it off with `-B`.

The transition from differential evolution to MCMC happens when the maximal likelihood crosses a certain threshold. You can override the default threshold with `-c`. Remember that logrithmic likelihoods are used, rather than linear ones; to move the threshold an order of magnitude, for instance, add or subtract 2.3.

### MCMC Options

While the posterior distribution is great for debugging, it takes up a lot of space and thus is not printed by default. To change this, supply the `-D` option. The number of rows in the posterior is the number of walkers, multiplied by the step size, multiplied by the number of steps used to generate the posterior. See the results section for details on how it is formatted.

`-b` adjusts the number of steps used to burn in the MCMC sampler. These samples are used to bring the collection of samples closer to the posterior; since future samples are more representative, these samples are discarded. To set the number of steps taken into the posterior, supply an integer to `-p`. Typically, the number of burn-in samples greatly outnumbers the posterior samples.

Rather than update the MCMC walkers once, it's more efficient to run batches of updates. You can adjust the size of these batches with the `-s` option. This also effects the responsiveness of the status display: larger step sizes mean longer delays between status updates, and the reverse is true. As mentioned earlier, changing this option also effects the number of samples in the posterior.

emcee and other MCMC samplers usually dispatch groups of "walkers" to explore the posterior. Using more than one reduces the chances of the sampler getting stuck in a rare part of the posterior. Conversely, if you want to run the algorithm for a fixed length of time, increasing the number of walkers will make it less likely that you'll get a good sample of the posterior; this is because how close any walker comes to the posterior is roughly proportional to the number of steps it has taken. A good heuristic is to at least double the number of variables you have in your model. Here, that is $`2 + n \cdot (1 + c)`$, where $`n`$ is the number of linear components and $`c`$ is the number of wavelengths. By default, this script uses a fixed number; to manually adjust the number of walkers, use the `-W` option.

Since nearly all modern processors have multiple cores, it makes sense to spread the fitting across them. By default, `mcmc_fit` is greedy and grabs every available core, but you can manually supply the number of cores to use via the `-t` option.

### Other Options

For most scattering function inputs, zero corresponds to zero likelihood. It may not, though, and a model that doesn't incorporate an offset will have difficulty matching this; it may try to use one of the components as a substitute. Worse, any offset will warp the resulting fit and lead to results which do not match the input.

That's why the fitting model has an offset value, even if is usually zeroed to stop emcee from exploring it. To enable offsets, supply the `-O` option. You will likely want to add the `-m` option as well, which sets a minimal absolute value of g to prevent emcee from using it as an offset.

At one point, the authors had difficulty with the weights of linear combinations becoming arbitrarily large. The `-x` option was added to clip how large weights could get. We haven't observed this problem recently, but the option remains just in case.

Keen observers will note we've been careful to say "fitting function" instead of "Henyey-Greenstein." There are other distributions which have the same input parameters, yet have slightly different outputs. Christophe Schlick, for instance, developed a similar function which is quicker to evaluate.[^2]

```math
\begin{aligned}
HG( \cos(\theta), g ) &= \frac{ 1 - g^2 }{ 4 \pi } (1 + g^2 - 2g\cos(\theta))^{-\frac 3 2} \\
\text{Schlick}( \cos(\theta), g ) &= \frac{ 1 - g^2 }{ 4 \pi } (1 + g\cos(\theta))^{-2}
\end{aligned}
```

While few renderers use Schlick's version, there is one notable exception: [LuxCore](https://github.com/LuxCoreRender/LuxCore/blob/f76865186f3067edb3621ee7cb3061b901749c59/src/slg/volumes/volume.cpp). Anyone mistakenly using a Henyey-Greenstein fit within LuxCore will find it doesn't behave as expected.

Another scattering function with the same inputs as the Henyey-Greenstein was developed by William Cornette and Joseph Shanks.[^3]

```math
\text{Cornette-Shanks}( \cos(\theta), g ) = \frac{ 3 }{ 8 \pi } \frac{ 1 - g^2 }{ 2 + g^2 } (1 + \cos(\theta)^2 ) (1 + g^2 - 2g\cos(\theta))^{-\frac 3 2}
```

Their formulation has some useful properties. When g = 0, the output is identical to Rayleigh scattering. Otherwise, the distribution has peaks for both forward and backward scatter, a better match for real-life scattering distributions than the Henyey-Greenstein.

Since all three functions have the same input parameters, and the same parameter domains, it is fairly trivial to allow any of the three to be used as a fitting function. The `-v` parameter allows you to do exactly that, with the Henyey-Greenstein function as the default.


### Fitting Guide

The authors have some experience with fitting, and can offer a little advice. Differential evolution works best when the number of fitting components is four or less. We recommend disabling it for higher numbers.

The curse of dimensionality happens early with emcee, it's not practical to attempt an eight-component fit without supplying a seed. Instead, a more efficient approach is to generate a fit with a lower component count, extract the BOOTSTRAP section into a file, modify it to have the higher component count in some way, then use that as a seed. This may take multiple steps; the authors always started from one component and worked upwards.

The authors originally expanded the seed by manually inserting intermediate g values with very small weights, however we found emcee rarely raised those weights and effectively ignored those components. A better approach is to split one component in two, halving the weight across two components. Some care must be taken with the anisotropy factors, as they must be strictly increasing and sufficiently far apart that the seed generation process won't reverse the order too much. The [doubleBootstrap](../fit_files/doubleBootstrap.perl) script automates this procedure.

Negative probability has a stronger dimensional curse. We recommend against using convex combinations to bootstrap. Instead, start from a single-component fit and work upwards. Two-component convex combinations may also work as seeds, as only ~3% of the materials in the database (2 and 6) benefitted from two-component negative probability.

The burn-in sample count heuristic is conservative. In most cases, it pays to periodically check in on the fit, and if there seems to be little progression use Control-C to skip to the posterior stage. Be careful, as more than one press of Control-C will quit the program.

As mentioned earlier, there's a wealth of detail in the posterior. Even the simple 16/84 credible intervals listed in the results can show the relative contribution of each component. Wide intervals mean a wide range of parameter values provide similar results, and vice-versa. The scripts used to [generate our charts](../fit_files/) provide much more information; spend some time browsing the charts already [contained in the database](../../charts/) to get a feel for what good and bad values look like, then chart your own fits and compare.

Ideally you'd like to see nice elliptical "clumps" in the posterior's scatterplot, but if you instead find fuzzy "boomerangs" that's a sign the fitness landscape is complex, difficult to explore, or both. If posterior points have quite a bit of empty space between them, emcee is having difficulty exploring the posterior; consider increasing the burn-in time. Performing a second fit using the BOOTSTRAP of a converged fit will tend to give you a better view of the posterior than the original, unless the original rapidly climbed to maximal fitness.


### Citations

[^1]: Storn, Rainer, and Kenneth Price. "[Differential evolution–a simple and efficient heuristic for global optimization over continuous spaces](ftp://ftp.icsi.berkeley.edu/pub/techreports/1995/tr-95-012.pdf)." Journal of global optimization 11.4 (1997): 341-359.

[^2]: Blasi, Philippe, Bertrand Le Saec, and Christophe Schlick. “A Rendering Algorithm for Discrete Volume Density Objects.” In Computer Graphics Forum, 12:201–210. Wiley Online Library, 1993.

[^3]: Cornette, William M., and Joseph G. Shanks. “Physically Reasonable Analytic Expression for the Single-Scattering Phase Function.” Applied Optics 31, no. 16 (June 1, 1992): 3152–60. https://doi.org/10.1364/AO.31.003152.