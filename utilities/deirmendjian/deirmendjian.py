#!/usr/bin/python3 

##########
# DEIRMENDJIAN: Generate a phase function from various cloud particle size distributions.
# Author: Haysn Hornbeck
# CC BY-SA 4.0

########## INCLUDES

import argparse
import emcee                    # needed for sampling
import miepython as mie
from multiprocessing import Pool
import multiprocessing as mp
import numpy as np
import scipy as sp
import signal as sig
import sys


########## FUNCTIONS

# the PDF of Levin's distribution, in log space (Levin 1958)
def lnLevin( a, rn, r ):
    if r <= 0:
        return -np.inf
    return (a-1)*np.log(r) - r/rn

# Deirmendjian's distribution, again in log space (Deirmendjian 1964)
def lnDeir( a, B, l, r ):
    if r <= 0:
        return -np.inf
    return a*np.log(r) - B*( r**l )

# a boring ol' Patero/power distribution
def lnPatero( a, r ):
    if r <= 0:
        return -np.inf
    return a*np.log(r)

# and this weird thing, from Deirmendjian
def lnHazeC( start, end, a, r ):
    if r < start:
        return -np.inf
    elif r > end:
        return a*np.log(r/end)
    else:
        return 0

# might as well add a Log Gaussian distro (tweaked via SciPy)
def lnLogGauss( sigma, scale, mean, r ):
    if r <= 0:
        return -np.inf
    base    = (np.log(r/scale) - mean)/sigma
    return -0.5*base*base - np.log(r*sigma/scale)

# as well as an actual Gamma distro
def lnGamma( alpha, beta, r ):
    if r <= 0:
       return -np.inf
    return (alpha - 1)*np.log(r) - beta*r


# a helper function for multiprocessing
def calcDist(i):
    global cosAngles
    global complexIOR
    S1, S2 = mie.mie_S1_S2( complexIOR, i, cosAngles )
    results = .5 * (abs(S1)**2 + abs(S2)**2)
    return results


########## VARIABLES

# the particle distributions we know of
dists = [
        "deirmendjian",
        "haze_c",
        "levin",
        "patero",
        "loggauss",
	"gamma",
        "dirac",
	"help"
        ]

presets = [
        "cloud_c.1",
        "haze_m",
        "haze_c",
        "levin",
        "delta_10.1",
        "monte_di_procida.A",
        "napoli.A",
	"help"
        ]

OPACangles = [ "112", "167" ]

# OPAC's hard-coded angles, 112-sample version
OPACangles112 = [ 0, 0.00174532922222222, 0.00349065844444445, 0.00523598766666667, 
    0.00698131688888889, 0.00872664611111111, 0.0104719753333333, 0.0122173045555556, 
    0.0139626337777778, 0.015707963, 0.0174532922222222, 0.0191986214444444, 
    0.0209439506666667, 0.0226892798888889, 0.0244346091111111, 0.0261799383333333, 
    0.0279252675555556, 0.0296705967777778, 0.031415926, 0.0331612552222222, 
    0.0349065844444444, 0.0523598766666667, 0.0698131688888889, 0.0872664611111111, 
    0.104719753333333, 0.139626337777778, 0.174532922222222, 0.209439506666667, 
    0.244346091111111, 0.279252675555556, 0.31415926, 0.349065844444444, 
    0.383972428888889, 0.418879013333333, 0.453785597777778, 0.488692182222222, 
    0.523598766666667, 0.558505351111111, 0.593411935555556, 0.62831852, 
    0.663225104444444, 0.698131688888889, 0.733038273333333, 0.767944857777778, 
    0.802851442222222, 0.837758026666667, 0.872664611111111, 0.907571195555556, 
    0.94247778, 0.977384364444445, 1.01229094888889, 1.04719753333333, 
    1.08210411777778, 1.11701070222222, 1.15191728666667, 1.18682387111111, 
    1.22173045555556, 1.25663704, 1.29154362444444, 1.32645020888889, 
    1.36135679333333, 1.39626337777778, 1.43116996222222, 1.46607654666667, 
    1.50098313111111, 1.53588971555556, 1.5707963, 1.60570288444444,
    1.64060946888889, 1.67551605333333, 1.71042263777778, 1.74532922222222,
    1.78023580666667, 1.81514239111111, 1.85004897555556, 1.88495556, 
    1.91986214444444, 1.95476872888889, 1.98967531333333, 2.02458189777778, 
    2.05948848222222, 2.09439506666667, 2.12930165111111, 2.16420823555556, 
    2.19911482, 2.23402140444444, 2.26892798888889, 2.30383457333333, 
    2.33874115777778, 2.37364774222222, 2.40855432666667, 2.44346091111111, 
    2.47836749555556, 2.51327408, 2.54818066444444, 2.58308724888889, 
    2.61799383333333, 2.65290041777778, 2.68780700222222, 2.72271358666667, 
    2.75762017111111, 2.79252675555556, 2.82743334, 2.86233992444444, 
    2.89724650888889, 2.93215309333333, 2.96705967777778, 3.00196626222222, 
    3.03687284666667, 3.07177943111111, 3.10668601555556, 3.14159265358979 ]

OPACangles167 = [ 0, 1.74532925199433e-05, 3.49065850398866e-05, 5.23598775598299e-05, 
    6.98131700797732e-05, 8.72664625997165e-05, 0.000174532925199433, 0.000261799387799149, 
    0.000349065850398866, 0.000436332312998582, 0.000523598775598299, 0.000698131700797732, 
    0.0010471975511966, 0.00139626340159546, 0.00174532925199433, 0.0020943951023932, 
    0.00244346095279206, 0.00279252680319093, 0.00314159265358979, 0.00349065850398866, 
    0.00383972435438753, 0.00418879020478639, 0.00453785605518526, 0.00488692190558412, 
    0.00523598775598299, 0.00610865238198015, 0.00698131700797732, 0.00785398163397448, 
    0.00872664625997165, 0.00959931088596881, 0.010471975511966, 0.0113446401379631, 
    0.0122173047639603, 0.0130899693899575, 0.0139626340159546, 0.0148352986419518, 
    0.015707963267949, 0.0165806278939461, 0.0174532925199433, 0.0183259571459405, 
    0.0191986217719376, 0.0200712863979348, 0.020943951023932, 0.0218166156499291, 
    0.0226892802759263, 0.0235619449019235, 0.0244346095279206, 0.0253072741539178, 
    0.0261799387799149, 0.0270526034059121, 0.0279252680319093, 0.0287979326579064, 
    0.0296705972839036, 0.0305432619099008, 0.0314159265358979, 0.0322885911618951, 
    0.0331612557878923, 0.0340339204138894, 0.0349065850398866, 0.0436332312998582, 
    0.0523598775598299, 0.0610865238198015, 0.0698131700797732, 0.0785398163397448, 
    0.0872664625997165, 0.10471975511966, 0.122173047639603, 0.139626340159546, 
    0.15707963267949, 0.174532925199433, 0.191986217719376, 0.20943951023932, 
    0.226892802759263, 0.244346095279206, 0.261799387799149, 0.279252680319093, 
    0.296705972839036, 0.314159265358979, 0.331612557878923, 0.349065850398866, 
    0.366519142918809, 0.383972435438752, 0.401425727958696, 0.418879020478639, 
    0.436332312998582, 0.453785605518526, 0.471238898038469, 0.488692190558412, 
    0.506145483078356, 0.523598775598299, 0.558505360638185, 0.593411945678072, 
    0.628318530717959, 0.663225115757845, 0.698131700797732, 0.733038285837618, 
    0.767944870877505, 0.802851455917391, 0.837758040957278, 0.872664625997165, 
    0.907571211037051, 0.942477796076938, 0.977384381116825, 1.01229096615671, 
    1.0471975511966, 1.08210413623648, 1.11701072127637, 1.15191730631626, 
    1.18682389135614, 1.22173047639603, 1.25663706143592, 1.2915436464758, 
    1.32645023151569, 1.36135681655558, 1.39626340159546, 1.43116998663535, 
    1.46607657167524, 1.50098315671512, 1.53588974175501, 1.5707963267949, 
    1.60570291183478, 1.64060949687467, 1.67551608191456, 1.71042266695444, 
    1.74532925199433, 1.78023583703422, 1.8151424220741, 1.85004900711399, 
    1.88495559215388, 1.91986217719376, 1.95476876223365, 1.98967534727354, 
    2.02458193231342, 2.05948851735331, 2.0943951023932, 2.12930168743308, 
    2.16420827247297, 2.19911485751286, 2.23402144255274, 2.26892802759263, 
    2.30383461263251, 2.3387411976724, 2.37364778271229, 2.40855436775218, 
    2.44346095279206, 2.47836753783195, 2.51327412287183, 2.54818070791172, 
    2.58308729295161, 2.61799387799149, 2.65290046303138, 2.68780704807127, 
    2.72271363311115, 2.75762021815104, 2.79252680319093, 2.82743338823081, 
    2.8623399732707, 2.89724655831059, 2.93215314335047, 2.96705972839036, 
    3.00196631343025, 3.03687289847013, 3.07177948351002, 3.10668606854991, 
    3.12413936106985, 3.13286600732982, 3.14159265358979 ]

########## MAIN

parser = argparse.ArgumentParser(description='Generate a volume scattering function from Mie scattering of a specific particle distribution.')
parser.add_argument('-q','--quiet',dest='quiet',default=False,action='store_true',help='Suppress status output, printing only the TSV')
parser.add_argument('-d','--distribution',dest='dist',choices=dists,metavar="NAME",help="The particle distribution to draw from. (default = {})".format(dists[0]))
parser.add_argument('-p','--preset',dest='preset',choices=presets,metavar="NAME",help='Use the parameters from the given preset')

parser.add_argument('-a','--angles',dest='divs',type=int,default=180,metavar="COUNT",help='The number of angular divisions between 0 and pi, inclusive (default = %(default)s)')
parser.add_argument('-w','--wavelengths',dest='waves',type=float,nargs='+',default=[.609, .550, .464],metavar="MICRONS",help='Evaluate the function at the given wavelengths, in micrometres (default = %(default)s)')         # RGB, tuned for Blender
parser.add_argument('-c','--cutoff',dest='cutoff',type=float,default=100,help='Ignore particles greater than this, in micrometres (default = %(default)s)')
parser.add_argument('-f','--cutoffFuzz',dest='fuzz',type=float,default=(1./3.),help='Amount of fuzz to apply to cut off particles (default = %(default)s)')
parser.add_argument('-i','--ior',dest='ior',type=float,nargs='+',default=[1.3320,1.3330,1.3364],help='The index of refraction of these particles (default = %(default)s)')
parser.add_argument('-A','--Attenuation',dest='atten',type=float,nargs='+',default=[1.1980e-8,1.9600e-9,9.7240e-10],help='The attenuation of these particles (default = %(default)s)')

parser.add_argument('--rn',dest='rn',type=float,help='Provide a custom value for "rn"')
parser.add_argument('--l',dest='l',type=float,help='Provide a custom value for "l"')
parser.add_argument('--a',dest='a',type=float,help='Provide a custom value for "a"')

parser.add_argument('-C','--CosineWeight',dest='cosW',default=False,action='store_true',help='Use cosine-weighting to sample the function')
parser.add_argument('-O','--OPAC',dest='OPAC',choices=OPACangles,help="Use OPAC's hard-wired angles.")

parser.add_argument('-s','--samples',dest='sampleCount',type=int,default=4096,help='The number of samples to draw from the particle distribution (default = %(default)s)')
parser.add_argument('-b','--burn-in',dest='burnin',type=int,default=65536,help='The number of samples to draw in order to burn in MCMC (default = %(default)s)')
parser.add_argument('-W','--Walkers',dest='walkers',type=int,default=64,help='The number of MCMC walkers. Larger values = greater step size = less responsiveness (default = %(default)s)')
parser.add_argument('-t','--threads',dest='threads',type=int,default=-1,help='The number of threads to use. -1 == system count (default = %(default)s)')
parser.add_argument('-S','--dumpSamples',dest='dumpSamp',default=False,action='store_true',help="Dump the samples drawn via MCMC and quit")

args = parser.parse_args()


# adjust the threadcount
if args.threads == -1:
    args.threads = mp.cpu_count()

# set up presets
a = 3
rn = 6.75
l = 1.3

# print out the known presets, if asked
if args.preset == "help":
	print("The available presets are:")
	for i,x in enumerate(presets):
		if x != "help":
			if i > 0:
				print(", ",end="")
			print("{}".format(x),end="")
	print("")
	sys.exit(1)

# otherwise, begin applying them
elif args.preset == "cloud_c.1":
       a  = 6.        # alpha
       rn = 1.5       # beta
       l  = 1.        # gamma
       args.dist = "deirmendjian"
    
elif args.preset == "haze_m":
       a  = 1.        # alpha
       rn = 8.944     # beta
       l  = 0.5       # gamma
       args.dist = "deirmendjian"
    
elif args.preset == "haze_c":
       rn = 0.3        # cliff start
       l  = 1.         # beginning of decline
       a  = -4.        # power of the decline
       args.dist = "haze_c"
    
elif args.preset == "levin":
       rn = 3.75    # reference particle size
       a  = 2        # power of the decline + 1
       args.dist = "levin"

elif args.preset == "delta_10.1":
       rn = 10.1    # reference particle size
       args.dist = "dirac"

elif args.preset == "monte_di_procida.A":
       rn = 4.69            # scale (the mean, in practice)
       a  = np.log(1.504)   # standard deviation
       l  = 0               # "mean" (more of an offset)
       args.dist = "loggauss"

elif args.preset == "napoli.A":
       rn = 6.9
       a  = np.log(1.261)
       l  = 0
       args.dist = "loggauss"

# now replace them with custom variables, if requested
if args.a:
   a = args.a

if args.l:
   l = args.l

if args.rn:
   rn = args.rn


# now do the same for the distributions
if not args.dist:
   args.dist = dists[0]        # force it to exist

# we should have some way to print out the known distributions
if args.dist == "help":
	print("The available distributions are:")
	for i,x in enumerate(dists):
		if x != "help":
			if i > 0:
				print(", ",end="")
			print("{}".format(x),end="")
	print("")
	sys.exit(1)

# otherwise, construct the appropriate distribution, using lambdas to hide the parameters
elif args.dist == "deirmendjian":
    def lnDropDist( r ):
      return lnDeir(a, rn, l, r)
    
elif args.dist == "haze_c":    # Deirmendjian's Haze C
    if a >= 0:
        print("ERROR: The falloff (--a) must be negative!")
        sys.exit(1)

    def lnDropDist( r ):
      return lnHazeC(rn, l, a, r)
    
elif args.dist == "levin":    # Levin
    def lnDropDist( r ):
      return lnLevin(a, rn, r)
    
elif args.dist == "patero":    # Patero
    if a >= 0:
        print("ERROR: The slope of a Patero distribution must be negative!")
        sys.exit(1)

    def lnDropDist( r ):
      return lnPatero(a, r)

elif args.dist == "loggauss":    # Log Gaussian
    def lnDropDist( r ):
      return lnLogGauss(a, rn, l, r)

elif args.dist == "gamma":    # Gamma
    beta = (a - 1) / rn
    def lnDropDist( r ):
      return lnGamma(a, beta, r)



# generate the angles
if args.OPAC == "112":
    angles = OPACangles112
    cosAngles = np.cos( angles )

elif args.OPAC == "167":
    angles = OPACangles167
    cosAngles = np.cos( angles )

elif args.cosW:
    cosAngles = np.linspace( 1, -1, args.divs )
    angles = np.arccos( cosAngles )

else:
    angles = np.linspace( 0, np.pi, args.divs )
    cosAngles = np.cos( angles )


# prep an MCMC sample, if we're using a distribution
if args.dist != "dirac":

    nSteps = int(args.sampleCount / args.walkers)
    seed = [ np.random.random(1)*args.cutoff for i in range(args.walkers) ]
    pos = seed

    sampler = emcee.EnsembleSampler(args.walkers, 1, lnDropDist, threads=args.threads)
    if not args.quiet:
       print("= Generating {} samples, burn-in phase ".format(args.burnin), end='')

    for i in range(int( args.burnin / args.walkers )):
        pos, prob, state = sampler.run_mcmc(pos, 1)
        if not args.quiet:
           print(".", end='')
           sys.stdout.flush()

    if not args.quiet:
       print(" done.")

    sampler.reset()

    if not args.quiet:
       print("= Generating {} samples, sampling phase ".format( args.sampleCount ), end='')
    for i in range( nSteps ):
        pos, prob, state = sampler.run_mcmc( pos, 1 )
        if not args.quiet:
           print(".", end='')
           sys.stdout.flush()

    if not args.quiet:
       print(" done.")

    samples = sampler.chain.reshape(-1, 1)    # reshape the gathered samples into a flat array
                                        # ensure the cutoff is respected, with some random noise injected
    fuzzScale = np.sqrt(args.fuzz)
    np.place( samples, samples>args.cutoff, [(np.random.random(1)*fuzzScale + 1)*args.cutoff for i in samples] )

else:
    samples = np.array([ rn ])    # Dirac delta? Test a single particle size

if args.dumpSamp:
   if not args.quiet:
      print( "==CUT==" )
   for i in samples:
       print( "{}".format(i[0]) )
   sys.exit()


if not args.quiet:
   print("= Integrating Mie distributions ", end='')

result = np.zeros( (len(args.waves), len(angles)) )	# the eventual output
delay = int(args.sampleCount / 80)			# how often we update
counter = 0						# keep track of when we update

for i,w in enumerate( args.waves ):

  # convert the IOR/attenuation into something more useful
  if i < len(args.ior):
      ior   = args.ior[i]
  else:
      ior   = args.ior[len(args.ior)-1]

  if i < len(args.atten):
      atten = -np.abs(args.atten[i])
  else:
      atten = -np.abs(args.atten[len(args.atten)-1])

  complexIOR = complex(ior, atten)

  adjSamples = samples / w            # particle size has to be relative to wavelength
  with Pool(processes=args.threads) as pool:

     for r in pool.imap_unordered(calcDist, adjSamples, 16):
         result[i] = np.add( result[i], r )
         counter += 1
         if (counter > delay) and not args.quiet:
            print(".", end='')
            sys.stdout.flush()
            counter = 0

  result[i] /= args.sampleCount


# begin the TSV portion
if not args.quiet:
   print(" done.\n")
   print( "==CUT==" )
   
   print("# theta", end='')
   for w in args.waves:
     print("\t{}".format( w ), end='')
   print("")

for i,a in enumerate( angles ):
 print( "{}".format( a ), end='' ) 
 for w in range( len(args.waves)  ):
    print( "\t{}".format( result[w][i] ), end='' )

 print("")
