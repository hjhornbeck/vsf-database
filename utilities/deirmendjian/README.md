The Volume Scattering Function Database
=======================================

# Deirmendjian
[This script](deirmendjian.py) turns information about spherical particles into a scattering function, printing the result in [TSV format](https://en.wikipedia.org/wiki/Tab-separated_values).

## Requirements
This Python 3 script depends on [emcee](http://dfm.io/emcee/current/), [miepython](https://github.com/scottprahl/miepython/), and numpy/scipy.

## Usage
To get a good summary of the options this script accepts, type

```
python3 deirmendjian.py --help
```

To replicate a number of known distributions, including all those mentioned in our paper, supply a preset on the command line. You can learn which ones are available via

```
python3 deirmendjian.py --preset help
```

A list of distributions is also available via `--distribution help`. Typically you'll want to capture the output of this program via something like

```
python3 deirmendjian.py --preset cloud_c.1 | tee results.tsv
```

The remaining options can be divided into several categories.

### Scattering Distribution Sampling
By default, `deirmendjian` will generate samples of the VSF at evenly-spaced angles between 0 and $`\pi`$, inclusive. `-a 360` will increase the number of samples to 360. `-C` will switch to cosine-weighted sampling.

OPAC[^4] fixes the angles you can sample from. The spacing is nonlinear and concentrated in areas which typically show large changes in likelihood, which can make comparisons to linear-spaced VSFs tricky. `deirmendjian` can be forced to use the same angles as OPAC, via the `-O` option. You will need to supply which collection of angles to choose, as OPAC has different presets for two categories of scattering function.

The photon wavelengths to sample can be controlled with the `-w` option. This input can take a list, and the default list matches the primaries used by Blender. The wavelength must be specified in microns. There is no restriction on the number of wavelengths.

### Droplet Physics
Mie theory needs a complex number to represent the index of refraction. `-i` assigns the real portion, what most people think of as "the" index of refraction, while `-A` deals with the complex or "attenuation" portion. Both accept a list of values, one for each wavelength. The default values correspond to liquid water at room temperature. A good resource for other values is the [Refractive Index Database](https://refractiveindex.info/).

The complex portion is in inverse microns, and forced to be negative. The equations behind Mie theory do not allow you to control the number of bounces inside the spherical particle, though you can approximate an opaque material by using a large attenuation.

### Droplet Distributions
In reality, droplets of material are never exactly one fixed radius, but follow a range that's described by a probability distribution function. `deirmendjian` comes with a number of such functions.

#### Levin's Modified Gamma (1958)
```math
\mathcal{L}(\alpha, r_n | r) = r^{\alpha-1} \cdot e^{-\frac{r}{r_n}}
```

One of the earliest droplet distributions published. As the name implies, it is based on the [Gamma distribution](https://en.wikipedia.org/wiki/Gamma_distribution).

It takes two parameters. `--rn` is the characteristic radius, in microns, while `--a` is a shape parameter which controls the rate of decline.

#### Deirmendjian's Modified Gamma (1964)
```math
\mathcal{L}(\alpha, \beta, \gamma | r) = r^\alpha \cdot e^{-\beta \cdot r^\gamma}
```

This is Deirmendjian's attempt to match real-world data. Levin's model has fewer and more intuitive parameters, but Deirmendjian's appears to be more realistic.

There are three parameters. `--a` is $`\alpha`$ (alpha) or one less than shape parameter, `--rn` assigns $`\beta`$ (beta), while `--l` represents $`\gamma`$ (gamma). There is no simple explaination for how each parameter alters the shape of the distribution, so for further details read Deirmendjian (1964)[^2].

#### Deirmendjian's Haze C (1964)
```math
\mathcal{L}(a, r_{\text{max}}, r_{\text{min}} | r) = \begin{cases} 0, & r < r_{\text{min}} \\ (\frac{r}{r_{\text{max}}})^a, & r > r_{\text{max}} \\ 1, & \text{otherwise} \end{cases}
```

The droplet radii in this haze have a sharp minimal size, and above a certain limit the probability drops off according to a Patero distribution; between those, all droplet sizes are equally likely.

`--rn` provides the minimal droplet size, `--l` is the cutoff where the probability of that droplet size decreases, and `--a` is the power of the decrease. The first two values are in microns, while the last must be negative.

#### Patero
```math
\mathcal{L}(a | r) = r^a
```

Surface tension places a strict limit on the size of the smallest droplet which can form. If a substance is hotter than its critical temperature, though, surface tension disappears and droplet sizes should follow a Patero distribution.

This takes one parameter, `--a`, which determines the slope of the Patero distribution. This must be a negative number.

#### Log Gaussian
```math
\begin{aligned}
\mathcal{L}(\mu, \log(\sigma),  s | r) = & ~ r \frac{\log(\sigma)}{s} \cdot e^{-\frac 1 2 b^2} \\
b = & ~ \frac 1{\log(\sigma)}  (\log\left(\frac r s\right) - \mu)
\end{aligned}
```

More commonly known as the "log normal" distribution, this was introduced as a generic distribution for building other distributions or experimenting. It also naturally occurs within certain types of fog (see Podzimek 1997[^1]).

The `--rn` parameter is a scaling factor, `--a` is the log of the standard deviation, and `--l` is the mean in microns. In practice, it's most intuitive to fix the mean at 0 and treat the scaling factor as if it was the mean.

#### Gamma
```math
\mathcal{L}(\alpha, \beta | r) = r^{\alpha - 1} e^{-\beta \cdot r}
```

Another generic distribution, meant for building or experimenting, though the parameters have been modified to be more intuitive. `--a` is the shape parameter $`\alpha`$, the same as with Levin, while `--rn` provides the mode of the distribution, $`\beta`$, in microns.

#### Dirac Delta
The [Dirac delta](https://en.wikipedia.org/wiki/Dirac_delta_function) isn't a true probability function; instead it allows you to directly specify a fixed particle radius. This is useful if you'd like to use a function that doesn't come with `deirmendjian`; simply generate a list of particle sizes, execute the script once for each particle size, then average the output of all runs. It can also be useful for debugging.

The `--rn` parameter specifies the droplet radius, in microns.

### MCMC sampling
Unfortunately, `miepython` cannot integrate over a probability density function, so we need to draw a pool of random samples from it. Markov Chain Monte Carlo is the easiest way to accomplish this, but it also introduces more parameters to tweak.

`-b` specifies the number of samples to discard, in order to "burn in" the algorithm. Increasing this number improves the quality of the sample, at the cost of additional time. The number of samples to keep for `miepython` is controlled by `-s`. `-W` controls the number of walkers used by `emcee`. More walkers dilutes the chance of the algorithm getting stuck in a low probability part of the function, but decreases the responsiveness of the script.

As the size of the droplet grows relative to the wavelength, the waviness due to interference decreases and the more alike their scattering distribution gets. However, `miepython`'s performance decreases exponentially with the radius of the droplet. To save computation time, `deirmendjian` can filter out any droplets above a certain size. `-c` will set a cut-off value for where the filter will kick in.

Our experience shows that any practical hard cutoff will distort the resulting scattering distribution, so any radius above the cutoff is replaced with one drawn from a uniform random distribution that ranges between $`c`$ and $`c \cdot (1 + \sqrt{f})`$, with $`c`$ and $`f`$ as the cutoff and fuzz values. The `-f` parameter controls the value of $`f`$.

### Other Options
`-q` will suppress all extraneous output, which is useful for scripts. `-t` controls the number of threads this script will use. The default value, -1, uses as many threads as there are processors.

If you would just like to examine the random sample of droplet sizes drawn via MCMC, adding `-S` to the command line will print them out. No scattering distribution will be calculated.

MCMC is not run when the particle distribution is a Dirac delta.

## Naming
`deirmendjian` is named after Diran Deirmendjian. He created an impressive amount of literature on droplet distributions, atmospheric properties, and Mie scattering. A few examples:

>>>
Diran Deirmendjian & Zdenek Sekera (1954) "[Global Radiation Resulting from Multiple Scattering in a Rayleigh Atmosphere](https://doi.org/10.3402/tellusa.v6i4.8756)," Tellus, 6:4, 382-398, DOI: 10.3402/tellusa.v6i4.8756

D. Deirmendjian, R. Clasen, and W. Viezee, "[Mie Scattering with Complex Index of Refraction](https://www.osapublishing.org/josa/abstract.cfm?uri=josa-51-6-620)," J. Opt. Soc. Am. 51, 620-633 (1961)

[^2]: D. Deirmendjian, "[Scattering and Polarization Properties of Water Clouds and Hazes in the Visible and Infrared](https://www.osapublishing.org/ao/abstract.cfm?uri=AO-3-2-187)," Appl. Opt. 3, 187-196 (1964)
>>>

Unfortunately, a number of citations have mangled the spelling of his last name; by naming this program after him, we hope to dilute their impact.

## Citations

[^1]: Podzimek, Josef. “Droplet Concentration and Size Distribution in Haze and Fog.” Studia Geophysica et Geodaetica 41, no. 3 (1997): 277–296.

[^4]: Hess, M., P. Koepke, and I. Schult. “[Optical Properties of Aerosols and Clouds: The Software Package OPAC.](https://doi.org/10.1175/1520-0477%281998%29079%3C0831%3AOPOAAC%3E2.0.CO%3B2)” Bulletin of the American Meteorological Society 79, no. 5 (May 1, 1998): 831–44.
