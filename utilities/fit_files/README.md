The Volume Scattering Function Database
=======================================

# fit_files
This is a collection of scripts which transform the output of [mcmc_fit](../mcmc_fit) in a variety of ways.

## chart_chromatic
An obvious way to check the quality of a fit is to graph it against the original scattering function. [chart_chromatic](chart_chromatic.perl) is a perl script to accomplish this. For instance, from the root directory, try

```
perl utilities/fit_files/chart_chromatic.perl originals/material.0033.tsv fit_logs/material.0033.8.fit | gnuplot > /tmp/material.0033.8.svg
```

As implied above, this script's STDOUT is a series of `gnuplot` commands, and when fed into `gnuplot` the result is an [SVG file](https://en.wikipedia.org/wiki/Scalable_Vector_Graphics) printed to STDOUT. Unfortunately, this script only handles a maximum of three wavelengths.

## chart_comparison
While the previous script is quite helpful, it deliberately allows only one fit to be charted. Multiple fits would cause confusion, as each would have multiple colour channels present.

[chart_comparison](chart_comparison.perl) handles this gap. It can display multiple fits, by only displaying one colour channel per fit. The default is to use the second wavelength, or the first if the fit was monochromatic. Usage is almost the same as the prior script.

```
perl utilities/fit_files/chart_comparison.perl originals/material.0033.tsv fit_logs/material.0033.?.fit | gnuplot > /tmp/material.0033.svg
```

## doubleBootstrap
`mcmc_fit` has difficulty finding a proper fit when the search space becomes large. The authors have found that the best way to overcome this is to "bootstrap" higher-dimensional searches by using a fit found in a lower dimension. After some experimentation, the authors found an algorithm which seems to work well: split each anisotropy factor in two, giving each a slightly different offset to ensure their uniqueness and half the original weight. To encourage exploration, the standard deviation is also inflated by a constant.

[doubleBootstrap](doubleBootstrap.perl) is a Perl script which implements this algorithm. Supply a `.fit` file via the command line or STDIN, and it will extract the BOOTSTRAP section and print a new bootstrap value to STDOUT. It accepts no command-line parameters.

## fit2progress
It can be difficult to assess the convergence of `mcmc_fit` by reading its output. [fit2progress](fit2progress.perl) is a Perl script which reads an input file or STDIN, extracting the maximal likelihoods at each burn-in or posterior step, and prints a gnuplot script to STDOUT. If given an input file, it attempts to build a custom title by parsing it. The gnuplot script itself prints to STDOUT, which allows for safe and easy multiprocessing at the cost of a redirect. If you'd like a usage example, open up the root [Makefile](../../Makefile).

## fit2scatter
We commonly focus on the maximal likelihood of the posterior, but this tosses out critical information. If a component does not contribute much towards the fit, its peak in the posterior will tend to be diffuse. Multiple reasonable solutions will get ignored. 

[fit2scatter](fit2scatter.perl) converts the posterior into a scatterplot, which reveals those details. It has the same usage as `fit2progress`, but while the former generates an SVG file the latter generates a PNG. Again, the [Makefile](../../Makefile) provides a usage example.

## fit2shader
Converting the output of `mcmc_fit` into something that can be used in a shader by hand is tedious, especially give the scale of this database. Instead, we use [fit2shader](fit2shader.perl) to automatically transform it into an Open Shading Language shader with correct attribution. From there, it should be easy to convert into another format.

While usage is similar to the prior two functions, `fit2shader` also accepts an option command-line parameter; if present, it will convert a chromatic fit into an achromatic one. The algorithm it uses is only approximate, so for best results feed achromatic data into `mcmc_fit` instead.

## likelihoods
To generate the summary of all fits, we needed a way to extract the maximal likelihood from every `mcmc_fit` output file in a computer-readable fashion. [likelihoods](likelihoods.sh) is a Bash script which, whe run in the [fit_logs](../../fit_logs/) directory, prints that output in [TSV format](https://en.wikipedia.org/wiki/Tab-separated_values).
