#!/usr/bin/perl -w 

# grab key variables
die "Please provide a filename on the command line!"	if (scalar(@ARGV) < 1);
my $number	= ($ARGV[0]	=~ /\.0{0,3}(\d{1,4})\.\d+\.fit/)[0];
my @data;			# store all the functions

my $achromatic	= 1	if( (scalar(@ARGV) > 1) and ($ARGV[1] eq "achromatic") );		# simple command-line toggle

# for each line
my $s;			# state variable
while (<>) {

	# extract g...
	push @data, [$1]	if ($s and /g\d+ += (.*)/);
	
	# ... and all associated scaling
	if ($s and /scale\d+ += (.*)/) {

		my @l = ($1 =~ /([\-\.e\d]+)/g);
		push @{$data[$#data]}, @l;
		}

	undef $s	if (/BOOTSTRAP/);
	$s = 1		if (/NORMALIZED/);
	}
		
	
# prepare some variables to make printing more convenient
my $paddedNum	= sprintf( "%04d", $number );
my $hgs		= scalar(@data);

# start off with the header
print <<EOL;
shader material${paddedNum}VSF [[ string help = "The volumetric shader for material $number, with $hgs Henyey-Greenstein functions. Coded by HJ Hornbeck, CC-BY-SA 4.0." ]] (

    color Color = color(1.0, 1.0, 1.0) [[ string help = "The colour of the cloud at the given point." ]],
    float Density = 0 [[ string help = "The density of the cloud at the given point." ]],

    output closure color BSDF = 0 [[ string help = "The resulting VSF." ]] )
    
{

    BSDF = Density*Color*( 

EOL

# now, for each HG
for (my $i = 0; $i < $hgs; $i++) {

	# start printing it out
	my @l	= @{$data[$i]};
	if (not defined $achromatic) {

		print "\t\t$l[1] * ";
		print "color($l[2], $l[3], $l[4]) * "	if (scalar(@l) > 3);
		}
	elsif (scalar(@l) <= 3 ) {	# it's achromatic after all

		print "\t\t$l[1] * ";
		}
	else {				# time to do some juggling
		# TODO: proper wavelength -> sRGB conversion, instead of this
		print "\t\t" . ($l[1] * (0.2126*$l[2] + 0.7152*$l[3] + 0.0722*$l[4])) . " * ";
		}

	print "henyey_greenstein($l[0])";
	print " +"	if ($i < $#data);

	print "\n";

	}

# finally, the footer
print "\n\n/** Fitted to a VSF provided by:\n";

print "\tI. Gkioulekas, S. Zhao, K. Bala, T. Zickler, and A. Levin." .
	" \"Inverse Volume Rendering with Material Dictionaries.\"" .
	" ACM Trans. Graph., 32(6):162:1–162:13, Nov. 2013. doi:" .
	" 10.1145/2508363.2508377"	if ($number < 20);

print "\tHess, M., P. Koepke, and I. Schult. \"Optical properties" .
	" of aerosols and clouds: The software package OPAC.\"" .
	" Bulletin of the American meteorological society 79.5" .
	" (1998): 831-844."	if ( ($number > 19) and ($number < 60) );

print	"\tLM Levin, via the following:\n" .
	"\tBouthors, Antoine, Fabrice Neyret, Nelson Max, Eric" .
	" Bruneton, and Cyril Crassin. \“Interactive Multiple" .
	" Anisotropic Scattering in Clouds,” 173. ACM Press, 2008." .
	" https://doi.org/10.1145/1342250.1342277." if ( ($number == 60) );

print 	"\tDeirmendjian, D. \“Scattering and Polarization Properties" .
	" of Water Clouds and Hazes in the Visible and Infrared.\”" .
	" Applied Optics 3, no. 2 (February 1, 1964): 187–96." .
	" https://doi.org/10.1364/AO.3.000187." 
	if ( ($number > 60) and ($number < 64) ); 

print 	"\tLaven, Philip. \“Simulation of Rainbows, Coronas, and Glories" .
	" by Use of Mie Theory.” Applied Optics 42, no. 3 (January 20," .
       	" 2003): 436. https://doi.org/10.1364/AO.42.000436." 
	if ( ($number > 101) and ($number < 105) ); 

print <<EOL;

 **/
            );

}
EOL
