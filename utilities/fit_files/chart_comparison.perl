#!/usr/bin/perl -w

# The achromatic version only grabs the given wavelength, or the last one
#  if it doesn't exist. In return, though, you can stack multiple fits by
#  supplying their fits on the command line.

my $channel = 1;		# starts at 0!

# can we generate a better title?
my $title	= "INSERT TITLE HERE";
my $mat		= -1;
if ( scalar(@ARGV) > 0 ) {

	$mat			= ($ARGV[0] =~ /\.0{0,3}(\d+)\./)[0];
	$title			= "Material " . ($mat + 0);
	}

my $FILE;		# a filehandle to recycle
my $max; my $min;	# min/max, useful for plotting and determining whether to switch to logscale
my @linears;		# an array of linear HG combinations, with each component as an array
my $logspace;		# are we doing logspace?

			# combination format: name, offset, (anisotropy, weight)+

# first command line option: the original VSF
open( $FILE, "<", $ARGV[0] )	or die "Could not open the VSF file! $!\n";

print "\$VSF << EOD\n";
while (<$FILE>) {

	next if (/^#/);		# comment line? discard it

	chomp;
	my @line	= split "\t";		# catch OOB
	$channel	= scalar(@line) - 2	if ( ($channel > scalar(@line) - 2) and not defined $max );

	$max = $line[$channel+1]	if ( (not defined $max) or ($max < $line[$channel+1]) );
	$min = $line[$channel+1]	if ( (not defined $min) or ($min > $line[$channel+1]) );

	print "$line[0]\t$line[$channel+1]\n";

	}
print "EOD\n";

# be polite
close( $FILE );

# are we going logscale?
$logspace = 1		if ( ($min > 0) and ($max/$min > 8) );

# time to consider the HG fits
shift @ARGV;

for my $file (@ARGV) {

	open( $FILE, "<", $file )	or die "Could not open the FIT file! $!\n";

	(my $num, my $order)	= ($file =~ /\.(\d{4})\.(\d+)\./);
	my $combo		= [$num];
	my $state;

	while( <$FILE> ) {

		# start reading in data
		push @{$combo}, $1	if ( defined $state and (/offset += ([\-\d\.]+)/) );
		push @{$combo}, $1	if ( defined $state and (/g\d+ += ([\-\d\.]+)/) );
		if ( defined $state and (/scale\d+ += \((.+)\)/) ) {

			my @comps	= ($1 =~ /([eE\+\-\d\.]+)/g);
			push @{$combo}, $comps[$channel];
			}

		$state = 1	if (/^RESULTS, ML$/);
		undef $state	if (defined $state and /^$/);
		}

	close( $FILE );
	push @linears, $combo;
	}
	
# prepare for the plot
my $midpoint	= 0.5*($max + $min);
if ($logspace) {

	$midpoint = sqrt($max * $min);
	print "set logscale y\n";
	}

print <<EOL;

hengreen(x,g) = (1 - g*g)/( (1 + g*g - 2.*g*cos(x))**1.5 )
set samples 1024

set term svg size 1280,720

set xtics (0, "pi/2" pi*.5, "pi" pi)
set ytics ($max, $midpoint, $min)

set title "$title"
set pointsize 0.5

EOL

# actually submit the commands
print "plot [0:pi] \$VSF u 1:2 w p pointtype 6 lc -1 t 'scattering function'";
for my $lhg (@linears) {

	my @combo = @{$lhg};
	print ", $combo[1]";

	my $neg;

	my $count = (scalar(@combo) - 2) >> 1;
	for( my $i = 0; $i < $count; $i++ ) {

		print " + $combo[$i*2 + 3]*hengreen(x,$combo[$i*2 + 2])";
		$neg = 1	if ($combo[$i*2 + 3] < 0);
		}
	print "t '$count (";
	if( $neg ) {	print "both"; }
	else {		print "convex"; }
	print ")'";
	
	}
