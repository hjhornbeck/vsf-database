#!/usr/bin/perl -w

my $offset	= 0.0001;	# how much are we offsetting these components? Adjust as necessary
my $scale	= sqrt(2);	# scale the standard deviation by this amount, to encourage exploration

my $components = 1;	# we'll need to know how many components there are
my @bootstrap;		# and of course we'll need a copy of the old bootstrap
my $state;		# standard state machine


# read in the data 
while (<>) {
	
	if (/^ *g(\d+) +=/) {	# update the component count

		$components	= $1	if ($1 > $components);
		}

	if ($state) {		# read in the bootstrap

		chomp;
		@bootstrap	= split "\t";
		undef $state;
		}
		
	$state = 1	if (/^BOOTSTRAP$/);	# trigger a bootstrap read
	}

# now to get down to brass tacks
die "Did not find a bootstrap!"		if (scalar(@bootstrap) < 3);

my @output	= ($bootstrap[0]*$scale, $bootstrap[1]);

my $step	= (scalar(@bootstrap) - 2) / $components;
my $colours	= $step - 1;
for (my $i = 2; $i < scalar(@bootstrap); $i += $step) {

	push @output, $bootstrap[$i] - $offset;		# low component
	for (my $c = 0; $c < $colours; $c++) {

		push @output, $bootstrap[$i+$c+1] * .5;
		}

	push @output, $bootstrap[$i] + $offset;		# high component
	for (my $c = 0; $c < $colours; $c++) {

		push @output, $bootstrap[$i+$c+1] * .5;
		}

	}

# print it all out
for (my $i = 0; $i < scalar(@output); $i++) {

	print "\t"	if ($i > 0);
	print $output[$i];
	}
print "\n";
