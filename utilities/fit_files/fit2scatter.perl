#!/usr/bin/perl -w

# can we generate a better title?
my $title	= "INSERT TITLE HERE";
if ( scalar(@ARGV) > 0 ) {

	(my $mat, my $oct)      = ($ARGV[0] =~ /\.(\d{4})\.(\d{1,2})\./);
	$title                  = "Material " . ($mat + 0) . ", $oct components";
	}

# set up our colours
my @col		= ("535200", "ff6340", "6ff88a", "371ac1");
# my $flipColours	= 1;		# comment out if using RGB components
undef $flipColours;			# comment out if using BGR

if (defined $flipColours) {

	my $temp	= $col[1];
	$col[1]		= $col[2];
	$col[2]		= $col[3];
	$col[3]		= $temp;
	}

# define some things
print <<EOL;

set term png size 1280,720
#set output 'fit2scatter.png'

set title "$title"
set xtics (-1, 0, 1)
set xlabel "anisotropy"

set grid y

EOL

# feed gnuplot the datapoints, and scan for interesting stuff
print "\$DATA << EOD\n";
my $max;
my $min;
my $count	= 1;	# number of components
my $colours	= 1;	# assume achromatic to start

my @ML_g;		# store maximal likelihoods
my @ML_w;


my $state;		# state variable

while (<>) {

	if (/g(\d+) += ([\-\.\d]+)$/) {

		$count = $1	if ($1 > $count);
		$ML_g[$1-1]	= $2;
		}

	elsif (/scale(\d+) += \((.*)\)$/) {

		my $temp	= $2 =~ tr/\.//;	# colour count!
		$colours	= $temp		if ($temp > $colours);

		$ML_w[$1-1]	= $2;
		}
	
	elsif ($state) {		# convert the poster into something friendlier

		chomp;
		my @line	= split "\t";
		shift @line;	shift @line;	# remove the standard deviation and offset

		for( my $i = 0; $i < $count; $i++ ) {

			print "$line[($colours+1)*$i + 0]";
			for( my $c = 0; $c < $colours; $c++ ) {

				# save these to help narrow down our bounds
				$max	= $line[($colours+1)*$i + $c + 1]
					if( (not defined $max) or ($line[($colours+1)*$i + $c + 1] > $max) );
				$min	= $line[($colours+1)*$i + $c + 1]
					if( (not defined $min) or ($line[($colours+1)*$i + $c + 1] < $min) );

				print "\t$line[($colours+1)*$i + $c + 1]";
				}

			print "\n";

			} # for( each component )

		} # if(state)

	$state = 1	if (/POSTERIOR/);
	}

print "EOD\n";

# if no posterior was read in, die here
exit 1	if ( (not defined $min) or (not defined $max) );


# now set up our lines and ytics
print "\$ML << EOD\n";
for( my $i = 0; $i < scalar(@ML_w); $i++ ) {
	
	my $temp	= ($ML_w[$i] =~ s/, */\t/g);
	print "$ML_g[$i]\t$ML_w[$i]\n";
	}

print "EOD\n";

# catch whether or not we've got negative weights
if ($min <= 0) {	# we do

	print "set ylabel \"weight\"\n";

	print "set ytics ($max, 0, $min)\n";

	} else {	# we don't

	print "set logscale y\n";
	print "set ylabel \"log(weight)\"\n";

	my $midPoint = sqrt($min*$max);
	print "set ytics ($max, $midPoint, $min)\n";

	}


# plot it out
print "plot [-1:1] [$min:$max] \$DATA u 1:2 w dots lc";

if( $colours != 3 ) {

	print " rgb '#$col[0]' notitle,";
	}
else {
	print 			   " rgb '#$col[1]' notitle," .
		" '' u 1:3 w dots lc rgb '#$col[2]' notitle," .
		" '' u 1:4 w dots lc rgb '#$col[3]' notitle,";
	}

print " \$ML u 1:2 pt 4 ps 2 lc";
if( $colours != 3 ) {

	print " rgb '#$col[3]' title 'Maximal Likelihood'\n";
	}
else {
	print	                      " rgb '#$col[0]' title 'Maximal Likelihood'," .
		" '' u 1:3 pt 4 ps 2 lc rgb '#$col[0]' notitle," .
		" '' u 1:4 pt 4 ps 2 lc rgb '#$col[0]' notitle\n";
	}
