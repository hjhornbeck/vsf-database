#!/usr/bin/perl -w

# The chromatic version displays the full chromatic spread of a material.
#  This comes at the cost of explicitly overlaying multiple materials.

# can we generate a better title?
my $title	= "INSERT TITLE HERE";
my $mat		= -1;
my $oct		= -1;
if ( scalar(@ARGV) > 1 ) {

	($mat, $oct)		= ($ARGV[1] =~ /\.0{0,3}(\d+)\.(\d+)\./);
	$title			= "Material " . ($mat + 0) . ", with $oct components";
	}

my $FILE;		# a filehandle to recycle
my $max; my $min;	# min/max, useful for plotting and determining whether to switch to logscale
my @linear;		# an array of linear HG combinations
my $logspace;		# are we doing logspace?
my $channels;		# how many channels do we have
my $components;		#  and how many components

			# combination format: offset, (anisotropy, weight+)+

sub colName($) {

	return 'red'		if ($_[0] == 0);
	return 'dark-green'	if ($_[0] == 1);
	return 'blue'		if ($_[0] == 2);
	return 'black';
	}

# first command line option: the original VSF
open( $FILE, "<", $ARGV[0] )	or die "Could not open the VSF file! $!\n";

print "\$VSF << EOD\n";
while (<$FILE>) {

	next if (/^#/);		# comment line? discard it

	chomp;
	my @line	= split "\t";
	$channels	= $#line
		if ( (not defined $channels) or ($#line < $channels) );

	for( my $c = 0; $c < scalar(@line); $c++ ) {

		print "\t"		if ($c > 0);
		print ($line[$c] + 0);	# print and store range
		next			if ($c == 0);

		$max = $line[$c]	if ( (not defined $max) or ($max < $line[$c]) );
		$min = $line[$c]	if ( (not defined $min) or ($min > $line[$c]) );
		}

	print "\n";

	}
print "EOD\n";

# be polite
close( $FILE );

# are we going logscale?
$logspace = 1		if ( ($min > 0) and ($max/$min > 8) );

# time to consider the HG fit
shift @ARGV;
open( $FILE, "<", $ARGV[0] )	or die "Could not open the FIT file! $!\n";

my $state;
while( <$FILE> ) {

	# start reading in data
	push @linear, $1	if ( defined $state and (/offset += ([\-\d\.]+)/) );
	push @linear, $1	if ( defined $state and (/g\d+ += ([\-\d\.]+)/) );
	if ( defined $state and (/scale\d+ += \((.+)\)/) ) {

		my @comps	= ($1 =~ /([e\-\d\.]+)/g);
		push @linear, @comps;
		$components++;
		}

	$state = 1	if (/^RESULTS, ML$/);
	undef $state	if (defined $state and /^$/);
	}

close( $FILE );
	
# prepare for the plot
my $midpoint	= 0.5*($max + $min);
if ($logspace) {

	$midpoint = sqrt($max * $min);
	print "set logscale y\n";
	}

print <<EOL;

hengreen(x,g) = (1 - g*g)/( (1 + g*g - 2.*g*cos(x))**1.5 )
set samples 1024

set term svg size 1280,720

set xtics (0, "pi/2" pi*.5, "pi" pi)
set ytics ($max, $midpoint, $min)
set pointsize 0.5

set title "$title"

EOL

# actually submit the commands
print "plot [0:pi] ";
for( my $c = 0; $c < $channels; $c++ ) {

	print "\$VSF u 1:".($c+2)." w p pointtype 6 lc rgb '".colName($c)."'";
        if ($c == 0) {
		print " t 'scattering function', ";
	} else {
		print " notitle, ";
		}
	}

for( my $c = 0; $c < $channels; $c++ ) {

	print "$linear[0]";

	for( my $i = 0; $i < $components; $i++ ) {

		my $index = $i*($channels+1) + 1;
		print " + $linear[$index+$c+1]*hengreen(x,$linear[$index])";
		}
	
	print " w l lc rgb '".colName($c)."'";
        if ($c == 0) {
		print " t 'fit'";
	} else {
		print " notitle";
		}

	print ", "	unless( ($c + 1) == $channels );
	}
