#!/usr/bin/perl -w

# can we generate a better title?
my $title	= "INSERT TITLE HERE";
if ( scalar(@ARGV) > 0 ) {

	(my $mat, my $oct)      = ($ARGV[0] =~ /\.(\d{4})\.(\d{1,2})\./);
	$title			= "Material " . ($mat + 0) . ", $oct components";
	}

# define some things
print <<EOL;
linear(x) = m*x + b

set term svg size 1280,720
#set output 'fit2progress.svg'

set style fill transparent solid 0.1
unset xtics
set title "$title"

EOL

# feed gnuplot the datapoints
print "\$DATA << EOD\n";
my $count = 0;
my $max;
my $min;

while (<>) {

	if (/ML = ([\-\d\.]+) \(/) {
		print "$1\n";
		$max = $1	if( (not defined $max) or ($max < $1));
		$min = $1	if( (not defined $min) or ($min > $1));
		$count++
		}
	}

print "EOD\n";

my $midPoint	= $count >> 1;

# generate a fit
print <<EOL;

fit linear(x) \$DATA u 0:1 via m,b
set ytics ($max, linear($midPoint), $min)
plot [] [$min:$max] \$DATA u 0:1 w filledc x1 t 'log(likelihood)', \\
	linear(x) lc -1 t sprintf("trendline (m=%f,b=%f)",m,b)

EOL
