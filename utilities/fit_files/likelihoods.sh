#!/bin/sh

for NUM in {0..13} {20..57} {60..65} {100..104} ; do 
	
	FULL=`printf "%04d" $NUM` 
	echo -ne "$FULL\t"
       	if [ -s material.$FULL.1.fit ] ; then echo -ne `grep "log(likelihood)" material.$FULL.1.fit | sed -e 's/.* = //'` ; fi
       	echo -ne "\t"
	if [ -s material.$FULL.2.fit ] ; then echo -ne `grep "log(likelihood)" material.$FULL.2.fit | sed -e 's/.* = //'` ; fi
       	echo -ne "\t"
	if [ -s material.$FULL.2.neg.fit ] ; then echo -ne `grep "log(likelihood)" material.$FULL.2.neg.fit | sed -e 's/.* = //'` ; fi
       	echo -ne "\t"
	if [ -s material.$FULL.4.fit ] ; then echo -ne `grep "log(likelihood)" material.$FULL.4.fit | sed -e 's/.* = //'` ; fi
       	echo -ne "\t"
	if [ -s material.$FULL.4.neg.fit ] ; then echo -ne `grep "log(likelihood)" material.$FULL.4.neg.fit | sed -e 's/.* = //'` ; fi
       	echo -ne "\t"
	if [ -s material.$FULL.8.fit ] ; then echo -ne `grep "log(likelihood)" material.$FULL.8.fit | sed -e 's/.* = //'` ; fi
       	echo -ne "\t"
	if [ -s material.$FULL.8.neg.fit ] ; then echo -ne `grep "log(likelihood)" material.$FULL.8.neg.fit | sed -e 's/.* = //'` ; fi
       	echo

done
