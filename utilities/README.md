The Volume Scattering Function Database
=======================================

# Utilities
Here, you will find various utilities used to create or maintain the database.

## mcmc_fit
`mcmc_fit` is a Python script developed by the authors to fit one or more Henyey-Greenstein distributions to an arbitrary scattering function. More details can be found [in its directory](mcmc_fit).

## fit_files
[This directory](fit_files) houses a collection of scripts which transform the output of `mcmc_fit` in a variety of ways. 

## Deirmendjian
`deirmendjian` is another Python script, which takes in information about spherical particles and generates a scattering function. More details can be found [in its directory](deirmendjian).
