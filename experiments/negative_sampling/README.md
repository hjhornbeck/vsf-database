The Volume Scattering Function Database
=======================================

# Sampling Algorithm Benchmarks
This code is the benchmarking testbed we used in our paper.[^1] The command-line options are fairly intuitive, and the code should provide enough hints as to which variables do what.

## Reproducibility
Development continues on this code. Since the submission it has been made multi-threaded on CPUs, and more algorithms can be executed on a GPU. It still contains a number of algorithms which had to be cut from our original paper, plus has some new ones slated for inclusion in a follow-up paper. This makes reproducing the original results a little more difficult than just cloning and compiling. There are two methods to retrieve the code used to create the benchmarks in the paper.

1. Retrieve [the last commit](https://gitlab.com/hjhornbeck/vsf-database/tree/686b12e8c141dc6cbba0109851a6cbc6ce946126/experiments/negative_sampling) of this code before the paper was published.
2. Use the DOI [published in the paper](https://doi.org/10.5281/zenodo.2629410) to retrieve a snapshot of the entire repository. This version has a few more commits than the prior one, but none of them relate to the benchmarking code.

You will need to create two directories, `bin` and `obj`, to get the code to compile. It can compile without modification via the CUDA SDK (V10.0.130) on Linux, however you may need to adjust the `CC` and `LN` variables in the `Makefile` depending on where you installed the SDK. It has not been tested on other operating systems.

From there, the following commands correspond to each entry of Table 1:

```
bin/sampling_benchmark -a 0 -s 8388608		# convex sampling
bin/sampling_benchmark -a 1 -s 8388608		# naive MCMC
bin/sampling_benchmark -a 10 -s 8388608		# rejection, 1HG
bin/sampling_benchmark -a 11 -s 8388608		# rejection, multi-HG
bin/sampling_benchmark -a 5 -s 8388608		# bisection
bin/sampling_benchmark -a 6 -s 8388608		# TOMS748
```

And these correspond to Table 2:

```
bin/sampling_benchmark -a 0 -s 8388608 -g 0	# convex sampling
bin/sampling_benchmark -a 10 -s 8388608 -g 0	# rejection, 1HG
bin/sampling_benchmark -a 11 -s 8388608	-g 0	# rejection, multi-HG
bin/sampling_benchmark -a 5 -s 8388608 -g 0	# bisection
```

The number after `-g` controls which GPU the code is executed on, and corresponds to the order returned by CUDA. To change the affine combination tested against, add `-v X` where `X` is between 0 and 2, inclusive.


# Citations and References
[^1]: Hornbeck H., Alim U. (2019) “[Improved Volume Scattering.](https://doi.org/10.1007/978-3-030-22514-8_6)” In: Gavrilova M., Chang J., Thalmann N., Hitzer E., Ishikawa H. (eds) Advances in Computer Graphics. CGI 2019. Lecture Notes in Computer Science, vol 11542. Springer, Cham
