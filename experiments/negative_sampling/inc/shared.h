/**************************************************************************
 *
 * Test out some methods for sampling a linear HG combination with negative
 *  coefficients. Now in C-like C++!
 * Author: Haysn Hornbeck
 * CC BY-SA 4.0
 *
 */

#ifndef SHARED_H
#define SHARED_H

/**************************************************************************
 * INCLUDES
 */

#include <cassert>
#include <cmath>
#include <cstdlib>

#include <atomic>
#include <chrono>
#include <iostream>
#include <map>
#include <memory>
#include <random>
#include <string>
#include <thread>
#include <vector>

#ifdef __NVCC__		// could we do GPU computation?
#include <cuda.h>

#else
#define HEMI_CUDA_DISABLE

#endif

#include "hemi/hemi.h"	// a thin wrapper for GPU computation
#include "hemi/array.h"
#include "hemi/device_api.h"
#include "hemi/launch.h"

// atomic
using std::atomic;

// chrono
using std::chrono::duration_cast;
using std::chrono::high_resolution_clock;
using std::chrono::nanoseconds;

// cmath
using std::cbrt;
using std::cos;

// iostream
using std::cout;
using std::cerr;
using std::endl;
using std::ostream;

// map
using std::map;

// memory
using std::dynamic_pointer_cast;
using std::make_shared;
using std::shared_ptr;

// random
using std::mt19937;
using std::uniform_real_distribution;

// string
using std::string;

// thread
using std::thread;

// vector
using std::vector;



/**************************************************************************
 * VARIABLES / TYPES
 */

typedef float fp;		// allows easy switching to double-precision

typedef unsigned char uchar;
typedef uint32_t uint;
typedef uint64_t ull;
typedef atomic<uint64_t> aull;

typedef unsigned long long int ulli;	// CUDA prefers this
typedef int64_t ll;

// hold the linear combination of HG's
typedef struct HEMI_ALIGN(8) LinearHG {

	uint count	= 0;		// 0 means nothing is stored
	fp* data	= nullptr;	// weight*, g*

	} LinearHG;

// set up a struct for storing the raw PDF
typedef struct PDF {

	int size	= -1;		// how many entries
	fp* angle	= nullptr;	// the angle that was sampled
	fp* likelihood	= nullptr;	//  and the likelihood at that point

	} PDF;

// finally, a "parameter football" to make parameter handling a lot easier
typedef struct Parameters {

	int algorithm	= 6;			// the default algorithm
	ull samples	= (1 << 21);		// the number of samples to draw
	ull seed	= 1;			// the RNG seed (don't worry, this'll change)
	int vsf		= 0;			// the linear HG to sample from, as a number

	fp epsilon	= 3e-6;			// what's our tolerance?

	LinearHG lhg;				// the linear HG to sample from

	uint binSize	= (1 << 16);		// how big a "bin" should we use?
	uint binCount	= (1 << 2);		// if using muliple bins, how many?

	uint DEGREE	= 3;			// the B-spline degree for interpolation
	
	uint MCMCburn	= 63;			// number of burn-in samples

	uint maxZeros	= 30;			// cap the number of zeros when root-finding

	fp mu		= 0.5;			// adjustable parameter of TOMS748
	uint maxIt	= 32;			// maximum number of iterations for TOMS748 (>7 is very rare)	

	uint verbose	= 0x0;			// bitmask of how many extra details should we print out
						// 1 = samples, 2 = PDF, 4 = CDF
	int useGPU	= -1;			// CPU or GPU execution? -1 = CPU
	uint CPUthreads	= 0;			// number of CPU threads to use. 0 = all

	bool stratified	= false;		// true = linear input between [0:1]

	} Parameters;
						// make printing this a LOT easier
ostream& operator<<( ostream& stream, const Parameters& params );

typedef struct FineTiming {

	ull transferToDev	= 0;		// in nanoseconds
	ull computation		= 0;
	ull transferFromDev	= 0;

	ll misses		= -1;		// number of rejected samples. -1 = invalid

	} FineTiming;
ostream& operator<<( ostream& stream, const FineTiming& params );

// would help to have PI lying around
static const fp PI		= (fp) 3.14159265358979323844;
static const fp QUARTER_INV_PI	= 0.25 / PI;

// we need a source of randomness
namespace Shared {

	extern mt19937 RNGsource;
	extern uniform_real_distribution<fp> fpDist;		// standard sampler
	}


/**************************************************************************
 * CLASSES
 */

// for functors, we need a base class to standardize them
struct functor { 
	
	const uint lhg_count;
	fp* lhg_data;		// all memory will be deleted elsewhere
	const uint samples;
	fp* output;

	functor( const uint a, fp* b, const uint c, fp* d ) :
		lhg_count{a}, lhg_data{b}, samples{c}, output{d} {}

	};

struct cpu_functor : public functor {

	cpu_functor( const uint a, fp* b, const uint c, fp* d ) :
		functor{a, b, c, d} {}

	// the CPU-exclusive entry point
	virtual void operator()(const uint offset = 0, 
			const uint stride = 1) const {}

	};

struct gpu_functor : public functor {

	gpu_functor( const uint a, fp* b, const uint c, fp* d ) :
		functor{a, b, c, d} {}

	// the GPU-exclusive one
	HEMI_LAMBDA virtual void operator()() const {}

	};


struct unified_functor : public functor {

	unified_functor( const uint a, fp* b, const uint c, fp* d ) :
		functor{a, b, c, d} {}

	// an entry point shared by both CPU and GPU
	HEMI_DEV_CALLABLE virtual void operator()(const uint offset, const uint stride) const {}

	};

// we need a class to parent all these samplers
class Sampler {

	protected:
		// every sampler needs a copy of the parameters
		Parameters params;

		// a simple wrapper for pointer testing
		bool testPointer( void* source, string msg ) const;

		// we need some way to allocate the results
		virtual bool allocResults( fp** output, ull** misses = nullptr ) const;

		// launch a series of functors
		void launch( cpu_functor& obj ) const;
		void launch( unified_functor& obj ) const;

	public:
		Sampler()		= default;	// constructors
		virtual ~Sampler()	= default;

		// in the form (type << 24) + id. Must be unique!
		virtual uint id() const		= 0;

		// a human-readable description of the sampler
		virtual string name() const	= 0;

		// set the computing device used. -1 = CPU, >=0 = GPU. True on success.
		virtual bool setCompute( const int device ) { return (device == -1); }
		virtual bool usingGPU() const { return false; }
		virtual uint getThreadCount() const;


		// prepare for a sampling run. Must be run before sample(). True on success.
		virtual bool setup( const Parameters& params, 
				FineTiming* ft = nullptr ) = 0;

		// execute a sampling run, returning "fp"'s as an array
		virtual fp* sample( FineTiming* ft = nullptr ) const = 0;

		// make timing a little easier
		static void incTimer( ull& start, ull& target );
		static ull setTimer();

	}; // Sampler

// plus a preprocessor macro to make initialization easier
#define REGISTER(ClassName) \
	bool ClassName::registered = SamplerFactory::add( \
			make_shared< ClassName >() );

// plus a factory class
class SamplerFactory {

		// store all the samplers we're aware of here
		static map<uint, shared_ptr<Sampler>>& samplers();

	public:
		SamplerFactory()	= delete;

		// add a Sampler to the factory. Returns true if successful.
		static bool add( shared_ptr<Sampler> object );

		// get the Nth sampler. N != id. nullptr if number >= count()
		static shared_ptr<Sampler> get( const uint number );

		// how many samplers are in the map?
		static uint count();

	}; // SamplerFactory

// a minor modification to make GPU-compatible samplers easier to create
class GPUsampler : public Sampler {

	protected:
		// track this so children know if they're running on CPU or GPU
		int device	= -1;
		

		// some shared routines to encourage code reuse
		virtual bool allocResults( fp** output, ull** misses = nullptr ) const;
		virtual bool retrieveResultsFromGPU( fp** output, ull** misses = nullptr ) const;

		// various allocation/copy wrappers
		fp* mallocGPU( const uint count, void* old_pointer = nullptr ) const;	// fp-sized units
		bool freeGPU( void* old_pointer ) const;

		bool memToGPU( const fp* source, const fp* target, const uint count ) const;
		fp* mallocToGPU( const fp* source, const uint count ) const;

		bool memFromGPU( const fp* source, const fp* target, const uint count ) const;

	public:
		// since this is likely identical between all samplers
		virtual bool setCompute( const int device );
		virtual bool usingGPU() const { return (device >= 0); }

	};


/**************************************************************************
 * SAMPLERS
 */

// the stock convex sampler. Broken, but a useful baseline.
class ConvexSampler : public GPUsampler {

		// the registration trigger
		static bool registered;

		// store these per-object, for flexibility
		fp* lhg_gpu	= nullptr;	// only valid on GPU

		fp* cdf_cpu	= nullptr;
		fp* cdf_gpu	= nullptr;

	public:
		~ConvexSampler();

		uint id() const;
		string name() const;

		bool setup( const Parameters& params, FineTiming* ft = nullptr );
		fp* sample( FineTiming* ft = nullptr ) const;
	};

// classic MCMC. The other variants crashed and burned, so we'll skip them
class MCMCsampler : public GPUsampler {

		// the registration trigger
		static bool registered;

	protected:

		fp* lhg_gpu	= nullptr;	// only valid on GPU

	public:
		~MCMCsampler();
		uint id() const;
		string name() const;

		bool setup( const Parameters& params, FineTiming* ft = nullptr );
		fp* sample( FineTiming* ft = nullptr ) const;

	}; // MCMCsampler


// a base class to contain helper functions
class RejectionBase : public GPUsampler {

	protected:

		fp* lhg_gpu	= nullptr;	// only valid on GPU

		// a helper for Halley's method
		void hengreen1D2D3D( const fp cos_x, const fp g, fp& firstD, 
				fp& secondD, fp& thirdD ) const;

		// one iteration of a vanilla version of Halley's method
		fp halleyZeros( const fp cos_x, const LinearHG& lhg ) const;

		// given two ends, figure out g's and scalars
		void calculateGandScale( const fp start, const fp end, 
				fp& g, fp& scalar );
		void calculateGandScale( const fp start, const fp end, LinearHG& lhg );

	public:
		~RejectionBase();

	}; // RejectionBase

// simple rejection with a Henyey-Greenstein envelope
class RejectionSimple : public RejectionBase {

		// the registration trigger
		static bool registered;

		// the envelope parameters
		fp g		= 0.f;
		fp scalar	= 1.f;


		// one iteration of Halley's method with a simple envelope
		fp halleyZeros( const fp cos_x, const LinearHG& lhg, 
				const fp g, const fp scalar ) const;

	public:
		uint id() const;
		string name() const;

		bool setup( const Parameters& params, FineTiming* ft = nullptr );
		fp* sample( FineTiming* ft = nullptr ) const;

	}; // RejectionSimple

// rejection with an envelope of multiple Henyey-Greensteins
class RejectionComposite : public RejectionBase {

		// the registration trigger
		static bool registered;

	protected:
		// the shape of the envelope
		LinearHG positive;
		fp* jump_cpu	= nullptr;

		fp* pos_gpu	= nullptr;
		fp* jump_gpu	= nullptr;

		// calculate the CDF of the positive envelope components
		void calculateJump();

	public:
		~RejectionComposite();
		virtual uint id() const;
		virtual string name() const;

		virtual bool setup( const Parameters& params, FineTiming* ft = nullptr );
		fp* sample( FineTiming* ft = nullptr ) const;

	};	// RejectionComposite

// same, but with a scaled envelope
class RejectionCompositeAdj : public RejectionComposite {

		// the registration trigger
		static bool registered;

		// the envelope parameters
		fp scalar	= 1.f;

		// a helper for discovering the scalar
		fp halleyZeros( const fp cos_x, const LinearHG& lhg,
				const LinearHG& minus, const fp scalar = 1.f ) const;

	public:
		uint id() const;
		string name() const;

		bool setup( const Parameters& params, FineTiming* ft = nullptr );
		// sample() is exactly the same

	};	// RejectionCompositeAdj

// rejection via a series of bins, first via fixing the bin width but inflating the rejection rate
class RejectionLinearBinned : public RejectionBase {

		// the registration trigger
		static bool registered;

		// stored boundaries and heights for the boxes
		fp* bounds_cpu	= nullptr;
		fp* bounds_gpu	= nullptr;

	public:
		~RejectionLinearBinned();
		uint id() const;
		string name() const;

		bool setup( const Parameters& params, FineTiming* ft = nullptr );
		fp* sample( FineTiming* ft = nullptr ) const;

	};	// RejectionLinearBinned

// TODO: fixing the rejection rate but varying the bin width


// Bisection. Simple, quick, effective
// first, Boost's version
class BisectionSampler : public Sampler {

		// the registration trigger
		static bool registered;

	public:
		uint id() const;
		string name() const;

		bool setup( const Parameters& params, FineTiming* ft = nullptr );
		fp* sample( FineTiming* ft = nullptr ) const;
	};

// now my implementation
class MyBisectionSampler : public GPUsampler {

		// the registration trigger
		static bool registered;

		// store these per-object, for flexibility
		fp* lhg_gpu	= nullptr;	// only valid on GPU

	public:
		uint id() const;
		string name() const;

		bool setup( const Parameters& params, FineTiming* ft = nullptr );
		fp* sample( FineTiming* ft = nullptr ) const;
	};

// TOMS748. Usually quicker than bisection. 
class TOMS748sampler : public Sampler {

		// the registration trigger
		static bool registered;

	public:
		uint id() const;
		string name() const;

		bool setup( const Parameters& params, FineTiming* ft = nullptr );
		fp* sample( FineTiming* ft = nullptr ) const;
	};

// now the one adapted from Boost's implementation
class MyTOMS748sampler : public GPUsampler {

		// the registration trigger
		static bool registered;

		// store these per-object, for flexibility
		fp* lhg_gpu	= nullptr;	// only valid on GPU

	public:
		uint id() const;
		string name() const;

		bool setup( const Parameters& params, FineTiming* ft = nullptr );
		fp* sample( FineTiming* ft = nullptr ) const;
	};


// a simple LUT approximation
class ApproxLUTsampler : public GPUsampler {

		// the registration trigger
		static bool registered;

	protected:
		uint LUTdim	= (1 << 16);	// separate this from the parameters to allow changes
		fp* TABLE_cpu	= nullptr;	// the primary LUT itself
		fp* TABLE_gpu	= nullptr;

						// fix the resolution of the PDF
		const uint dataSize	= 1 << 11;
		PDF data;			// we need to convert the HG into a LUT
		int lastRawPDF	= 0;		// a hint of where to sample the intermediate PDF 
		fp* pdfBuffer	= nullptr;	// cache the bootstrap PDF
		int pdfIndex	= -100;		//  and store where it windows
		int OFFSET	= 2;		// helps get the alignment right


		int clip( const int index ) const;	// a helper to make clipping to the data easier
		fp index2val( const int index ) const;	// convert an index to a cosine (allow sloppy bounds
							// calculate the weight via Cox-de Boor's algorithm
		fp coxDeBoor( const uint degree, const fp cos_x, const int index ) const;
							// evaluate the CDF of the cubic B-spline basis function at the given value
		fp cubic_cdf( const uchar type, const fp x ) const;
							// sample the incoming PDF via bisection. Optimized for random access.
		fp orderedSamplePDF( const fp cos_x ) const;
		void fixBuffer( const int index );	// fix the PDF buffer, if necessary
		fp full_cubic_cdf( int index );	// evaluate the full CDF at the given bootstrap PDF index
							// same as above, but this time we're doing a partial integral
		fp partial_cubic_cdf( const fp x, int index );

							// generate the incoming PDF from the provided HG function
		void generateRawPDF( const LinearHG& lhg );
							// create a partition in the LUT (NOTE: LUTend is EXCLUSIVE)
		fp createPartition( const uint LUTstart, uint& LUTend, const uint intPDFstart, const uint intPDFend );

	public:
		~ApproxLUTsampler();
		uint id() const;
		string name() const;

		virtual bool setup( const Parameters& params, FineTiming* ft = nullptr );
		virtual fp* sample( FineTiming* ft = nullptr ) const;

	}; // ApproxLUTsampler

// a LUT approximation that uses partitions
class ApproxPartLUTsampler : public ApproxLUTsampler {

		// the registration trigger
		static bool registered;

		fp* LUTmeta_cpu		= nullptr;	// some metadata for the sampler
		uint* LUToff_cpu	= nullptr;

		fp* LUTmeta_gpu		= nullptr;
		uint* LUToff_gpu	= nullptr;

	public:
		~ApproxPartLUTsampler();
		uint id() const;
		string name() const;

		bool setup( const Parameters& params, FineTiming* ft = nullptr );
		fp* sample( FineTiming* ft = nullptr ) const;

	}; // ApproxPartLUTsampler

// Linear LUT. A memory hog, but on paper the fastest of the bunch (in practice...)
class LinearLUTsamplerV2 : public GPUsampler {

		// the registration trigger
		static bool registered;

	protected:
		// store these per-object, for flexibility
		vector<fp> TABLE_cpu;
		fp* TABLE_gpu	= nullptr;
		uint LUTdim	= 0;
		uint LUToff	= 0;

	public:
		~LinearLUTsamplerV2();

		uint id() const;
		string name() const;

		virtual bool setup( const Parameters& params, FineTiming* ft = nullptr );
		virtual fp* sample( FineTiming* ft = nullptr ) const;
	};


// Arbitrary knot sampler, using linear b-splines and bisection. Slow but accurate.
class ArbKnotLinBiSampler : public GPUsampler {

		// the registration trigger
		static bool registered;

	protected:

		// the tables necessary to pull this off
		fp* angles_cpu	= nullptr;
		fp* pdf_cpu	= nullptr;
		fp* cdf_cpu	= nullptr;

		fp* angles_gpu	= nullptr;
		fp* pdf_gpu	= nullptr;
		fp* cdf_gpu	= nullptr;

		void generateRawPDF( const LinearHG& lhg );
		virtual bool generateLinCDF( const LinearHG& lhg );
		bool allocGPU();		// make this easier on our children

	public:
		~ArbKnotLinBiSampler();

		virtual uint id() const;
		virtual string name() const;

		virtual bool setup( const Parameters& params, FineTiming* ft = nullptr );
		virtual fp* sample( FineTiming* ft = nullptr ) const;
	};

// same, but with a jump table to short-circuit some bisection steps
class ArbKnotLinJumpBiSampler : public ArbKnotLinBiSampler {

		// the registration trigger
		static bool registered;

	protected:

		// the tables necessary to pull this off
		fp* jump_cpu	= nullptr;
		fp* jump_gpu	= nullptr;

		// build the jump table
		void generateJump();

	public:
		~ArbKnotLinJumpBiSampler();

		uint id() const;
		string name() const;

		virtual bool setup( const Parameters& params, FineTiming* ft = nullptr );
		virtual fp* sample( FineTiming* ft = nullptr ) const;
	};

// same, but with quadratic interpolation
class ArbKnotQuadJumpBiSampler : public ArbKnotLinJumpBiSampler {

		// the registration trigger
		static bool registered;

	protected:
		// we need to redo the CDF
		bool generateLinCDF( const LinearHG& lhg );

	public:
		uint id() const;
		string name() const;

// the setup routine should call the child's functions, not the parent's, so nothing changes
//		bool setup( const Parameters& params, FineTiming* ft = nullptr );
		fp* sample( FineTiming* ft = nullptr ) const;
	};

// speed up quadratic by using binned rejection sampling
class ArbKnotQuadRejSampler : public ArbKnotLinBiSampler {

		// the registration trigger
		static bool registered;

	protected:
		// we need to store our bin table
		fp* bounds_cpu	= nullptr;
		fp* bounds_gpu	= nullptr;

		uint* hints_cpu	= nullptr;	// plus hints to speed up binary search
		uint* hints_gpu	= nullptr;

		// speed isn't important, so prioritize flexibility
		void calculate_knots_values( fp& a, fp& b, fp& c, fp& d,
				fp& t, fp& u, fp& v, const uint index );
		fp quad_pdf( const fp a, const fp b, const fp c, const fp d, 
				const fp t, const fp u, const fp v, 
				const fp x ) const;
		fp quad_cdf( const fp a, const fp b, const fp c, const fp d, 
				const fp t, const fp u, const fp v ) const;
		fp quad_cdf( const fp a, const fp b, const fp c, const fp d, 
				const fp t, const fp u, const fp v, 
				const fp x ) const;
		fp interval_extrema( const fp a, const fp b, const fp c, const fp d, 
				const fp t, const fp u, const fp v ) const;
		bool generateEnvelope( const LinearHG& lhg );

	public:
		~ArbKnotQuadRejSampler();

		uint id() const;
		string name() const;

		bool setup( const Parameters& params, FineTiming* ft = nullptr );
		fp* sample( FineTiming* ft = nullptr ) const;
	};


/**************************************************************************
 * FUNCTIONS
 */

namespace Shared {

	// grab the greatest |g| from this linear combo
	fp greatestGbyG( const LinearHG& lhg );
	fp greatestGbyW( const LinearHG& lhg );

	// calculate a cosine-weighted Henyey-Greenstein. NOTE: scalar value was omitted
	HEMI_DEV_CALLABLE static fp hengreen0( const fp cos_x, const fp g ) {

        	if ((cos_x < -1.f) || (cos_x > 1.f))
	                return 0.f;

	        fp g2           = g*g;
	        fp denom        = 1.f + g2 - 2.*g*cos_x;
	        return (1.f - g2) * pow(denom, -1.5);           // TODO: sqrt method is faster?
	        }

	// same, but for all of a linear combination
	HEMI_DEV_CALLABLE static fp hengreen0( const fp cos_x, const LinearHG& lhg ) {

	        fp retVal = 0.f;
	        for (int i = 0; i < lhg.count; i++)

	                retVal += lhg.data[i] * Shared::hengreen0( cos_x, lhg.data[i + lhg.count] );

		return retVal;
		}


	// same, but for integrals (KLUDGE: divide by two for a proper integral)
	HEMI_DEV_CALLABLE static fp hengreen1I(		const fp cos_x, const fp g ) {

		if ((cos_x < -1.f) || (cos_x > 1.f))
	              return 0.f;

	        fp g2           = g*g;
	        fp denom        = g * sqrt( 1.f + g2 - 2.*g*cos_x );    // interior is never negative!
	        return          (1.f - g2) / denom;
		}

	HEMI_DEV_CALLABLE static fp hengreen1I(		const fp cos_x, const LinearHG& lhg ) {

	        fp retVal = 0.f;
	        for (int i = 0; i < lhg.count; i++)

	                retVal += lhg.data[i] * hengreen1I( cos_x, lhg.data[i + lhg.count] );

	        return retVal;
		}

	HEMI_DEV_CALLABLE static void hengreen1Idata(	const LinearHG& lhg, fp& max, fp& min ) {

		// TODO: swap in a quicker calculation
		max     = hengreen1I(  1.f, lhg );
		min     = hengreen1I( -1.f, lhg );

		}

	// sample from a Henyey-Greenstein, given a random number (taken from Cycles' code)
	HEMI_DEV_CALLABLE static fp hengreenSamp( const fp x, const fp g ) {

	        if ( (x < 0.f) || (x > 1.f) )
	                return 0;

	        fp g2   = g*g;
	        fp k    = (1.f - g2) / (1.f - g + 2.*g*x);
	        fp retVal       = (1.f + g2 - k*k) / (2. * g);

	        if( retVal < -1.f )     // in case of numeric precision issues
			retVal = -1.f;
	        else if( retVal > 1.f )
	                retVal = 1.f;

	        return retVal;
		}

	} // Shared namespace


#endif
