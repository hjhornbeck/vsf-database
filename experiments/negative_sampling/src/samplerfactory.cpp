/**************************************************************************
 * INCLUDES
 */

#include "shared.h"

/**************************************************************************
 * VARIABLES
 */

// the map is stored locally, to prevent race conditions

/**************************************************************************
 * FUNCTIONS
 */

map<uint, shared_ptr<Sampler>>& SamplerFactory::samplers() {

	static map<uint, shared_ptr<Sampler>> internal;
	return internal;
	}


bool SamplerFactory::add( shared_ptr<Sampler> object ) {

	// standard null checking
	if ( object == nullptr )
		return false;

	// check if this ID already exists in the DB
	if( samplers().find(object->id()) != samplers().end() )
		return false;

	samplers()[object->id()] = object;
	return true;

	} // SamplerFactory::add

shared_ptr<Sampler> SamplerFactory::get( const uint number ) {

	// slow? Yes. But speed isn't important here.
	uint index	= 0;
	auto it		= samplers().begin();

	while( it != samplers().end() ) {

		if( index == number )
			return it->second;

		index++;
		it++;
		}

	// none found?
	return nullptr;

	} // SamplerFactory::get

uint SamplerFactory::count() {	return samplers().size(); }
