/**************************************************************************
 * INCLUDES
 */

#include "shared.h"
using Shared::hengreen0;

#ifdef __NVCC__
#include <curand_kernel.h>
#endif


/**************************************************************************
 * VARIABLES / TYPES
 */

REGISTER(MCMCsampler)


/**************************************************************************
 * FUNCTORS
 */

struct generic_mcmc_cpu : public cpu_functor { 

	ull* missTotal;
	const ull seed;
	const uint burnin;

	generic_mcmc_cpu(const int a, fp* b, const uint c, fp* d,
			ull* e, const ull f, const uint g ) :
		cpu_functor(a, b, c, d), missTotal{e}, seed{f}, burnin{g} {}

	void operator()(const uint offset = 0, const uint stride = 1) const;

	};

// we need two of these, thanks to RNG and atomics
struct generic_mcmc_gpu : public gpu_functor {

	ull* missTotal;
	const ull seed;
	const uint burnin;

	generic_mcmc_gpu(const int a, fp* b, const uint c, fp* d,
			ull* e, const ull f, const uint g ) :
		gpu_functor(a, b, c, d), missTotal{e}, seed{f}, burnin{g} {}

	HEMI_LAMBDA void operator()() const;

	};


// now fill out the core functor routines
void generic_mcmc_cpu::operator()(const uint offset, const uint stride) const {

	LinearHG stored = { lhg_count, lhg_data };
	ull misses	= 0;
	mt19937 RNGsource( seed + offset + 1 ); // ensure thread-safe operation

	uint o		= offset;

	while( o < samples ) {		// while we're within bounds

		fp best		= output[o]*2 - 1;
		fp bestLike	= hengreen0( best, stored );

		for( uint i = 0; i < burnin; i++ ) {

			fp proposal	= Shared::fpDist( RNGsource )*2 - 1;
			fp propLike	= hengreen0( proposal, stored );

			if( (propLike > bestLike) || (Shared::fpDist( RNGsource )*bestLike < propLike) ) {

				best		= proposal;
				bestLike	= propLike;
				}

			} // while( burning in sampler )

		misses		+= burnin;
		output[o]	 = best;
		o		+= stride;
		}

	// now to safely tally up the misses
	missTotal[offset]	= misses;

	} // generic_mcmc_cpu

HEMI_LAMBDA void generic_mcmc_gpu::operator()() const {

#ifdef __NVCC__
	// set up the RNG
	curandState_t RNGstate;
	curand_init( seed, hemi::globalThreadIndex(), 0, &RNGstate );

	// other variables we'll need
	LinearHG stored = { lhg_count, lhg_data };
	ull misses	= 0;

	uint o		= hemi::globalThreadIndex();
	uint stride	= hemi::globalThreadCount();

	while( o < samples ) {		// while we're within bounds

		fp best		= output[o]*2 - 1;
		fp bestLike	= hengreen0( best, stored );

		for( uint i = 0; i < burnin; i++ ) {

			fp proposal	= curand_uniform(&RNGstate)*2 - 1;
			fp propLike	= hengreen0( proposal, stored );

			if( (propLike > bestLike) || (curand_uniform(&RNGstate)*bestLike < propLike) ) {

				best		= proposal;
				bestLike	= propLike;
				}

			} // while( burning in sampler )

		misses		+= burnin;
		output[o]	 = best;
		o		+= stride;
		}

	// now to safely tally up the misses
	atomicAdd( (ulli*)missTotal, misses );
#endif
	} // generic_mcmc_gpu

/**************************************************************************
 * SIMPLE FUNCTIONS
 */

uint MCMCsampler::id() const {		return (1 << 24) | 1; }

string MCMCsampler::name() const {
	return "generic Metropolis MCMC sampling"; }


// need a destructor, too
MCMCsampler::~MCMCsampler() {

#ifdef __NVCC__
	if( lhg_gpu )
		cudaFree(lhg_gpu);
#endif
	}


/**************************************************************************
 * MCMCsampler
 */

bool MCMCsampler::setup( const Parameters& p, FineTiming* t ) {

	if( !registered ) {

		cout << "ERROR: Why isn't MCMCsampler registered?!" << endl;
		return false;
		}

	// capture some basic timing data
	ull startTime;
	if( t != nullptr )
		startTime	= setTimer();

	// store some key variables
	params		= p;

	// transfer data to the GPU, if necessary
	if( usingGPU() ) {

		freeGPU(lhg_gpu);
		lhg_gpu = mallocToGPU( params.lhg.data, params.lhg.count*2 );
		if( !testPointer(lhg_gpu, "ERROR: Could not allocate memory for the LHG on the GPU!") )
			return false;

		hemi::deviceSynchronize();
		}

	if( t != nullptr )
		incTimer( startTime, t->transferToDev );

	return true;
	}

fp* MCMCsampler::sample( FineTiming* t ) const {

	// some preliminary timing setup
	ull startTime;

	if( t != nullptr )
		startTime	= setTimer();

	fp* output	= nullptr;
	ull* misses	= nullptr;

	if( !allocResults( &output, &misses ) )
		return nullptr;

	if( t != nullptr )
		incTimer( startTime, t->transferToDev );

	// time to start computing!
	if( usingGPU() ) {
#ifdef __NVCC__

		generic_mcmc_gpu calc( params.lhg.count, lhg_gpu, params.samples, output,
			misses, params.seed, params.MCMCburn + 1 );
		hemi::launch(calc);

		hemi::deviceSynchronize();		// force all memory activity to complete
#else
		cout << "ERROR: How did you manage to enable the GPU without using nvcc?" << endl;
		return nullptr;
#endif
		}
	else {

		generic_mcmc_cpu calc( params.lhg.count, params.lhg.data, params.samples, output,
			misses, params.seed, params.MCMCburn + 1 );
		launch( calc );

		// finally, consolidate the miss counts to unify GPU and CPU handling
		for( uint i = 1; i < getThreadCount(); i++ )
			misses[0]       += misses[i];

		}

	if( t != nullptr )
		incTimer( startTime, t->computation );

	// copy results and return
	if( usingGPU() && !retrieveResultsFromGPU( &output, &misses ) ) {

		cout << "ERROR: Could not copy the results back from the GPU." << endl;
		t->misses       = -1;
		return nullptr;
		}

	if( t != nullptr ) {

		incTimer( startTime, t->transferFromDev );
		t->misses = (ll) *misses;
		}

	if( misses != nullptr )
		delete[] misses;
	return output;

	} // MCMCsampler::sample
