/**************************************************************************
 * INCLUDES
 */

#include "shared.h"

#include <iomanip>
using std::setw;	// helps with printing parameters


/**************************************************************************
 * VARIABLES
 */

mt19937 Shared::RNGsource;	// for randomness
uniform_real_distribution<fp> Shared::fpDist(0.0,1.0);


/**************************************************************************
 * FUNCTIONS
 */

// print out a parameter set
ostream& operator<<( ostream& stream, const Parameters& p ) {

	stream << setw(12) << "algorithm = "	<< p.algorithm << endl;
	stream << setw(12) << "samples = "	<< p.samples << endl;
	stream << setw(12) << "seed = "		<< p.seed << endl;

	stream << setw(12) << "vsf = "		<< p.vsf << endl;

	stream << setw(12) << "epsilon = "	<< p.epsilon << endl;
	// skip printing the actual LinearHG, the VSF should be enough info

	stream << setw(12) << "binSize = "	<< p.binSize << endl;
	stream << setw(12) << "binCount = "	<< p.binCount << endl;

	stream << setw(12) << "DEGREE = "	<< p.DEGREE << endl;

	stream << setw(12) << "MCMCburn = "	<< p.MCMCburn << endl;

	stream << setw(12) << "maxZeros = "	<< p.maxZeros << endl;

	stream << setw(12) << "mu = "		<< p.mu << endl;
	stream << setw(12) << "maxIt = "	<< p.maxIt << endl;

	stream << setw(12) << "verbose = "	<< p.verbose << endl;
	stream << setw(12) << "useGPU = "	<< p.useGPU << endl;
	stream << setw(12) << "CPUthreads = "	<< p.CPUthreads << endl;

	return stream;
	}

ostream& operator<<( ostream& stream, const FineTiming& ft ) {

	if( ft.transferToDev > 0 )
		stream << "Time spent transfering to the device = " << ft.transferToDev << "ns" << endl;

	if( ft.computation > 0 )
		stream << "Time spent in computation = " << ft.computation << "ns" << endl;

	if( ft.transferFromDev > 0 )
		stream << "Time spent transfering from the device = " << ft.transferFromDev << "ns" << endl;

	if( ft.misses >= 0 )
		stream << "Rejected samples = " << ft.misses << endl;

	return stream;
	}


// grab the greatest |g| from this linear combo
fp Shared::greatestGbyG( const LinearHG& lhg ) {

	fp retVal	= lhg.data[lhg.count];
	for( int i = 1; i < lhg.count; i++ ) {

		int index	= i + lhg.count;
		if( fabs(lhg.data[index]) > fabs(retVal) )
			retVal	= lhg.data[index];

		}

	return retVal;
	}

// similar, but grab the g with the greatest |w|
fp Shared::greatestGbyW( const LinearHG& lhg ) {

	fp maxW		= lhg.data[0];
	fp retVal	= lhg.data[lhg.count];

	for( int i = 1; i < lhg.count; i++ ) {

		if( fabs(maxW) < fabs(lhg.data[i]) ) {

			retVal	= lhg.data[ i+lhg.count ];
			maxW	= lhg.data[ i ];
			}
		}

	return retVal;
	}


// first derivative
fp hengreen1D( const fp cos_x, const fp g ) {

	if ((cos_x < -1.f) || (cos_x > 1.f))
		return 0.f;

	fp g2		= g*g;
	fp denom	= 1.f + g2 - 2.*g*cos_x;
	return 		3.*g*(1.f - g2) * pow(denom, -2.5);
	}
	
// same, but for a combo
fp hengreen1D( const fp cos_x, const LinearHG& lhg ) {

	fp retVal = 0.f;
	for (int i = 0; i < lhg.count; i++)

		retVal += lhg.data[i] * hengreen1D( cos_x, lhg.data[ i+lhg.count ] );

	return retVal;
	}

// second derivative
fp hengreen2D( const fp cos_x, const fp g ) {

	if ((cos_x < -1.f) || (cos_x > 1.f))
		return 0.f;

	fp g2		= g*g;
	fp denom	= 1.f + g2 - 2.*g*cos_x;
	return 		15.*g2*(1.f - g2) * pow(denom, -3.5);
	}
	
// third derivative
fp hengreen3D( const fp cos_x, const fp g ) {

	if ((cos_x < -1.f) || (cos_x > 1.f))
		return 0.f;

	fp g2		= g*g;
	fp denom	= 1.f + g2 - 2.*g*cos_x;
	return 		105.*g*g2*(1.f - g2) * pow(denom, -4.5);
	}
	
