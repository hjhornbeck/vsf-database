/**************************************************************************
 * INCLUDES
 */

#include "shared.h"

#include <iomanip>
using std::defaultfloat;
using std::scientific;
using std::setprecision;	// help with printing floating point numbers

#ifdef __NVCC__
#include <curand_kernel.h>
#endif




/**************************************************************************
 * VARIABLES / TYPES
 */

REGISTER(ArbKnotLinBiSampler)
REGISTER(ArbKnotLinJumpBiSampler)

REGISTER(ArbKnotQuadJumpBiSampler)
REGISTER(ArbKnotQuadRejSampler)


/**************************************************************************
 * FUNCTORS
 */

struct arbLin : public unified_functor {

	const fp* ANGLES	= nullptr;
	const fp* LIKELI	= nullptr;
	const fp* cdf		= nullptr;
	const uint LUTdim;
	const fp epsilon;

	arbLin(const uint a, fp* b, const uint c, fp* d,
			const uint e, const fp* f, const fp* g, const fp* h, 
			const fp i ) :
		unified_functor(a, b, c, d), LUTdim{e}, ANGLES{f}, LIKELI{g}, cdf{h}, 
			epsilon{i} {}

	// no RNG = the same code across GPU and CPU!
	HEMI_DEV_CALLABLE void operator()(const uint offset = 0, const uint stride = 1) const;

	};

struct arbJumpLin : public unified_functor {

	const fp* ANGLES	= nullptr;
	const fp* LIKELI	= nullptr;
	const fp* cdf		= nullptr;
	const fp* jump		= nullptr;
	const uint LUTdim;
	const uint bins;
	const fp epsilon;

	arbJumpLin(const uint a, fp* b, const uint c, fp* d,
			const uint e, const fp* f, const fp* g, const fp* h, const fp* i,
			const uint J, const fp k ) :
		unified_functor(a, b, c, d), LUTdim{e}, ANGLES{f}, LIKELI{g}, cdf{h}, jump{i},
			bins{J}, epsilon{k} {}

	// no RNG = the same code across GPU and CPU!
	HEMI_DEV_CALLABLE void operator()(const uint offset = 0, const uint stride = 1) const;

	};

struct arbJumpQuad : public unified_functor {

	const fp* ANGLES	= nullptr;
	const fp* LIKELI	= nullptr;
	const fp* cdf		= nullptr;
	const fp* jump		= nullptr;
	const uint LUTdim;
	const uint bins;
	const fp epsilon;

	arbJumpQuad(const uint a, fp* b, const uint c, fp* d,
			const uint e, const fp* f, const fp* g, const fp* h, const fp* i,
			const uint J, const fp k ) :
		unified_functor(a, b, c, d), LUTdim{e}, ANGLES{f}, LIKELI{g}, cdf{h}, jump{i},
			bins{J}, epsilon{k} {}

	HEMI_DEV_CALLABLE void operator()(const uint offset = 0, const uint stride = 1) const;

	};

struct arbQuadRej_cpu : public cpu_functor {

	const uint LUTdim;
	const fp* ANGLES	= nullptr;
	const fp* LIKELI	= nullptr;

	const uint bins;
	const fp* bounds	= nullptr;
	const uint* hints	= nullptr;

	ull* missTotal		= nullptr;
	const ull seed;


	arbQuadRej_cpu(const uint a, fp* b, const uint c, fp* d,
			ull* e, const ull f, const uint g, const fp* h, const fp* i, const uint j,
			const fp* k, const uint* l ) :
		cpu_functor(a, b, c, d), missTotal{e}, seed{f}, 
			LUTdim{g}, ANGLES{h}, LIKELI{i}, 
			bins{j}, bounds{k}, hints{l} {}

	// alas, we need RNG
	void operator()(const uint offset = 0, const uint stride = 1) const;

	};

struct arbQuadRej_gpu : public gpu_functor {

	const uint LUTdim;
	const fp* ANGLES	= nullptr;
	const fp* LIKELI	= nullptr;

	const uint bins;
	const fp* bounds	= nullptr;
	const uint* hints	= nullptr;

	ull* missTotal		= nullptr;
	const ull seed;


	arbQuadRej_gpu(const uint a, fp* b, const uint c, fp* d,
			ull* e, const ull f, const uint g, const fp* h, const fp* i, const uint j,
			const fp* k, const uint* l ) :
		gpu_functor(a, b, c, d), missTotal{e}, seed{f}, 
			LUTdim{g}, ANGLES{h}, LIKELI{i}, 
			bins{j}, bounds{k}, hints{l} {}

	HEMI_LAMBDA void operator()() const;

	};


// now the meat of it
HEMI_DEV_CALLABLE void arbLin::operator()(const uint o, const uint s) const {

	uint offset;
	uint stride;

	if( s == 0 ) {

		offset	= hemi::globalThreadIndex();
		stride	= hemi::globalThreadCount();
		}
	else {
		offset	= o;
		stride	= s;
		}

	// just dive right in
	while( offset < samples ) {

		fp target	 = output[offset];

		uint leftInd    = 0;		// binary search to bracket within the CDF
		uint rightInd   = LUTdim - 1;
		while( (rightInd-leftInd) > 1 ) {

			uint middle = (leftInd + rightInd) >> 1;
			if( cdf[middle] < target )
				leftInd		= middle;
			else
				rightInd	= middle;
			}

		// continue the search for the fractional amount
		// for x' = x-a, b' = b-a, integral = x'/b'*(.5*x'*(v-u) + u*b')
		fp left		= 0;
		fp leftVal	= LIKELI[leftInd];
		fp leftCulm	= cdf[leftInd];

		fp right	= ANGLES[rightInd] - ANGLES[leftInd];
		fp rightBra	= right;
		fp rightVal	= LIKELI[rightInd];

		// further speed up calculation by memorizing a few things
		double invRB	= 1. / rightBra;		// 1/b'
		double halfVU	= 0.5 * (rightVal - leftVal);	// .5*(v-u)
		double UrB	= rightBra * leftVal;		// u*b'

		fp culm		= leftCulm;			// just stash a temporary value
		fp tarAdj	= target - leftCulm;		// save an op

		while( fabs(tarAdj-culm) > epsilon ) {

			double middle	= 0.5*(left + right);

			culm		= middle*invRB*( middle*halfVU + UrB );
			if( culm < tarAdj )
				left	= middle;
			else
				right	= middle;
			}

		// KLUDGE: should be close enough. Needs a LERP if not
		output[offset]	= ANGLES[leftInd] + 0.5*(left + right);
		offset		+= stride;

		}

	} // arbLin

HEMI_DEV_CALLABLE void arbJumpLin::operator()(const uint o, const uint s) const {

	uint offset;
	uint stride;

	if( s == 0 ) {

		offset	= hemi::globalThreadIndex();
		stride	= hemi::globalThreadCount();
		}
	else {
		offset	= o;
		stride	= s;
		}

	while( offset < samples ) {

		fp target	 = output[offset];

		uint leftInd    = 0;		// binary search to bracket within the cdf
		uint rightInd   = LUTdim - 1;

		// use the jump table to narrow down this binary search
		uint bin        = target * (bins + 1);
		if( (bin > 0) && (jump[bin-1] > 0) )	// need to add a slight overlap
			leftInd		= jump[bin-1] - 1;

		if ( bin < bins )
			rightInd	= jump[bin];

		assert( cdf[leftInd] < target );		// a few asserts never hurt nobody
		assert( cdf[rightInd] > target );

		// you know the rest from here
		while( (rightInd-leftInd) > 1 ) {

			uint middle = (leftInd + rightInd) >> 1;
			if( cdf[middle] < target )
				leftInd		= middle;
			else
				rightInd	= middle;
			}

		// continue the search for the fractional amount
		// for x' = x-a, b' = b-a, integral = x'/b'*(.5*x'*(v-u) + u*b')
		// inverting that, we find x' = (sqrt((u*u*b' + 2*int*(v-u))*b') - u*b')/(v-u)
		fp bp		= ANGLES[rightInd] - ANGLES[leftInd];
		fp u		= LIKELI[leftInd];
		fp v		= LIKELI[rightInd];
		fp integral	= target - cdf[leftInd];

		fp vu		= v - u;	// precalculate some things
		fp bpu		= bp * u;

		output[offset]	= ANGLES[leftInd] + (sqrt(bp*(bpu*u + 2.*integral*vu)) - bpu)/vu;
		offset		+= stride;

		}

	} // arbJumpLin

HEMI_DEV_CALLABLE void arbJumpQuad::operator()(const uint o, const uint s) const {

	uint offset;
	uint stride;

	if( s == 0 ) {

		offset	= hemi::globalThreadIndex();
		stride	= hemi::globalThreadCount();
		}
	else {
		offset	= o;
		stride	= s;
		}

	while( offset < samples ) {

		fp target	 = output[offset];

		uint bInd    = 0;		// binary search to bracket within the CDF
		uint cInd   = LUTdim - 1;

		// use the jump table to narrow down this binary search
		uint bin        = target * (bins + 1);
		if( (bin > 0) && (jump[bin-1] > 0) )	// need to add a slight overlap
			bInd		= jump[bin-1] - 1;

		if ( bin < bins )
			cInd	= jump[bin];

		assert( cdf[bInd] < target );		// a few asserts never hurt nobody
		assert( cdf[cInd] > target );

		// you know the rest from here
		while( (cInd-bInd) > 1 ) {

			uint middle = (bInd + cInd) >> 1;
			if( cdf[middle] < target )
				bInd		= middle;
			else
				cInd	= middle;
			}

		uint aInd	= 0;		// gather up other indicies
	       	if( bInd > 0 )
			aInd	= bInd - 1;

		uint dInd = cInd + 1;
		if( dInd >= LUTdim )
			dInd = LUTdim - 1;

		// now knot locations, relative to a:
		fp b		= ANGLES[bInd] - ANGLES[aInd];
		fp c		= ANGLES[cInd] - ANGLES[aInd];
		fp d		= ANGLES[dInd] - ANGLES[aInd];

		fp t		= LIKELI[bInd];		// knots are offset by 1 to their corresponding values
		fp u		= LIKELI[cInd];
		fp v		= LIKELI[dInd];

		double b2	= b * b;	// start building up
		double b3	= b2 * b;
		double invDenom	= 1. / (3*c*(b - c)*(b2 - b*c - b*d + c*d));
		double bc	= b - c;
		double bcd	= bc - d;

		double cvbc	= c * v * bc;	// still building
		double ubc	= u * bc;
		double b2bcbdcd	= b2 - b*c - b*d + c*d; 
		double c_3	= 3.*c;
		double b2bc	= b*c - b2;

		double left	= b;
		double right	= c;
		fp tarAdj	= target - cdf[bInd];
		fp culm		= cdf[bInd];		// just stash a temporary value

		while( fabs(tarAdj-culm) > epsilon ) {

			double x 	= 0.5*(left + right);
			double x2	= x * x;
			double x3	= x2 * x;

			culm	= (t*(b3 + c_3*(b2bc - c*x + x2) - x3)*b2bcbdcd +
				   ubc*(-b3*bcd + c_3*d*x*(x - b) + x3*bcd) - 
				   cvbc*(b3 + 3*b*x*(x - b) - x3)) * 
				invDenom;

			assert( culm >= 0 );	// sanity check

			if( culm < tarAdj )
				left	= x;
			else
				right	= x;
			}

		// KLUDGE: should be close enough. Needs a LERP if not
		output[offset]	= ANGLES[bInd] + 0.5*(left + right);
		offset		+= stride;

		}

	} // arbJumpQuad

void arbQuadRej_cpu::operator()(const uint offset, const uint stride) const {

	mt19937 RNGsource( seed + offset + 1 );	// ensure thread-safe operation

	// other variables we'll need
	ull misses	= 0;

	uint o		= offset;

	while( o < samples ) {			// while we're within bounds

		// pick a random bin from the acceleration structure
		fp target	= output[o] * bins;
		uint bin	= target;
		fp frac		= target - (fp)bin;

		// figure out the bounds of that bin
		uint offset     = bin*2;
		fp left;
		if( bin == 0 )
			left	= -1.f;		// start from -1
		else
			left	= bounds[ offset - 2 ];
		
		fp right	= bounds[ offset ];

		// lerp the remainder to find our true theta
		fp proposal	= (1.f - frac)*left + frac*right;
		fp thresh	= Shared::fpDist(RNGsource) * bounds[offset + 1];

		// binary search to bracket the PDF (hint: use hints)
		uint bInd;
		if( bin == 0 )
			bInd	= 0;
		else
			bInd = hints[ bin-1 ] - 1;


		uint cInd	= hints[ bin ];

		assert( ANGLES[bInd] <= proposal );
		assert( ANGLES[cInd] >= proposal );

		while( (cInd - bInd) > 1 ) {

			uint middle	= (bInd + cInd) >> 1;
			if( ANGLES[middle] <= proposal )
				bInd	= middle;
			else
				cInd	= middle;
			}

		// evaluate the quadratic at that point (relative to a)
		double a;
		if( bInd == 0 )
			a	= ANGLES[bInd];
		else
			a	= ANGLES[bInd-1];

		double b	= ANGLES[bInd] - a;
		double c	= ANGLES[cInd] - a;

		double d;
		if( (cInd + 1) < LUTdim )
			d	= ANGLES[cInd+1] - a;
		else
			d	= ANGLES[cInd] - a;

		double t	= LIKELI[bInd];
		double u	= LIKELI[cInd];
		double v;
		if( (cInd + 1) < LUTdim )
			v	= LIKELI[cInd+1];
		else
			v	= u;
		
		// do some consolidation
		double xa	= proposal - a;
		double bx	= b - xa;
		double bd	= b - d;
		double cx	= c - xa;

		// (c*v*(b-x)**2 - t*(b-d)*(c-x)**2 - u*(c*(b-x)*(d-x) + x*(b-d)*(c-x)))/(c*(b-c)*(b-d))
		fp dataLike	= (c*v*bx*bx - t*bd*cx*cx - u*(c*bx*(d-xa) + xa*bd*cx)) /
			(c * (b-c) * bd);

		assert( (dataLike/bounds[offset + 1]) <= 1.f );

		// hopefully we're within the envelope
		while( thresh > dataLike ) {

			misses++;
			target		= Shared::fpDist(RNGsource) * bins;
			bin		= target;
			frac		= target - (fp)bin;

			offset		= bin*2;
			if( bin == 0 )
				left	= -1.f;
			else
				left	= bounds[ offset - 2 ];

			right		= bounds[ offset ];

			proposal	= (1.f - frac)*left + frac*right;
			thresh		= Shared::fpDist(RNGsource) * bounds[offset + 1];

			// copy-paste code is ugly, but we want to maximize speed
			// binary search to bracket the PDF (hint: use hints)
			if( bin == 0 )
				bInd	= 0;
			else
				bInd = hints[ bin-1 ] - 1;

			cInd		= hints[ bin ];

			assert( ANGLES[bInd] <= proposal );
			assert( ANGLES[cInd] >= proposal );

			while( (cInd - bInd) > 1 ) {

				uint middle	= (bInd + cInd) >> 1;
				if( ANGLES[middle] <= proposal )
					bInd	= middle;
				else
					cInd	= middle;
				}

			// evaluate the quadratic at that point (relative to a)
			if( bInd == 0 )
				a	= ANGLES[bInd];
			else
				a	= ANGLES[bInd-1];

			b		= ANGLES[bInd] - a;
			c		= ANGLES[cInd] - a;

			if( (cInd + 1) < LUTdim )
				d	= ANGLES[cInd+1] - a;
			else
				d	= ANGLES[cInd] - a;

			t	= LIKELI[bInd];
			u	= LIKELI[cInd];
			if( (cInd + 1) < LUTdim )
				v	= LIKELI[cInd+1];
			else
				v	= u;
		
			// do some consolidation
			xa	= proposal - a;
			bx	= b - xa;
			bd	= b - d;
			cx	= c - xa;

			dataLike	= (c*v*bx*bx - t*bd*cx*cx - u*(c*bx*(d-xa) + xa*bd*cx)) /
				(c * (b-c) * bd);

			assert( (dataLike/bounds[offset + 1]) <= 1.f );

			}

		output[o]	 = proposal;
		o		+= stride;

		} // while( we have data to write )

	// now to safely tally up the misses
	missTotal[offset]	= misses;

	} // arbQuadRej_cpu

HEMI_LAMBDA void arbQuadRej_gpu::operator()() const {
	
#ifdef __NVCC__
	curandState_t RNGstate;
	curand_init( seed, hemi::globalThreadIndex(), 0, &RNGstate );

	// other variables we'll need
	ull misses	= 0;

	uint o		= hemi::globalThreadIndex();
	uint stride	= hemi::globalThreadCount();

	while( o < samples ) {			// while we're within bounds

		// pick a random bin from the acceleration structure
		fp target	= output[o] * bins;
		uint bin	= target;
		fp frac		= target - (fp)bin;

		// figure out the bounds of that bin
		uint offset     = bin*2;
		fp left;
		if( bin == 0 )
			left	= -1.f;		// start from -1
		else
			left	= bounds[ offset - 2 ];
		
		fp right	= bounds[ offset ];

		// lerp the remainder to find our true theta
		fp proposal	= (1.f - frac)*left + frac*right;
		fp thresh	= curand_uniform(&RNGstate) * bounds[offset + 1];

		// binary search to bracket the PDF (hint: use hints)
		uint bInd;
		if( bin == 0 )
			bInd	= 0;
		else
			bInd = hints[ bin-1 ] - 1;

		uint cInd	= hints[ bin ];

		while( (cInd - bInd) > 1 ) {

			uint middle	= (bInd + cInd) >> 1;
			if( ANGLES[middle] <= proposal )
				bInd	= middle;
			else
				cInd	= middle;
			}

		// evaluate the quadratic at that point (relative to a)
		double a;
		if( bInd == 0 )
			a	= ANGLES[bInd];
		else
			a	= ANGLES[bInd-1];

		double b	= ANGLES[bInd] - a;
		double c	= ANGLES[cInd] - a;

		double d;
		if( (cInd + 1) < LUTdim )
			d	= ANGLES[cInd+1] - a;
		else
			d	= ANGLES[cInd] - a;

		double t	= LIKELI[bInd];
		double u	= LIKELI[cInd];
		double v;
		if( (cInd + 1) < LUTdim )
			v	= LIKELI[cInd+1];
		else
			v	= u;
		
		// do some consolidation
		double xa	= proposal - a;
		double bx	= b - xa;
		double bd	= b - d;
		double cx	= c - xa;

		// (c*v*(b-x)**2 - t*(b-d)*(c-x)**2 - u*(c*(b-x)*(d-x) + x*(b-d)*(c-x)))/(c*(b-c)*(b-d))
		fp dataLike	= (c*v*bx*bx - t*bd*cx*cx - u*(c*bx*(d-xa) + xa*bd*cx)) /
			(c * (b-c) * bd);

		// hopefully we're within the envelope
		while( thresh > dataLike ) {

			misses++;
			target		= curand_uniform(&RNGstate) * bins;
			bin		= target;
			frac		= target - (fp)bin;

			offset		= bin*2;
			if( bin == 0 )
				left	= -1.f;
			else
				left	= bounds[ offset - 2 ];

			right		= bounds[ offset ];

			proposal	= (1.f - frac)*left + frac*right;
			thresh		= curand_uniform(&RNGstate) * bounds[offset + 1];

			// copy-paste code is ugly, but we want to maximize speed
			// binary search to bracket the PDF (hint: use hints)
			if( bin == 0 )
				bInd	= 0;
			else
				bInd = hints[ bin-1 ] - 1;

			cInd		= hints[ bin ];

			while( (cInd - bInd) > 1 ) {

				uint middle	= (bInd + cInd) >> 1;
				if( ANGLES[middle] <= proposal )
					bInd	= middle;
				else
					cInd	= middle;
				}

			// evaluate the quadratic at that point (relative to a)
			if( bInd == 0 )
				a	= ANGLES[bInd];
			else
				a	= ANGLES[bInd-1];

			b		= ANGLES[bInd] - a;
			c		= ANGLES[cInd] - a;

			if( (cInd + 1) < LUTdim )
				d	= ANGLES[cInd+1] - a;
			else
				d	= ANGLES[cInd] - a;

			t	= LIKELI[bInd];
			u	= LIKELI[cInd];
			if( (cInd + 1) < LUTdim )
				v	= LIKELI[cInd+1];
			else
				v	= u;
		
			// do some consolidation
			xa	= proposal - a;
			bx	= b - xa;
			bd	= b - d;
			cx	= c - xa;

			dataLike	= (c*v*bx*bx - t*bd*cx*cx - u*(c*bx*(d-xa) + xa*bd*cx)) /
				(c * (b-c) * bd);
			}

		output[o]	 = proposal;
		o		+= stride;

		} // while( we have data to write )

	// now to safely tally up the misses
	atomicAdd( (ulli*)missTotal, misses );
#endif
	} // arbQuadRej_gpu

/**************************************************************************
 * SIMPLE FUNCTIONS
 */

uint ArbKnotLinBiSampler::id() const {			return (4 << 24) | 192; }
uint ArbKnotLinJumpBiSampler::id() const {		return (4 << 24) | 193; }

uint ArbKnotQuadJumpBiSampler::id() const {		return (4 << 24) | 204; }
uint ArbKnotQuadRejSampler::id() const {		return (4 << 24) | 205; }

string ArbKnotLinBiSampler::name() const {
	return "arbitrary-knot look-up tables, linear interpolation, bisection"; }
string ArbKnotLinJumpBiSampler::name() const {
	return "arbitrary-knot look-up tables, linear interpolation, bisection, jump table"; }
string ArbKnotQuadJumpBiSampler::name() const {
	return "arbitrary-knot look-up tables, quadratic interpolation, bisection, jump table"; }
string ArbKnotQuadRejSampler::name() const {
	return "arbitrary-knot look-up tables, quadratic interpolation, rejection sampling"; }

ArbKnotLinBiSampler::~ArbKnotLinBiSampler() {

	if( angles_cpu != nullptr )
		delete[] angles_cpu;
	if( pdf_cpu != nullptr )
		delete[] pdf_cpu;
	if( cdf_cpu != nullptr )
		delete[] cdf_cpu;

	if( angles_gpu != nullptr )
		freeGPU( angles_gpu );
	if( pdf_gpu != nullptr )
		freeGPU( pdf_gpu );
	if( cdf_gpu != nullptr )
		freeGPU( cdf_gpu );

	}

ArbKnotLinJumpBiSampler::~ArbKnotLinJumpBiSampler() {

	if( jump_cpu != nullptr )
		delete[] jump_cpu;

	if( jump_gpu != nullptr )
		freeGPU( jump_gpu );

	}

ArbKnotQuadRejSampler::~ArbKnotQuadRejSampler() {

	if( bounds_cpu != nullptr )
		delete[] bounds_cpu;
	if( hints_cpu != nullptr )
		delete[] hints_cpu;

	if( bounds_gpu != nullptr )
		freeGPU( bounds_gpu );
	if( hints_gpu != nullptr )
		freeGPU( hints_gpu );

	}


/**************************************************************************
 * ArbKnotLinBiSampler
 */

void ArbKnotLinBiSampler::generateRawPDF( const LinearHG& lhg ) {

	if( angles_cpu != nullptr )
		delete[] angles_cpu;
	if( pdf_cpu != nullptr )
		delete[] pdf_cpu;

	angles_cpu	=  new fp[params.binSize];
	pdf_cpu		=  new fp[params.binSize];

	// assumption check
	assert( params.binSize > 0 );
	assert( angles_cpu != nullptr );
	assert( pdf_cpu != nullptr );

	for( uint it = 0; it < params.binSize; it++ ) {

		// NOTE: gotta flip this, the algorithm breaks otherwise
		fp angle	= cos( (fp)(params.binSize - it - 1) /
				(fp)(params.binSize - 1)*PI );
		angles_cpu[it]	= angle;
		pdf_cpu[it]	= Shared::hengreen0( angle, lhg );
		assert(		pdf_cpu[it] > 0 );
		}

	assert( angles_cpu[0] < 0.f );

	// DEBUG
	if( params.verbose & 0x1 ) {

		cout << "DEBUG: raw PDF follows" << endl;
		for( uint i = 0; i < params.binSize; i++ )
			cout << angles_cpu[i] << "\t" << pdf_cpu[i] << endl;
		cout << endl;
		}

	} // ArbKnotLinBiSampler::generateRawPDF()


bool ArbKnotLinBiSampler::generateLinCDF( const LinearHG& lhg ) {

	if( cdf_cpu != nullptr )
		delete[] cdf_cpu;

	cdf_cpu		=  new fp[params.binSize];
	assert( cdf_cpu != nullptr );

	cdf_cpu[0]	= 0.f;		// force this
	fp error	= 0.f;		// see http://alex.uwplse.org/2015/10/16/improving-accuracy-summation.html
	fp oldSum	= 0.f;

	if( params.verbose & 0x2 ) {

		cout << "DEBUG: incoming TSV to help track precision issues" << endl;
		cout << "u\tv\ta\tb\t1/(b-a)\t.5*(v-u)\tu*(b-a)\tefficient calc\tlong calc" << setprecision(12) << endl;
		}

	for( uint i = 1; i < params.binSize; i++ ) {    // now convert to a CDF

		double u	= pdf_cpu[i-1];		// we're in no rush, use the full formula
		double a	= angles_cpu[i-1];	//  MUST use doubles here, floats don't have enough precision
		double v	= pdf_cpu[i];		//  also, on CPU, juggling between float/double is actually ~1% slower
		double b	= angles_cpu[i];
		double x	= b;			// makes copy-pasting easier

		// check this first
		if( b == a ) {

			cout << "ERROR: binSize is too big! Try lowering it." << endl;
			return false;
			}

		double current	= (0.5*( x*x*(v-u) + a*a*(u+v) ) + u*b*(x-a) - x*a*v)/(b-a);

		if( params.verbose & 0x2 )		// dodge scoping issues
			cout << u << "\t" << v << "\t" << a << "\t" << b << "\t" << scientific << 1./(b-a) <<
				"\t" << .5*(v-u) << "\t" << u*(b-a) << "\t" << setprecision(12) << current <<
				"\t" << (0.5*(x*x*v - x*x*u + a*a*u + a*a*v) + u*b*x - u*b*a - x*a*v)/(b-a) <<
				endl << defaultfloat;

		fp newSum	= oldSum + ((fp)current) + error;
		cdf_cpu[i]	= newSum;

		// sanity checks
		assert(		cdf_cpu[i] > 0 );
		assert(		cdf_cpu[i] >= cdf_cpu[i-1] );

		error		= current - ((newSum - oldSum) - error);
		oldSum		= newSum;

		}

	for( uint i = 1; i < params.binSize; i++ )	// second round, to normalize
		cdf_cpu[i]	/= oldSum;

	cout << setprecision(6);			// back to the default

	return true;

	} // ArbKnotLinBiSampler::generateLinCDF

bool ArbKnotLinBiSampler::allocGPU() {

	freeGPU( angles_gpu );
	angles_gpu = mallocToGPU( angles_cpu, params.binSize );
	if( !testPointer(angles_gpu, "ERROR: Could not allocate memory for the angles on the GPU!") )
		return false;

	freeGPU( pdf_gpu );
	pdf_gpu = mallocToGPU( pdf_cpu, params.binSize );
	if( !testPointer(pdf_gpu, "ERROR: Could not allocate memory for the PDF on the GPU!") )
		return false;

	if( cdf_cpu != nullptr ) {	// make this optional, for ArbKnotQuadRejSampler

		freeGPU( cdf_gpu );
		cdf_gpu = mallocToGPU( cdf_cpu, params.binSize );
		if( !testPointer(cdf_gpu, "ERROR: Could not allocate memory for the CDF on the GPU!") )
			return false;
		}

	return true;
	}

bool ArbKnotLinBiSampler::setup( const Parameters& p, FineTiming* ft ) {

	if( !registered ) {

		cout << "ERROR: Why isn't ArbKnotLinBiSampler registered?!" << endl;
		return false;
		}

	// capture some basic timing data
	ull startTime;
	if( ft != nullptr )
		startTime	= setTimer();

	// store some key variables
	params		= p;
	
	generateRawPDF(	params.lhg );
	if( !generateLinCDF(params.lhg) )
		return false;

	if( ft != nullptr )
		incTimer( startTime, ft->computation );

	if( usingGPU() ) {
		
		if( allocGPU() )	// make this easier on our child class
			hemi::deviceSynchronize();
		else
			return false;
		}

	if( ft != nullptr )
		incTimer( startTime, ft->transferToDev );

	return true;

	} // ArbKnotLinBiSampler::setup

fp* ArbKnotLinBiSampler::sample( FineTiming* t ) const {

	// some preliminary timing setup
	ull startTime;
	if( t != nullptr )
		startTime	= setTimer();

	fp* output	= nullptr;
	if( !allocResults( &output, nullptr ) )
		return nullptr;

	if( t != nullptr )
		incTimer( startTime, t->transferToDev );

	// time to start computing!
	if( usingGPU() ) {
#ifdef __NVCC__
		arbLin calc( 0, nullptr, params.samples, output, 
				params.binSize, angles_gpu, pdf_gpu, cdf_gpu, params.epsilon );
		hemi::launch( calc, 0, 0 );

		hemi::deviceSynchronize();
#else
		cout << "ERROR: How did you manage to enable the GPU without using nvcc?" << endl;
		return nullptr;
#endif
		}
	else {
		arbLin calc( 0, nullptr, params.samples, output, 
				params.binSize, angles_cpu, pdf_cpu, cdf_cpu,
				params.epsilon ); 
		launch( calc );

		}

	if( t != nullptr ) 
		incTimer( startTime, t->computation );

	if( usingGPU() && !retrieveResultsFromGPU( &output ) ) {

		cout << "ERROR: Could not copy the results back from the GPU." << endl; 
		return nullptr; 
		}

	if( t != nullptr ) 
		incTimer( startTime, t->transferFromDev );

	return output;

	} // ArbKnotLinBiSampler::sample



/**************************************************************************
 * ArbKnotLinJumpBiSampler
 */

void ArbKnotLinJumpBiSampler::generateJump() {

	assert( cdf_cpu != nullptr );			// this must be set up first!

	if ( jump_cpu )					// clean up any existing data
		delete[] jump_cpu;
	jump_cpu		= new fp[params.binCount];

	uint index	= 0;
	for( uint i = 0; i < params.binCount; i++ ) {

		fp target	= (fp)(i + 1) / (fp)(params.binCount + 1);

		while( cdf_cpu[index] < target )		// boring linear search
			index++;

		jump_cpu[i]		= index;

		}

	} // generateJump()


bool ArbKnotLinJumpBiSampler::setup( const Parameters& p, FineTiming* ft ) {

	if( !registered ) {

		cout << "ERROR: Why isn't ArbKnotLinJumpBiSampler registered?!" << endl;
		return false;
		}

	// capture some basic timing data
	ull startTime;
	if( ft != nullptr )
		startTime	= setTimer();

	// store some key variables
	params		= p;
	
	generateRawPDF(	params.lhg );
	if( !generateLinCDF(params.lhg) )
		return false;
	generateJump();

	if( ft != nullptr )
		incTimer( startTime, ft->computation );

	
	if( usingGPU() ) {
		
		if( !allocGPU() )
			return false;
       
		freeGPU( jump_gpu );
		jump_gpu = mallocToGPU( jump_cpu, params.binSize );
		if( !testPointer(jump_gpu, "ERROR: Could not allocate memory for the angles on the GPU!") )
			return false;

		hemi::deviceSynchronize();
		}

	if( ft != nullptr )
		incTimer( startTime, ft->transferToDev );

	return true;

	} // ArbKnotLinJumpBiSampler::setup

fp* ArbKnotLinJumpBiSampler::sample( FineTiming* t ) const {

	// some preliminary timing setup
	ull startTime;
	if( t != nullptr )
		startTime	= setTimer();

	fp* output	= nullptr;
	if( !allocResults( &output, nullptr ) )
		return nullptr;

	if( t != nullptr )
		incTimer( startTime, t->transferToDev );

	// time to start computing!
	if( usingGPU() ) {
#ifdef __NVCC__
		arbJumpLin calc( 0, nullptr, params.samples, output, 
				params.binSize, angles_gpu, pdf_gpu, cdf_gpu, jump_gpu, 
				params.binCount, params.epsilon );
		hemi::launch( calc, 0, 0 );

		hemi::deviceSynchronize();
#else
		cout << "ERROR: How did you manage to enable the GPU without using nvcc?" << endl;
		return nullptr;
#endif
		}
	else {
		arbJumpLin calc( 0, nullptr, params.samples, output, 
				params.binSize, angles_cpu, pdf_cpu, cdf_cpu, jump_cpu, 
				params.binCount, params.epsilon );
		launch( calc );

		}

	if( t != nullptr ) 
		incTimer( startTime, t->computation );

	if( usingGPU() && !retrieveResultsFromGPU( &output ) ) {

		cout << "ERROR: Could not copy the results back from the GPU." << endl; 
		return nullptr; 
		}

	if( t != nullptr ) 
		incTimer( startTime, t->transferFromDev );

	return output;

	} // ArbitraryJumpKnotSampler::sample

/**************************************************************************
 * ArbKnotQuadJumpBiSampler
 */

bool ArbKnotQuadJumpBiSampler::generateLinCDF( const LinearHG& lhg ) {

	if( cdf_cpu != nullptr )
		delete[] cdf_cpu;

	cdf_cpu		=  new fp[params.binSize];
	assert( cdf_cpu != nullptr );

	cdf_cpu[0]	= 0.f;		// force this
	fp error	= 0.f;		// see http://alex.uwplse.org/2015/10/16/improving-accuracy-summation.html
	fp oldSum	= 0.f;

	for( uint i = 1; i < params.binSize; i++ ) {    // now convert to a CDF

		// shifting the knots is mandatory for IEEE binary64, to guarantee sufficient precision
		double a;
		if( i == 1 )				// duplicate the ends if missing
			a	= angles_cpu[0];
		else
			a	= angles_cpu[i-2];

		double b;
		if( i == 1 )
			b	= 0;
		else
			b	= angles_cpu[i-1] - a;

		double c	= angles_cpu[i] - a;

		double d;
		if( i+1 >= params.binSize )		// duplicate the ends if missing
			d	= angles_cpu[params.binSize-1] - a;
		else
			d	= angles_cpu[i+1] - a;

		double t	= pdf_cpu[i-1];		// now their associated values
		double u	= pdf_cpu[i];
		double v;
		if( i+1 >= params.binSize )		// duplicate the rightmost end if missing
			v	= pdf_cpu[params.binSize - 1];
		else
			v	= pdf_cpu[i+1];

		double x	= c;			// b <= x <= c
							//  here, we're integrating the entire interval
							
		double x2	= x*x;			// why bother with pow?
		double x3	= x2*x;
		double b2	= b*b;
		double b3	= b2*b;

		// check the knots aren't too close together
		if( (b == c) && ((b == 0) || (c == d)) ) {

			cout << "ERROR: binSize is too big! Try lowering it." << endl;
			return false;
			}

		// thank goodness for SymPy
		double current	= (t*(b3 + 3*c*(-b2 + b*c - c*x + x2) - x3)*(b2 - b*c - b*d + c*d) +
				   u*(b - c)*(b3*(-b + c + d) + 3*c*d*x*(-b + x) + x3*(b - c - d)) - 
				   v*c*(b - c)*(b3 + 3*b*x*(-b + x) - x3)) / 
			(3*c*(b - c)*(b2 - b*c - b*d + c*d)); 

		fp newSum	= oldSum + ((fp)current) + error;
		cdf_cpu[i]	= newSum;

		// sanity checks
		assert(		cdf_cpu[i] > 0 );
		assert(		cdf_cpu[i] >= cdf_cpu[i-1] );

		error		= current - ((newSum - oldSum) - error);
		oldSum		= newSum;

		}

	for( uint i = 1; i < params.binSize; i++ )	// second round, to normalize
		cdf_cpu[i]	/= oldSum;

	cout << setprecision(6);			// back to the default

	return true;

	} // ArbKnotQuadJumpBiSampler::generateLinCDF

fp* ArbKnotQuadJumpBiSampler::sample( FineTiming* t ) const {

	// some preliminary timing setup
	ull startTime;
	if( t != nullptr )
		startTime	= setTimer();

	fp* output	= nullptr;
	if( !allocResults( &output, nullptr ) )
		return nullptr;

	if( t != nullptr )
		incTimer( startTime, t->transferToDev );

	// time to start computing!
	if( usingGPU() ) {
#ifdef __NVCC__
		arbJumpQuad calc( 0, nullptr, params.samples, output, 
				params.binSize, angles_gpu, pdf_gpu, cdf_gpu, jump_gpu, 
				params.binCount, params.epsilon );
		hemi::launch( calc, 0, 0 );

		hemi::deviceSynchronize();
#else
		cout << "ERROR: How did you manage to enable the GPU without using nvcc?" << endl;
		return nullptr;
#endif
		}
	else {
		arbJumpQuad calc( 0, nullptr, params.samples, output, 
				params.binSize, angles_cpu, pdf_cpu, cdf_cpu, jump_cpu, 
				params.binCount, params.epsilon );
		launch( calc );

		}

	if( t != nullptr ) 
		incTimer( startTime, t->computation );

	if( usingGPU() && !retrieveResultsFromGPU( &output ) ) {

		cout << "ERROR: Could not copy the results back from the GPU." << endl; 
		return nullptr; 
		}

	if( t != nullptr ) 
		incTimer( startTime, t->transferFromDev );

	return output;

	} // ArbKnotQuadJumpBiSampler::sample

/**************************************************************************
 * ArbKnotQuadRejSampler
 */

fp ArbKnotQuadRejSampler::quad_pdf( const fp a, const fp B, const fp C, const fp D,
	      const fp t, const fp u, const fp v, const fp X ) const {

	double x = X - a;			// subtract off a
	double b = B - a;
	double c = C - a;
	double d = D - a;

	assert( x >= b );			// catch a few bugs
	assert( x <= c );

	double bd	= b - d;		// build up the expression
	double denom	= c * (b-c) * bd;
	if( denom == 0 ) {

		cout << "ERROR: binSize is too big! Try lowering it." << endl;
		assert( false );
		}

	double bx	= b - x;
	double cx	= c - x;

	return (float)( (c*v*bx*bx - t*bd*cx*cx - u*(c*bx*(d - x) + x*bd*cx)) / denom );

	} // ArbKnotQuadRejSampler::quad_pdf

fp ArbKnotQuadRejSampler::quad_cdf( const fp a, const fp B, const fp C, const fp D,
	      const fp t, const fp u, const fp v ) const {

	double b = B - a;			// subtract off a
	double c = C - a;
	double d = D - a;

	double denom = 3. * c * (b-d);		// check the denom
	if( denom == 0 ) {

		cout << "ERROR: binSize is too big! Try lowering it." << endl;
		assert( false );
		}

	double b2	= b*b;			// start consolidating
	double b3	= b2*b;
	double c2	= c*c;
	double c3	= c2*c;

	// take advantage of cancellation to speed this up
	return (float)( (t*(b3 - 2*b2*c - b2*d + b*c2 + 2*b*c*d - c2*d) + 
		u*(b2*d - b3 + b*c*d + c3 - 2*c2*d) + 
		v*(2*b*c2 - b2*c - c3)) / denom );
	}

fp ArbKnotQuadRejSampler::quad_cdf( const fp a, const fp B, const fp C, const fp D,
	      const fp t, const fp u, const fp v, const fp X ) const {

	double x = X - a;			// subtract off a
	double b = B - a;
	double c = C - a;
	double d = D - a;

	assert( x >= b );			// catch a few bugs
	assert( x <= c );

	double x2	= x*x;
	double x3	= x2*x;
	double b2	= b*b;
	double b3	= b2*b;

	// check the knots aren't too close together
	if( (b == c) && ((b == 0) || (c == d)) ) {

		cout << "ERROR: binSize is too big! Try lowering it." << endl;
		assert( false );
		}

	// thank goodness for SymPy
	return (float)( (t*(b3 + 3*c*(-b2 + b*c - c*x + x2) - x3)*(b2 - b*c - b*d + c*d) +
		   u*(b - c)*(b3*(-b + c + d) + 3*c*d*x*(-b + x) + x3*(b - c - d)) - 
		   v*c*(b - c)*(b3 + 3*b*x*(-b + x) - x3)) / 
		(3*c*(b - c)*(b2 - b*c - b*d + c*d)) );

	}

void ArbKnotQuadRejSampler::calculate_knots_values( fp& a, fp& b, fp& c, fp& d, 
		fp& t, fp& u, fp& v, 
		const uint index ) {

	if( index == 1 )
		a	= angles_cpu[0];
	else
		a	= angles_cpu[index-2];

	b	= angles_cpu[index-1];
	t	= pdf_cpu[index-1];
	c	= angles_cpu[index];
	u	= pdf_cpu[index];

	if( (index + 1) >= params.binSize ) {

		d	= angles_cpu[params.binSize-1];
		v	= pdf_cpu[params.binSize-1];
		}
	else {

		d	= angles_cpu[index+1];
		v	= pdf_cpu[index+1];
		}

	} // ArbKnotQuadRejSampler::calculate_knots_values()

fp ArbKnotQuadRejSampler::interval_extrema( const fp a, const fp B, const fp C, const fp D,
	      const fp t, const fp u, const fp v ) const {

	double b = B - a;			// subtract off a
	double c = C - a;
	double d = D - a;
	
	// consolidation might cause precision issues, and we're in no hurry
	return (float)( c*(b*t - b*v - d*t + d*u) / (b*t - b*u + c*u - c*v - d*t + d*u) ) + a;

	} // ArbKnotQuadRejSampler::interval_extrema

bool ArbKnotQuadRejSampler::generateEnvelope( const LinearHG& lhg ) {

	bool retVal	= true;

	if( bounds_cpu != nullptr )		// allocate memory
		delete[] bounds_cpu;
	bounds_cpu	= new fp[ params.binCount * 2 ];

	if( hints_cpu != nullptr )
		delete[] hints_cpu;
	hints_cpu	= new uint[ params.binCount ];

						// memoize this to speed computation
	fp* cdf_cpu	= new fp[ params.binSize ];		

	fp invBinCount	= 1.f / (fp)params.binCount;
	fp location	= -1.;			// fractional location in the CDF
	double total	= 0;			// current CDF sum
	fp minAccept	= 1.f;			// minimum acceptance rate seen

	fp a, b, c, d, t, u, v;			// b-spline parameters

	cdf_cpu[0]	= 0;			// first, generate a sum to help normalize all this
	for( uint index = 1; index < params.binSize; index++ ) {

		calculate_knots_values( a,b,c,d, t,u,v, index );
		total		+= quad_cdf( a,b,c,d, t,u,v );
		cdf_cpu[index]	= (fp)total;

		}

	uint index	= 1;			// second, fill in the bins
	bounds_cpu[1]	= pdf_cpu[0];		// special-case the first boundary
	for( uint bin = 0; bin < params.binCount; bin++ ) {

		fp target		= (bin + 1) * total * invBinCount;

						// accumulate until we'd jump the target
		while( cdf_cpu[index] <= target ) {

			uint offset = 2*bin + 1;
			if( pdf_cpu[index] > bounds_cpu[offset] )
				bounds_cpu[offset] = pdf_cpu[index];

			calculate_knots_values( a,b,c,d, t,u,v, index );
			fp x = interval_extrema( a,b,c,d, t,u,v );
			if( (x > b) && (x < c) && 
					(location >= b) && (x >= location) ) {
				
				fp possibleBound = quad_pdf( a,b,c,d, t,u,v, x );
				if( possibleBound > bounds_cpu[offset])
					bounds_cpu[offset] = possibleBound;
				}

			index++;
			if( index >= params.binSize ) {

				index--;	// ensure this remains valid
				break;		//  but otherwise bail
				}
			}

		if( cdf_cpu[index] == target )
			location	= c;
		else {				// binary search to find the exact location

			calculate_knots_values( a,b,c,d, t,u,v, index );
			fp left		= b;
			if( location > left )
				left	= location;
			fp right	= c;

			fp culmGoal	= target - cdf_cpu[index-1];

			while( (right-left) > params.epsilon ) {

				fp middle 	= .5*(left + right);
				fp culmProposal	= quad_cdf( a,b,c,d, t,u,v, middle );

				if( culmProposal > culmGoal )
					right	= middle;
				else
					left	= middle;
				}

			// do a lerp to approximate the true solution
			double leftInt	= quad_cdf( a,b,c,d, t,u,v, left );
			double rightInt	= quad_cdf( a,b,c,d, t,u,v, right );
			location	= (float)( ((double)culmGoal - (double)leftInt) / (rightInt - leftInt)
						* ((double)right - (double)left) ) + left;
			}

		// store this data for the current bin
		uint offset		= 2*bin;
		bounds_cpu[offset]	= location;

		// compensate for numeric inaccuracy
		if( (bin+1) >= params.binCount )	
			hints_cpu[bin]	= params.binSize - 1;
		else
			hints_cpu[bin]	= index;

		// update the stored maximum, if applicable
		fp possibleBound = quad_pdf( a,b,c,d, t,u,v, location );
		if( possibleBound > bounds_cpu[offset+1])
			bounds_cpu[offset+1] = possibleBound;
		if( (bin + 1) < params.binSize )	// propagate it forward, if possible
			bounds_cpu[offset+3] = possibleBound;

		fp extrema	= interval_extrema( a,b,c,d, t,u,v );
		if( (extrema >= b) && (extrema <= location) ) {

			possibleBound = quad_pdf( a,b,c,d, t,u,v, extrema );
			if( possibleBound > bounds_cpu[offset+1])
				bounds_cpu[offset+1] = possibleBound;
			}

		// also update the highest acceptance rate seen
		fp left;
		if( bin == 0 )
			left	= -1.f;
		else
			left	= bounds_cpu[offset - 2];
		fp right	= location;

		// KLUDGE: the ideal area is within epsilon of the actual area
		fp acceptRate	= total * invBinCount / ((right-left) * bounds_cpu[offset+1]);
		if( acceptRate < minAccept )
			minAccept = acceptRate;

		}

	// finally, adjust each bin's ceiling so their rejection rates are the same
	for( uint bin = 0; bin < params.binCount; bin++ ) {

		fp left		= -1.f;
		if( bin > 0 )
			left	= bounds_cpu[ bin*2 - 2 ];
		fp right	= bounds_cpu[ bin*2 ];

		bounds_cpu[ bin*2 + 1 ] = total * invBinCount / ((right-left) * minAccept);

		}

	return retVal;

	} // ArbKnotQuadRejSampler::generateEnvelope

bool ArbKnotQuadRejSampler::setup( const Parameters& p, FineTiming* ft ) {

	if( !registered ) {

		cout << "ERROR: Why isn't ArbKnotQuadRejSampler registered?!" << endl;
		return false;
		}

	// capture some basic timing data
	ull startTime;
	if( ft != nullptr )
		startTime	= setTimer();

	params		= p;
	
	generateRawPDF(	params.lhg );
	if( !generateEnvelope(params.lhg) )
		return false;

	if( ft != nullptr )
		incTimer( startTime, ft->computation );

	
	if( usingGPU() ) {
		
		if( !allocGPU() )
			return false;
       
		freeGPU( bounds_gpu );
		bounds_gpu = mallocToGPU( bounds_cpu, params.binSize*2 );
		if( !testPointer(bounds_gpu, "ERROR: Could not allocate memory for the bins on the GPU!") )
			return false;

		freeGPU( hints_gpu );
		hints_gpu = (uint*) mallocToGPU( (fp*)hints_cpu, sizeof(uint)*params.binSize/sizeof(fp) );
		if( !testPointer( (fp*) hints_gpu, "ERROR: Could not allocate memory for the hints on the GPU!") )
			return false;

		hemi::deviceSynchronize();
		}

	if( ft != nullptr )
		incTimer( startTime, ft->transferToDev );

	return true;

	} // ArbKnotQuadRejSampler::setup

fp* ArbKnotQuadRejSampler::sample( FineTiming* t ) const {

	// some preliminary timing setup
	ull startTime;
	if( t != nullptr )
		startTime	= setTimer();

	fp* output	= nullptr;
	ull* misses	= nullptr;

	if( !allocResults( &output, &misses ) )
		return nullptr;

	if( t != nullptr )
		incTimer( startTime, t->transferToDev );

	// time to start computing!
	if( usingGPU() ) {
#ifdef __NVCC__
		arbQuadRej_gpu calc( 0, nullptr, params.samples, output, 
				misses, params.seed,
				params.binSize, angles_gpu, pdf_gpu, 
				params.binCount, bounds_gpu, hints_gpu );
		hemi::launch( calc );

		hemi::deviceSynchronize();
#else
		cout << "ERROR: How did you manage to enable the GPU without using nvcc?" << endl;
		return nullptr;
#endif
		}
	else {
		arbQuadRej_cpu calc( 0, nullptr, params.samples, output, 
				misses, params.seed,
				params.binSize, angles_cpu, pdf_cpu, 
				params.binCount, bounds_cpu, hints_cpu );
		launch( calc );

		// finally, consolidate the miss counts to unify GPU and CPU handling
		for( uint i = 1; i < getThreadCount(); i++ )
			misses[0]	+= misses[i];

		}

	if( t != nullptr ) 
		incTimer( startTime, t->computation );

	if( usingGPU() && !retrieveResultsFromGPU( &output, &misses ) ) {

		cout << "ERROR: Could not copy the results back from the GPU." << endl; 
		t->misses	= -1;
		return nullptr; 
		}

	if( t != nullptr ) {

		incTimer( startTime, t->transferFromDev );
		t->misses = (ull) *misses;
		}

	if( misses != nullptr )
		delete[] misses;
	return output;

	} // ArbKnotQuadRejSampler::sample
