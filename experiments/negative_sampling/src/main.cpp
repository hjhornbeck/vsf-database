/**************************************************************************
 * INCLUDES
 */

#include "shared.h"

#include <iomanip>

// for command-line parsing
#include <boost/program_options/option.hpp>
#include <boost/program_options/options_description.hpp>
#include <boost/program_options/parsers.hpp>
#include <boost/program_options/positional_options.hpp>
#include <boost/program_options/variables_map.hpp>

namespace po = boost::program_options;


// <iomanip>
using std::setw;


/**************************************************************************
 * VARIABLES / TYPES
 */

// set up some linear HG's to sample from
fp mat06[] = {	
		-0.6033295825349737,  1.8314803736250862, -2.6504313579637646, 2.4222805668736522,	// weights
		 0.05981173707285781, 0.1566684047941715,  0.2893850250614432, 0.3070069333161101	// g's
		};


fp mat33[] = {
		 0.01525224868390501551, -0.49309487749307124603,  1.00886082867963726114, -0.53030274510155330787,
		-2.03391158275431140816,  6.57943593787538236001, -4.12496610035671128414,  0.57872629046672308974,

		-0.818496277910187, -0.5684962772015145, -0.47500929345813897, -0.33789398309329827,
		 0.5731561289370747, 0.6778898575997304,  0.7228078814492348,   0.9728078814492348
		};


fp difficult[] = {
		 0.08907889097496813, -0.4884205834744561, 0.8817618401076659, 0.517579852391822,
		-0.33905086194559686,  0.291148103561683,  0.6043903097689696, 0.9913068319809905
		};


fp rayleigh[] = {
		 0.5,    0.5,
		-0.3065, 0.3065
		};

/**************************************************************************
 * FUNCTIONS
 */

void printAlgorithms() {

	cout << "ALGORITHMS" << endl;
	for( uint i = 0; i < SamplerFactory::count(); i++ ) {

		shared_ptr<Sampler> s	= SamplerFactory::get(i);

		// should never happen, but the check is cheap
		if( s == nullptr )
			break;

		cout << setw(3) << i << " (CPU";
#ifdef __NVCC__
		if( dynamic_cast<GPUsampler*>(s.get()) )	// cast for GPU support
			cout << " GPU";
		else
#endif
			cout << "    ";				// improved formatting

		cout << "): " << SamplerFactory::get(i)->name() << "." << endl;
		}

	} // printAlgorithms()

/**************************************************************************
 * MAIN
 */

int main( int argc, char** argv ) {

	// set the important variables we'll need
	Parameters params;
	params.seed	= Sampler::setTimer();		// don't go with the default here


	// parse the command line
	po::options_description general("General Options");
	general.add_options()
	("help", "Show this help")				// looks weird, but it's legit syntax
	("print,p", "Print the stored samples")
	("algorithm,a", po::value< int >(&params.algorithm), "The algorithm to use (start from zero)")
	("samples,s", po::value< ull >(&params.samples), "The number of samples to draw")
	("seed,S", po::value< ull >(&params.seed), "The RNG seed. If unspecified, use the system time")
	("vsf,v", po::value< int >(&params.vsf), "The linear HG VSF to sample from (start from zero)")
	("gpu,g", po::value< int >(&params.useGPU), "Use the given GPU, or the CPU if equal to -1")
	("threads,t", po::value< uint >(&params.CPUthreads), "Use the given number of CPU threads. 0 = hardware limit")
	;
	
	po::options_description parameters("Algorithm-specific Parameters");
	parameters.add_options()
	("verbose,V", po::value< uint >(&params.verbose), "Bitmask for debugging info to be printed out")
	("epsilon", po::value< fp >(&params.epsilon), "How much precision to accept")
	("binSize", po::value< uint >(&params.binSize), "How big a 'bin' should we use?")
	("binCount", po::value< uint >(&params.binCount), "How many bins should we use?")
	("degree", po::value< uint >(&params.DEGREE), "The degree of the B-spline used for interpolation")
	("MCMCburn", po::value< uint >(&params.MCMCburn), "How many iterations to burn in the MCMC sampler")
//	("maxZeros", po::value< uint >(&params.maxZeros), "The maximum number of zeros encountered with the linear HG")
	("mu", po::value< fp >(&params.mu), "A tuning parameter for TOMS748")
	("maxIt", po::value< uint >(&params.maxIt), "The maximum number of TOMS748 iterations")
	;

	po::options_description desc("Options");
	desc.add(general).add(parameters);

	// do the actual parsing here
	po::variables_map vm;
	po::store(po::parse_command_line(argc, argv, desc), vm);
	po::notify(vm);

	// do we need to print out the help?
	if( vm.count("help") ) {

		cout << desc << endl;
		cout << "PARAMETERS" << endl;
		cout << params << endl;
		printAlgorithms();

		// TODO: the same, but for VSFs
		return 1;
		}

	// set up the seed
	Shared::RNGsource	= mt19937( params.seed );

	// which material are we using?
	switch( params.vsf ) {

		default:		// invalid? croak!
			cout << "ERROR: Please supply a valid linear HG. Quitting." << endl;
			return 2;
	
		case 0:
			params.lhg.count	= 4;
			params.lhg.data	= mat06;
			break;

		case 1:
			params.lhg.count	= 8;
			params.lhg.data	= mat33;
			break;

		case 2:
			params.lhg.count	= 4;
			params.lhg.data	= difficult;
			break;

		case 3:
			params.lhg.count	= 2;
			params.lhg.data	= rayleigh;
			break;
		}

	// make room for a timer and the RNGs
	ull marker	= 0;
	ull duration	= 0;
	
	fp* resultsPtr	= nullptr;				// to store the results
	FineTiming ft;

	// we should also state our parameters
	cout << "PARAMETERS" << endl;
	cout << params;
	cout << endl;


	// time to engage the sampler
	shared_ptr<Sampler> sampler	= SamplerFactory::get( params.algorithm );
	if( sampler == nullptr ) {

		cout << "ERROR: Invalid algorithm supplied. Quitting." << endl << endl;
		printAlgorithms();
		return 3;
		}

	// see if we can switch to the proper compute device
	if( !sampler->setCompute(params.useGPU) ) {

		cout << "ERROR: Could not engage compute device #" << params.useGPU << 
			". Quitting." << endl;
		return 4;
		}

	// try the set-up routine
	cout << params.algorithm << ": " << sampler->name() << "." << endl;

	marker	= Sampler::setTimer();

	if( !sampler->setup( params, &ft ) ) {

		cout << "ERROR: Could not set up the given algorithm. Quitting." << endl;
		return 5;
		}

	Sampler::incTimer( marker, duration );

	// print out the stats
	cout << "Startup = " << duration << "ns" << endl;
	cout << ft;

	// and at long last, engage the sampler!
	marker		= Sampler::setTimer();		// to eliminate console output from timing info
	resultsPtr	= sampler->sample( &ft );

	Sampler::incTimer( marker, duration );

	// check for errors
	if( resultsPtr == nullptr ) {

		cout << "ERROR: No samples returned! Quitting." << endl;
		return 6;
		}

	// otherwise, print out the stats
	cout << "Average time per sample = " << (duration/(fp)params.samples) << "ns." << endl;
	cout << "Average computation time per sample = " << 
			(ft.computation/(fp)params.samples) << "ns" << endl;
	cout << ft;
						

	// if requested, print the values out
	if( vm.count("print") && (resultsPtr != nullptr) ) {

		cout << endl;
		cout << "==CUT==" << endl;

		for( uint i = 0; i < params.samples; i++ )
			cout << resultsPtr[i] << endl;

		delete[] resultsPtr;		// be nice about memory

		}

	} // main
