/**************************************************************************
 * INCLUDES
 */

#include "shared.h"
using Shared::hengreen0;
using Shared::hengreen1I;
using Shared::hengreen1Idata;
using Shared::hengreenSamp;

#ifdef __NVCC__
#include <curand_kernel.h>
#endif

#include <deque>			// handy for building up roots
using std::deque;


/**************************************************************************
 * VARIABLES / TYPES
 */

REGISTER(RejectionSimple)
REGISTER(RejectionComposite)
REGISTER(RejectionCompositeAdj)
	
REGISTER(RejectionLinearBinned)


/**************************************************************************
 * FUNCTORS
 */


struct rejection_simple_cpu : public cpu_functor {

       	ull* missTotal;
	const ull seed;
	const fp scale;
	const fp g;

	rejection_simple_cpu(const uint a, fp* b, const uint c, fp* d, 
			ull* e, const ull f, const fp G, const fp h ) :
		cpu_functor{a, b, c, d}, 
		missTotal{e}, seed{f}, scale{G}, g{h} {}

	void operator()(const uint offset = 0, const uint stride = 1) const;

	};

struct rejection_simple_gpu : public gpu_functor {

       	ull* missTotal;
	const ull seed;
	const fp scale;
	const fp g;

	rejection_simple_gpu(const uint a, fp* b, const uint c, fp* d, 
			ull* e, const ull f, const fp G, const fp h ) :
		gpu_functor{a, b, c, d}, 
		missTotal{e}, seed{f}, scale{G}, g{h} {}

	HEMI_LAMBDA void operator()() const;

	};

struct rejection_composite_cpu : public cpu_functor {

       	ull* missTotal;
	const ull seed;
	const uint pos_count;
	fp* pos_data;
	fp* jump;

	rejection_composite_cpu(const uint a, fp* b, const uint c, fp* d, 
			ull* e, const ull f, const uint G, fp* h, fp* i ) :
		cpu_functor{a, b, c, d}, 
		missTotal{e}, seed{f}, pos_count{G}, pos_data{h}, jump{i} {}

	void operator()(const uint offset = 0, const uint stride = 1) const;

	};

struct rejection_composite_gpu : public gpu_functor {

       	ull* missTotal;
	const ull seed;
	const uint pos_count;
	fp* pos_data;
	fp* jump;

	rejection_composite_gpu(const uint a, fp* b, const uint c, fp* d, 
			ull* e, const ull f, const uint G, fp* h, fp* i ) :
		gpu_functor{a, b, c, d}, 
		missTotal{e}, seed{f}, pos_count{G}, pos_data{h}, jump{i} {}

	HEMI_LAMBDA void operator()() const;

	};

struct rejection_linbin_cpu : public cpu_functor {

       	ull* missTotal;
	const ull seed;
	const uint bins;
	fp* bounds;

	rejection_linbin_cpu( const uint a, fp* b, const uint c, fp* d,
			ull* e, const ull f, const uint G, fp* h ) :
		cpu_functor{a, b, c, d}, 
		missTotal{e}, seed{f}, bins{G}, bounds{h} {}

	void operator()(const uint offset = 0, const uint stride = 1) const;

	};

struct rejection_linbin_gpu : public gpu_functor {

       	ull* missTotal;
	const ull seed;
	const uint bins;
	fp* bounds;

	rejection_linbin_gpu( const uint a, fp* b, const uint c, fp* d,
			ull* e, const ull f, const uint G, fp* h ) :
		gpu_functor{a, b, c, d}, 
		missTotal{e}, seed{f}, bins{G}, bounds{h} {}

	HEMI_LAMBDA void operator()() const;

	};


// now fill out the core functor routines
void rejection_simple_cpu::operator()(const uint offset, const uint stride) const {
	
	// other variables we'll need
	LinearHG stored = { lhg_count, lhg_data };
	ull misses	= 0;
	mt19937 RNGsource( seed + offset + 1 );	// ensure thread-safe operation

        uint o          = offset;

	while( o < samples ) {      // while we're within bounds

		// for each sample, pick a target
		fp target	= output[o];
		fp proposal	= hengreenSamp( target, g );
		fp envLike	= hengreen0( proposal, g ) * scale;
		fp dataLike	= hengreen0( proposal, stored );

		// check if we've within the acceptable range
		fp thresh	= Shared::fpDist( RNGsource ) * envLike;
		assert(		envLike >= dataLike );	// a few asserts never hurt nobody
		while (thresh > dataLike) {

			misses++;
			target		= Shared::fpDist( RNGsource );
			proposal	= hengreenSamp( target, g );
			envLike		= hengreen0( proposal, g ) * scale;
			dataLike	= hengreen0( proposal, stored );

			thresh		= Shared::fpDist( RNGsource ) * envLike;
			assert( 	envLike >= dataLike );
			}

		output[o]	 = proposal;
		o		+= stride;
		}

	// now to safely tally up the misses
	missTotal[offset]	= misses;

	} // rejection_simple_cpu

HEMI_LAMBDA void rejection_simple_gpu::operator()() const {

#ifdef __NVCC__
        // set up the RNG
        curandState_t RNGstate;
        curand_init( seed, hemi::globalThreadIndex(), 0, &RNGstate );
	
	// other variables we'll need
	LinearHG stored = { lhg_count, lhg_data };
	ulli misses	= 0;

	// set up our location within the input and output
        uint o          = hemi::globalThreadIndex();
	uint stride     = hemi::globalThreadCount();

	while( o < samples ) {      // while we're within bounds

		// for each sample, pick a target
		fp target	= output[o];
		fp proposal	= hengreenSamp( target, g );
		fp envLike	= hengreen0( proposal, g ) * scale;
		fp dataLike	= hengreen0( proposal, stored );

		// check if we've within the acceptable range
		fp thresh	= curand_uniform(&RNGstate) * envLike;
		while (thresh > dataLike) {

			misses++;
			target		= curand_uniform(&RNGstate);
			proposal	= hengreenSamp( target, g );
			envLike		= hengreen0( proposal, g ) * scale;
			dataLike	= hengreen0( proposal, stored );

			thresh		= curand_uniform(&RNGstate) * envLike;
			}

		output[o]	 = proposal;
		o		+= stride;
		}

	// now to safely tally up the misses
	atomicAdd( (ulli*)missTotal, misses );
#endif
	} // rejection_simple_gpu

void rejection_composite_cpu::operator()(const uint offset, const uint stride) const {

        // set up the RNG
	mt19937 RNGsource( seed + offset + 1 );
	
	// other variables we'll need
	LinearHG stored		= { lhg_count, lhg_data };
	LinearHG positive	= { pos_count, pos_data };
	ull misses		= 0;

	// set up our location within the input and output
        uint o          = offset;

	while( o < samples ) {      // while we're within bounds

		// for each sample, pick a target
		fp target	= output[o];

		int index	= 0;
		while( jump[index] < target )
			index++;

		fp proposal	= hengreenSamp( Shared::fpDist( RNGsource ), positive.data[index + positive.count] );
		fp envLike	= hengreen0( proposal, positive );
		fp dataLike	= hengreen0( proposal, stored );
		assert(		envLike >= dataLike );	// a few asserts never hurt nobody

		// check if we've within the acceptable range
		fp thresh	= Shared::fpDist( RNGsource ) * envLike;
		while (thresh > dataLike) {

			misses++;
			target		= Shared::fpDist( RNGsource );

			int index	= 0;
			while( jump[index] < target )
				index++;

			proposal	= hengreenSamp( Shared::fpDist( RNGsource ), positive.data[index + positive.count] );
			envLike		= hengreen0( proposal, positive );
			dataLike	= hengreen0( proposal, stored );
			assert( 	envLike >= dataLike );

			thresh		= Shared::fpDist( RNGsource ) * envLike;
			}

		output[o]	 = proposal;
		o		+= stride;
		}

	// now to safely tally up the misses
	missTotal[offset] = misses;

	} // rejection_composite_gpu

HEMI_LAMBDA void rejection_composite_gpu::operator()() const {

#ifdef __NVCC__
        // set up the RNG
        curandState_t RNGstate;
        curand_init( seed, hemi::globalThreadIndex(), 0, &RNGstate );
	
	// other variables we'll need
	LinearHG stored		= { lhg_count, lhg_data };
	LinearHG positive	= { pos_count, pos_data };
	ull misses		= 0;

	// set up our location within the input and output
        uint o          = hemi::globalThreadIndex();
	uint stride     = hemi::globalThreadCount();

	while( o < samples ) {      // while we're within bounds

		// for each sample, pick a target
		fp target	= output[o];

		int index	= 0;
		while( jump[index] < target )
			index++;

		fp proposal	= hengreenSamp( curand_uniform(&RNGstate), positive.data[index + positive.count] );
		fp envLike	= hengreen0( proposal, positive );
		fp dataLike	= hengreen0( proposal, stored );

		// check if we've within the acceptable range
		fp thresh	= curand_uniform(&RNGstate) * envLike;
		while (thresh > dataLike) {

			misses++;
			target		= curand_uniform(&RNGstate);

			int index	= 0;
			while( jump[index] < target )
				index++;

			proposal	= hengreenSamp( curand_uniform(&RNGstate), positive.data[index + positive.count] );
			envLike		= hengreen0( proposal, positive );
			dataLike	= hengreen0( proposal, stored );

			thresh		= curand_uniform(&RNGstate) * envLike;
			}

		output[o]	 = proposal;
		o		+= stride;
		}

	// now to safely tally up the misses
	atomicAdd( (ulli*)missTotal, misses );
#endif
	} // rejection_composite_gpu

void rejection_linbin_cpu::operator()(const uint offset, const uint stride) const {

        // set up the RNG
	mt19937 RNGsource( seed + offset + 1 );
	
	// other variables we'll need
	LinearHG stored		= { lhg_count, lhg_data };
	ull misses		= 0;

	// set up our location within the input and output
        uint o          = offset;

	while( o < samples ) {      // while we're within bounds

		// pick a random bin from the acceleration structure
		fp target	= output[o] * bins;
		uint bin	= target;
		fp frac		= target - (fp)bin;

		// figure out the bounds of that bin
		uint offset	= bin*2;
		fp left;
		if( bin == 0 )
			left	= 1.f;
		else
			left	= bounds[ offset - 2 ];

		fp right	= bounds[ offset ];

		// lerp the remainder to find our true theta
		fp proposal	= (1.f - frac)*left + frac*right;

		// evaluate the PDF and our envelope
		fp dataLike	= hengreen0( proposal, stored );
		fp thresh	= Shared::fpDist(RNGsource) * bounds[offset + 1];	// combine two steps

		assert( (dataLike/bounds[offset + 1]) <= 1.f );

		// hopefully we're within the envelope
		while( thresh > dataLike ) {

			misses++;		// nope! recalculate
			target	= Shared::fpDist(RNGsource) * bins;
			bin	= target;
			frac	= target - (fp)bin;

			offset	= bin*2;
			if( bin == 0 )
				left	= 1.f;
			else
				left	= bounds[ offset - 2 ];
			right		= bounds[ offset ];

			proposal	= (1.f - frac)*left + frac*right;

			dataLike	= hengreen0( proposal, stored );
			thresh		= Shared::fpDist(RNGsource) * bounds[offset + 1];
			assert( (dataLike/bounds[offset + 1]) <= 1.f );

			} // while( proposal was rejected )

		output[o]	 = proposal;
		o		+= stride;

		} // while( we have data to write )

	// now to safely tally up the misses
	missTotal[offset] = misses;

	} // rejection_linbin_cpu

HEMI_LAMBDA void rejection_linbin_gpu::operator()() const {

#ifdef __NVCC__
        // set up the RNG
        curandState_t RNGstate;
        curand_init( seed, hemi::globalThreadIndex(), 0, &RNGstate );
	
	// other variables we'll need
	LinearHG stored		= { lhg_count, lhg_data };
	ull misses		= 0;

	// set up our location within the input and output
        uint o          = hemi::globalThreadIndex();
	uint stride     = hemi::globalThreadCount();

	while( o < samples ) {      // while we're within bounds

		// pick a random bin from the acceleration structure
		fp target	= output[o] * bins;
		uint bin	= target;
		fp frac		= target - (fp)bin;

		// figure out the bounds of that bin
		uint offset	= bin*2;
		fp left;
		if( bin == 0 )
			left	= 1.f;
		else
			left	= bounds[ offset - 2 ];

		fp right	= bounds[ offset ];

		// lerp the remainder to find our true theta
		fp proposal	= (1.f - frac)*left + frac*right;

		// evaluate the PDF and our envelope
		fp dataLike	= hengreen0( proposal, stored );
		fp thresh	= curand_uniform(&RNGstate) * bounds[offset + 1];	// combine two steps

		// hopefully we're within the envelope
		while( thresh > dataLike ) {

			misses++;		// nope! recalculate
			target	= curand_uniform(&RNGstate) * bins;
			bin	= target;
			frac	= target - (fp)bin;

			uint offset	= bin*2;
			fp left;
			if( bin == 0 )
				left	= 1.f;
			else
				left	= bounds[ offset - 2 ];
			right		= bounds[ offset ];

			proposal	= (1.f - frac)*left + frac*right;

			dataLike	= hengreen0( proposal, stored );
			thresh		= curand_uniform(&RNGstate) * bounds[offset + 1];
			} // while( proposal was rejected )

		output[o]	 = proposal;
		o		+= stride;

		} // while( we have data to write )

	// now to safely tally up the misses
	atomicAdd( (ulli*)missTotal, misses );
#endif
	} // rejection_linbin_gpu



/**************************************************************************
 * SIMPLE FUNCTIONS
 */

// basic ID functions
uint RejectionSimple::id() const {		return (2 << 24) | 1; }
uint RejectionComposite::id() const {		return (2 << 24) | 2; }
uint RejectionCompositeAdj::id() const {	return (2 << 24) | 3; }

uint RejectionLinearBinned::id() const {	return (2 << 24) | 128; }

string RejectionSimple::name() const {	
	return "rejection sampling via fitted HG, new envelope"; }
string RejectionComposite::name() const {	
	return "rejection sampling via positive components"; }
string RejectionCompositeAdj::name() const {	
	return "rejection sampling via positive components, with scaling"; }

string RejectionLinearBinned::name() const {
	return "rejection sampling via equal-sized bins"; }
//string RejectionConvexBinned::name() const {	
//	return "rejection sampling via a fixed rejection rate"; }


// fill in some destructors, too
RejectionBase::~RejectionBase() {

#ifdef __NVCC__
	if( lhg_gpu )
		cudaFree(lhg_gpu);
#endif

	}

RejectionComposite::~RejectionComposite() {

	if( positive.data )
		delete[] positive.data;
	if( jump_cpu )
		delete[] jump_cpu;

#ifdef __NVCC__
	if( jump_gpu )
		cudaFree(jump_gpu);
	if( pos_gpu )
		cudaFree(pos_gpu);
#endif
	}

// ~RejectionCompositeAdj()'s tasks should be handled by its parent

RejectionLinearBinned::~RejectionLinearBinned() {

	// basic clean-up
	if( bounds_cpu )
		delete[] bounds_cpu;

#ifdef __NVCC__
	if( bounds_gpu )
		cudaFree(bounds_gpu);
#endif
	}

/**************************************************************************
 * RejectionBase
 */

fp RejectionBase::halleyZeros( const fp cos_x, const LinearHG& lhg ) const {

       // checking is done lower down
       fp firSum       = 0.f;
       fp secSum       = 0.f;
       fp thiSum       = 0.f;

       for (int i = 0; i < lhg.count; i++) {

               fp first        = 0.f;
               fp secon        = 0.f;
               fp third        = 0.f;

               hengreen1D2D3D( cos_x, lhg.data[i+lhg.count], first, secon, third  );
               firSum +=       lhg.data[i] * first;
               secSum +=       lhg.data[i] * secon;
               thiSum +=       lhg.data[i] * third;
               }
	 
      	return cos_x - (2. * firSum * secSum) / (2.*secSum*secSum - firSum*thiSum);

	} // RejectionBase::halleyZeros

void RejectionBase::hengreen1D2D3D( const fp cos_x, const fp g, 
		fp& firstD, fp& secondD, fp& thirdD ) const {

	if ((cos_x < -1.f) || (cos_x > 1.f)) {
		firstD = secondD = thirdD = 0.f;
		return;
		}

	fp g2		= g*g;
	fp partDenom	= 1.f / (1.f + g2 - 2.*g*cos_x);
	fp sqrtDenom	= sqrt(partDenom);
	partDenom	*= g;			// reduce the number of multiplies

	firstD		= 3. * (1 - g2) * partDenom * sqrtDenom;
	secondD		= firstD * 5. * partDenom;
	thirdD		= secondD * 7. * partDenom;
	return;

	} // RejectionBase::hengreen1D2D3D

void RejectionBase::calculateGandScale( const fp start, const fp end, fp& g, 
		fp& scalar ) {

	fp y		= cbrt( start / end );
	g		= (y - 1.f) / (1.f + y);

	fp omg		= (1.f - g);
	fp opg		= (1.f + g);
	fp scale1	= start * omg*omg / opg;
	fp scale2	= end * opg*opg / omg;
	
	if (scale1 > scale2)	// should be equal, but precision can be a tricky beast
		scalar	= scale1;
	else
		scalar	= scale2;
	
	} // RejectionBase::calculateGandScale

void RejectionBase::calculateGandScale( const fp start, const fp end, LinearHG& envelope ) {

	// calculate individual starts/ends
	uint c		= envelope.count;
	fp* starts	= new fp[c];
	fp* ends	= new fp[c];
	fp oldStart	= 0.f;
	fp oldEnd	= 0.f;

	for( uint i = 0; i < c; i++ ) {

		fp weight	 = envelope.data[i];
		starts[i] 	 = Shared::hengreen0(  1., envelope.data[c + i] ) * weight;
		ends[i]		 = Shared::hengreen0( -1., envelope.data[c + i] ) * weight;
		oldStart	+= starts[i];
		oldEnd		+= ends[i];
		}

	fp ratioStart	= start / oldStart;
	fp ratioEnd	= end / oldEnd;

	// now revise their weights and scalars
	for( uint i = 0; i < c; i++ )
		calculateGandScale( starts[i]*ratioStart, ends[i]*ratioEnd, envelope.data[c + i], envelope.data[i] );

	} // RejectionBase::calculateGandScale


/**************************************************************************
 * RejectionSimple
 */

fp RejectionSimple::halleyZeros( const fp cos_x, const LinearHG& lhg, 
		const fp g, const fp scalar ) const {

	// checking is done lower down
	fp firSum	= 0.f;
	fp secSum	= 0.f;
	fp thiSum	= 0.f;

	hengreen1D2D3D( cos_x, g, firSum, secSum, thiSum );
	firSum *= -scalar;
	secSum *= -scalar;
	thiSum *= -scalar;

	for (int i = 0; i < lhg.count; i++) {

		fp first	= 0.f;
		fp secon	= 0.f;
		fp third	= 0.f;
		
		hengreen1D2D3D( cos_x, lhg.data[i+lhg.count], first, secon, third  );
		firSum += 	lhg.data[i] * first;
		secSum += 	lhg.data[i] * secon;
		thiSum += 	lhg.data[i] * third;
		}

	return cos_x - (2. * firSum * secSum) / (2.*secSum*secSum - firSum*thiSum);
	}

bool RejectionSimple::setup( const Parameters& p, FineTiming* t ) {

	if( !registered ) {

		cout << "ERROR: Why isn't RejectionSimple registered?!" << endl;
		return false;
		}

	// capture some basic timing data
	ull startTime;
	if( t != nullptr )
		startTime	= setTimer();

	// store some key variables
	params		= p;

	// store the start and end points of the current envelope
	fp startEval	= hengreen0(   1.f, params.lhg );
	fp endEval 	= hengreen0(  -1.f, params.lhg );

	fp maxFound	= startEval;
	if( endEval > startEval )
		maxFound	= endEval;

	// estimate the values for g and scalar
	calculateGandScale( startEval, endEval, g, scalar );
	scalar *= 1.001;		// KLUDGE: compensate for imprecision

	// let's limit ourselves to a specific number of tries
	int attempts = params.maxIt;

	deque<fp> stack;		// divide and conquer breakout areas
	stack.push_back( 1 );
	stack.push_back( -1 );

	while( stack.size() > 1 ) {		// while we haven't ruled out all intervals

		fp oldVal	= 0.5*(stack[0] + stack[1]);
		fp newVal	= halleyZeros( oldVal, params.lhg, g, scalar );
		int count	= params.maxIt;		// precision can lead to endless loops

		// proper binary search between intervals
		while ( (newVal < stack[0]) && (newVal > stack[1]) && (count > 0) ) {	

			// Halley's method converged? flag
			if( fabs(oldVal - newVal) < params.epsilon ) {

				count = -1;
				break;
				}

			// otherwise, continue the search
			oldVal	= newVal;
			newVal	= halleyZeros( oldVal, params.lhg, g, scalar );
			count--;
			}

		// did we break due to convergence, or a broken envelope?
		if( count <= 0 ) {

			// broken envelope, so update the max
			fp envelopeSamp		= hengreen0( newVal, g )*scalar;
			fp dataSamp		= hengreen0( newVal, params.lhg );

			// this is almost certainly false, but it doesn't hurt to check
			if( dataSamp > maxFound )
				maxFound	= dataSamp;

			// if we're out of envelope, which side needs scaling?
			if( envelopeSamp < dataSamp ) {

				fp proposal = dataSamp / envelopeSamp;

				if( newVal >= 0.f )
					startEval	*= proposal * 1.001;	// overstep to encourage convergence
				else
					endEval		*= proposal * 1.001;
								
				calculateGandScale( startEval, endEval, g, scalar );

				// ensure we don't loop infinitely				
				if (attempts <= 0)
					break;

				// otherwise, our stack is now invalid. Reset!
				stack.clear();
				stack.push_back( 1 );
				stack.push_back( -1 );
				attempts--;
	
				}
	
			// otherwise grow the stack, setting up new bounds
			else {
				fp temp		= stack[0];
				stack[0]	= newVal;
				stack.insert(	stack.begin(), temp );
				}

			} // if (found envelope violation)

		// otherwise, we escaped. Next bounds!
		else
			stack.pop_front();

		} // while( there are bounds to check )

	// sanity check: did we exhaust our tries?
	if( attempts <= 0 ) {

		// then settle for the worst-case scenario
		g	= 0;
		scalar	= maxFound * 1.001;	// KLUDGE: compensate for imprecision
		}

	// update timing info
	if( t != nullptr )
		incTimer( startTime, t->computation );

	// transfer data to the GPU, if necessary
	if( usingGPU() ) {

		freeGPU(lhg_gpu);
		lhg_gpu = mallocToGPU( params.lhg.data, params.lhg.count*2 );
		if( !testPointer(lhg_gpu, "ERROR: Could not allocate memory for the LHG on the GPU!") )
			return false;

		hemi::deviceSynchronize();              // force all memory activity to complete
		}

	if( t != nullptr )
		incTimer( startTime, t->transferToDev );

	return true;

	} // RejectionSimple::setup()

fp* RejectionSimple::sample( FineTiming* t ) const {

	// some preliminary timing setup
	ull startTime;

	if( t != nullptr )
		startTime	= setTimer();

	fp* output	= nullptr;
	ull* misses	= nullptr;		// ull[threads] on CPU, ull[1] on GPU

	if( !allocResults( &output, &misses ) )
		return nullptr;

        if( t != nullptr )
		incTimer( startTime, t->transferToDev );

	// time to start computing!
	if( usingGPU() ) {
#ifdef __NVCC__

		rejection_simple_gpu calc( params.lhg.count, lhg_gpu, params.samples, output,
				misses, params.seed, scalar, g );
		hemi::launch(calc);

	        hemi::deviceSynchronize();              // force all memory activity to complete
#else
		cout << "ERROR: How did you manage to enable the GPU without using nvcc?" << endl;
		return nullptr;
#endif
		}
	else {
		
		rejection_simple_cpu calc( params.lhg.count, params.lhg.data, params.samples, output,
				misses, params.seed, scalar, g );
		launch( calc );

		// finally, consolidate the miss counts to unify GPU and CPU handling
		for( uint i = 1; i < getThreadCount(); i++ )
			misses[0]	+= misses[i];

		}

	if( t != nullptr )
		incTimer( startTime, t->computation );

	// copy results and return
	if( usingGPU() && !retrieveResultsFromGPU( &output, &misses ) ) {

		cout << "ERROR: Could not copy the results back from the GPU." << endl;
		t->misses	= -1;
		return nullptr;
		}

	if( t != nullptr ) {

		incTimer( startTime, t->transferFromDev );
		t->misses = (ull) *misses;
		}

	if( misses != nullptr )
		delete[] misses;
	return output;

	} // RejectionSimple::sample


/**************************************************************************
 * RejectionComposite
 */

void RejectionComposite::calculateJump() {

	if( jump_cpu )		// handle memory allocation
		delete[] jump_cpu;

	jump_cpu	= new fp[ positive.count ];

	fp sum		= 0;	// pass 1: generate a CDF
	for( uint i = 0; i < positive.count; i++ ) {

		sum		+= positive.data[i];
		jump_cpu[i]	= sum;
		}

				// pass 2: normalize
	for( uint i = 0; i < positive.count; i++ )
		jump_cpu[i]	/= sum;

	} // RejectionComposite::calculateJump()

bool RejectionComposite::setup( const Parameters& p, FineTiming* t ) {

	if( !registered ) {

		cout << "ERROR: Why isn't RejectionComposite registered?!" << endl;
		return false;
		}

	// capture some basic timing data
	ull startTime;
	if( t != nullptr )
		startTime	= setTimer();

	// store some key variables
	params		= p;

	// extract the positive components 
	positive.count	= 0;
	for( uint i = 0; i < params.lhg.count; i++ )
		if( params.lhg.data[i] > 0 )
			positive.count++;

	positive.data	= new fp[ positive.count*2 ];
	uint index	= 0;
	for( uint i = 0; i < params.lhg.count; i++ )
		if( params.lhg.data[i] > 0 ) {

			positive.data[index]			= params.lhg.data[i];
			positive.data[index + positive.count]	= params.lhg.data[i + params.lhg.count];
			index++;
			}

	// calculate the jump table
	calculateJump();

	// update timing info
	if( t != nullptr )
		incTimer( startTime, t->computation );

	// now transfer this to the GPU
	if( usingGPU() ) {

		freeGPU(lhg_gpu);
		lhg_gpu = mallocToGPU( params.lhg.data, params.lhg.count*2 );
		if( !testPointer(lhg_gpu, "ERROR: Could not allocate memory for the LHG on the GPU!") )
			return false;

		freeGPU(pos_gpu);
		pos_gpu = mallocToGPU( positive.data, positive.count*2 );
		if( !testPointer(pos_gpu, "ERROR: Could not allocate memory for the envelope on the GPU!") )
			return false;

		freeGPU(jump_gpu);
		jump_gpu = mallocToGPU( jump_cpu, positive.count );
		if( !testPointer(jump_gpu, "ERROR: Could not allocate memory for the jump table on the GPU!") )
			return false;

		hemi::deviceSynchronize();              // force all memory activity to complete
		}

	if( t != nullptr )
		incTimer( startTime, t->transferToDev );

	return true;

	} // RejectionComposite::setup

fp* RejectionComposite::sample( FineTiming* t ) const {

	// some preliminary timing setup
	ull startTime;

	if( t != nullptr )
		startTime	= setTimer();

	fp* output	= nullptr;
	ull* misses	= nullptr;

	if( !allocResults( &output, &misses ) )
		return nullptr;

        if( t != nullptr )
		incTimer( startTime, t->transferToDev );

	// time to start computing!
	if( usingGPU() ) {
#ifdef __NVCC__

		rejection_composite_gpu calc( params.lhg.count, lhg_gpu, params.samples, output, 
				misses, params.seed, positive.count, pos_gpu, jump_gpu );
		hemi::launch(calc);

	        hemi::deviceSynchronize();              // force all memory activity to complete
#else
		cout << "ERROR: How did you manage to enable the GPU without using nvcc?" << endl;
		return nullptr;
#endif
		}
	else {

		rejection_composite_cpu calc( params.lhg.count, params.lhg.data, params.samples, output, 
				misses, params.seed, positive.count, positive.data, jump_cpu );
		launch( calc );

		// finally, consolidate the miss counts to unify GPU and CPU handling 
		for( uint i = 1; i < getThreadCount(); i++ )
			misses[0]       += misses[i];

		}

	if( t != nullptr )
		incTimer( startTime, t->computation );

	// copy results and return
	if( usingGPU() && !retrieveResultsFromGPU( &output, &misses ) ) {

		cout << "ERROR: Could not copy the results back from the GPU." << endl;
		 t->misses	= -1;
		 return nullptr;
		}

	if( t != nullptr ) {

		incTimer( startTime, t->transferFromDev );
		t->misses = (ull) *misses;
		}

	if( misses != nullptr )
		delete[] misses;
	return output;

	} // RejectionComposite::sample

/**************************************************************************
 * RejectionCompositeAdj
 */

fp RejectionCompositeAdj::halleyZeros( const fp cos_x, const LinearHG& lhg, 
		const LinearHG& minus, const fp scalar ) const {

	assert( lhg.count >= minus.count );	// this should always be true

	// checking is done lower down
	fp firSum	= 0.f;
	fp secSum	= 0.f;
	fp thiSum	= 0.f;

	// set up an iterator to loop over it all
	for( int i = 0; i < lhg.count; i++ ) {

		fp first	= 0.f;
		fp secon	= 0.f;
		fp third	= 0.f;

		if( i < lhg.count ) {

			hengreen1D2D3D( cos_x, lhg.data[i+lhg.count], first, secon, third  );
			firSum += 	lhg.data[i] * first;
			secSum += 	lhg.data[i] * secon;
			thiSum += 	lhg.data[i] * third;
			}

		if( i < minus.count ) {
			
			hengreen1D2D3D( cos_x, minus.data[i+minus.count], first, secon, third );
			firSum -= 	minus.data[i] * first * scalar;
			secSum -= 	minus.data[i] * secon * scalar;
			thiSum -= 	minus.data[i] * third * scalar;
			}
		}

	return cos_x - (2. * firSum * secSum) / (2.*secSum*secSum - firSum*thiSum);
	}

bool RejectionCompositeAdj::setup( const Parameters& p, FineTiming* t ) {

	if( !registered ) {

		cout << "ERROR: Why isn't RejectionCompositeAdj registered?!" << endl;
		return false;
		}

	// capture some basic timing data
	ull startTime;
	if( t != nullptr )
		startTime	= setTimer();

	// store some key variables
	params		= p;

	// perform a quick census to get the total and allocate space
	positive.count	= 0;
	for( int i = 0; i < params.lhg.count; i++ )
		if( params.lhg.data[i] > 0 )
		       positive.count++;

	// use the count as a counter, and make room for everything
	positive.data	= new fp[ positive.count*2 ];

	uint offset	= 0;
	for( int i = 0; i < params.lhg.count; i++ )

		if( params.lhg.data[i] > 0 ) {	// if a positive, store it

			positive.data[offset]			= params.lhg.data[i];
			positive.data[offset+positive.count]	= params.lhg.data[i+params.lhg.count];
			offset++;

			}

	// create the jump table
	calculateJump();

	// HEY, isn't this the same as RejectionSimple's, except the latter only uses one component?

	// store the start and end points of the underlying function
	fp startEval	= hengreen0(   1.f, params.lhg );
	fp endEval 	= hengreen0(  -1.f, params.lhg );
	fp maxFound	= startEval;
	if( endEval > maxFound )
		maxFound	= endEval;

	// estimate the value of the scalar
	scalar		= startEval	/ hengreen0(	 1.f, positive );
	fp guess	= endEval	/ hengreen0(	-1.f, positive );
	if( guess > scalar )
		scalar	= guess;

	// let's limit ourselves to a specific number of tries
	int attempts = params.maxIt;

	deque<fp> stack;		// divide and conquer breakout areas
	stack.push_back( 1 );
	stack.push_back( -1 );

	while( stack.size() > 1 ) {		// while we haven't ruled out all intervals

		fp oldVal	= 0.5*(stack[0] + stack[1]);
		fp newVal	= halleyZeros( oldVal, params.lhg, positive, scalar );
		int count	= params.maxIt;		// precision can lead to endless loops

		// proper binary search between intervals
		while ( (newVal < stack[0]) && (newVal > stack[1]) && (count > 0) ) {	

			// Halley's method converged? flag
			if( fabs(oldVal - newVal) < params.epsilon ) {

				count = -1;
				break;
				}

			// otherwise, continue the search
			oldVal	= newVal;
			newVal	= halleyZeros( oldVal, params.lhg, positive, scalar );
			count--;
			}

		// did we break due to convergence, or a broken envelope?
		if( count <= 0 ) {

			// broken envelope, so update the max
			fp envelopeSamp		= hengreen0( newVal, positive )*scalar;
			fp dataSamp		= hengreen0( newVal, params.lhg );

			// this is almost certainly false, but it doesn't hurt to check
			if( dataSamp > maxFound )
				maxFound	= dataSamp;

			// if we're out of envelope, scale and retry
			if( envelopeSamp < dataSamp ) {

				// overstep for convergence
				scalar *= dataSamp / envelopeSamp * 1.01;

				// ensure we don't loop infinitely				
				if (attempts <= 0)
					break;

				// our stack is now invalid. Reset!
				stack.clear();
				stack.push_back( 1 );
				stack.push_back( -1 );
				attempts--;
	
				}
	
			// otherwise grow the stack, setting up new bounds
			else {
				fp temp		= stack[0];
				stack[0]	= newVal;
				stack.insert(	stack.begin(), temp );
				}

			} // if (found envelope violation)

		// otherwise, we escaped. Next bounds!
		else
			stack.pop_front();

		} // while( there are bounds to check )

	// if we didn't exhaust our tries, apply the scalar
	if( attempts > 0 ) {

		// if we sailed through, manually overstep a bit to compensate for FP imprecision
		if( attempts == params.maxIt )
			scalar *= 1.01;

		if( scalar > 1. )	// but don't go further than we need to
			scalar	= 1.;

		for( uint i = 0; i < positive.count; i++ )
			positive.data[i]	*= scalar;
		}

	// otherwise, settle on the worst-case scenario: positive envelope, no scaling

	// update timing info
	if( t != nullptr )
		incTimer( startTime, t->computation );

	// transfer data to the GPU, if necessary
	if( usingGPU() ) {

		freeGPU(lhg_gpu);
		lhg_gpu = mallocToGPU( params.lhg.data, params.lhg.count*2 );
		if( !testPointer(lhg_gpu, "ERROR: Could not allocate memory for the LHG on the GPU!") )
			return false;

		freeGPU(pos_gpu);
		pos_gpu = mallocToGPU( positive.data, positive.count*2 );
		if( !testPointer(pos_gpu, "ERROR: Could not allocate memory for the envelope on the GPU!") )
			return false;

		freeGPU(jump_gpu);
		jump_gpu = mallocToGPU( jump_cpu, positive.count );
		if( !testPointer(jump_gpu, "ERROR: Could not allocate memory for the jump table on the GPU!") )
			return false;

		hemi::deviceSynchronize();              // force all memory activity to complete
		}

	if( t != nullptr )
		incTimer( startTime, t->transferToDev );

	return true;

	} // RejectionCompositeAdj::setup

/**************************************************************************
 * RejectionLinearBinned
 */

// set up our acceleration structure
bool RejectionLinearBinned::setup( const Parameters& p, FineTiming* ft ) {

	if( !registered ) {

		cout << "ERROR: Why isn't RejectionCompositeAdj registered?!" << endl;
		return false;
		}

	// capture some basic timing data
	ull startTime;
	if( ft != nullptr )
		startTime	= setTimer();

	// store some key variables
	params		= p;

	// start laying out the bins
	fp invBinCount	= 1.f / (fp)params.binCount;
	fp leftHeight	= hengreen0( 1.f, params.lhg );

	// allocate some storage space for the structure (location, height)
	if( bounds_cpu )
		delete[] bounds_cpu;
	bounds_cpu		= new fp[ params.binCount * 2 ];

	fp* actual_area		= new fp[ params.binCount ];		// spare us from recalculating the numerator
	fp least_accept	= 1.f;

	// figure out the integral from 1 to -1
	fp CDFmax, CDFmin;
	hengreen1Idata( params.lhg, CDFmax, CDFmin );
	fp totalArea	= CDFmax - CDFmin;
	
	// for each bin
	for( uint bin = 0; bin < params.binCount; bin++ ) {

		// target = bin / number * previous integral
		fp target	= (bin + 1) * totalArea * invBinCount;

		// use binary search to find target = integral from 1 to theta
		fp left		= 1.f;
		if( bin > 0 ) 
			left	= bounds_cpu[ (bin-1)*2 ];
		fp right	= -1.f;

		while( (left-right) > params.epsilon ) {

			fp middle	= 0.5*( left + right );
			fp val		= CDFmax - hengreen1I( middle, params.lhg );

			if( target < val )
				right   = middle;
			else
				left	= middle;
			}

		// use a linear approximation to get theta
		fp leftInt	= CDFmax - hengreen1I( left, params.lhg );
		fp rightInt	= CDFmax - hengreen1I( right, params.lhg );

		fp theta	= left + ((target - leftInt) * (right - left) / (rightInt - leftInt));

		if( bin == (params.binCount - 1) )
			theta	= -1.f;			// compensate for FP imprecision

		// recycle height at left of box from last calculation
		// calculate height at right of box
		fp rightHeight	= hengreen0( theta, params.lhg );

		// HEY, isn't this the same as prior envelope code, just with strict bounds and a different envelope?

		// store max(left,right) as best-seen height
		fp boxHeight;
		if( leftHeight < rightHeight )
			boxHeight = rightHeight;
		else
			boxHeight = leftHeight;

		// prepare for a few rounds of Halley's method
		if( bin > 0 )
			left	= bounds_cpu[ (bin-1)*2 ];
		else
			left	= 1.f;

		deque<fp> stack;	// we need to divide-and-conquer the box, alas
		stack.push_back(left);	// KLUDGE: switch to using arrays, as per prior code
		stack.push_back(theta);

		while( stack.size() > 1 ) {	// while we have bounds within here

			fp oldVal	= 0.5*(stack[0] + stack[1]);
			fp newVal	= halleyZeros( oldVal, params.lhg );
			int count	= params.maxIt;	// ... but beware endless loops

			// slipping out of bounds = failure
			while( (newVal < stack[0]) && (newVal > stack[1]) && (count > 0) ) {

				// Halley's method converged? Flag this
				if( fabs(oldVal - newVal) < params.epsilon ) {

					count = -1;
					break;
					}

				// otherwise, loop back
				oldVal	= newVal;
				newVal	= halleyZeros( oldVal, params.lhg );
				count--;
				}

			// did we break out because we converged? Or oscillated at a local extrema?
			if( count <= 0 ) {

				// local extrema, so update the max and grow the stack
				fp candidate	= hengreen0( newVal, params.lhg );
				if( candidate > boxHeight )
					boxHeight = candidate;

				fp temp		= stack[0];
				stack[0]	= newVal;
				stack.insert(	stack.begin(), temp );
				}

			// otherwise, we must have escaped our bounds
			else
				stack.pop_front();

			} // while( we have bounds to check )

		// store right bound & best seen height in array
		bounds_cpu[ bin*2 ]	= theta;
		bounds_cpu[ bin*2 + 1 ]	= boxHeight * 1.0025;	// KLUDGE: compensates for imprecision

		// figure out the rejection rates
		if( bin == 0 )
			left = 1.f;
		else
			left = bounds_cpu[ (bin-1)*2 ];

		actual_area[bin]	= (hengreen1I( left, params.lhg ) - hengreen1I( theta, params.lhg ));

		assert( actual_area[bin] >= 0.f );

		fp temp	= actual_area[bin] / ((left-theta) * bounds_cpu[ bin*2 + 1 ]);
		if( temp < least_accept )
			least_accept = temp;

		// shuffle down to the next box
		leftHeight	= rightHeight;

		} // for( each box )

	// now go back and equalize the rejection rates
	for( uint bin = 0; bin < params.binCount; bin++ ) {
	
		fp left		= 1.f;
		if( bin > 0 ) 
			left	= bounds_cpu[ (bin-1)*2 ];
		fp right	= bounds_cpu[ bin*2 ];

		bounds_cpu[ bin*2 + 1 ]	= actual_area[bin] / 
			((left-right) * least_accept);
		}

	// update timing info
	if( ft != nullptr )
		incTimer( startTime, ft->computation );

	// transfer data to the GPU, if necessary
	if( usingGPU() ) {

		freeGPU(lhg_gpu);
		lhg_gpu = mallocToGPU( params.lhg.data, params.lhg.count*2 );
		if( !testPointer(lhg_gpu, "ERROR: Could not allocate memory for the LHG on the GPU!") )
			return false;

		freeGPU(bounds_gpu);
		bounds_gpu = mallocToGPU( bounds_cpu, params.binCount*2 );
		if( !testPointer(bounds_gpu, "ERROR: Could not allocate memory for the bounds on the GPU!") )
			return false;

		hemi::deviceSynchronize();              // force all memory activity to complete
		}

	if( ft != nullptr )
		incTimer( startTime, ft->transferToDev );

	return true;

	} // RejectionLinearBinned::setup

fp* RejectionLinearBinned::sample( FineTiming* ft ) const {

	// some preliminary timing setup
	ull startTime;

	if( ft != nullptr )
		startTime	= setTimer();

	fp* output	= nullptr;
	ull* misses	= nullptr;

	if( !allocResults( &output, &misses ) )
		return nullptr;

        if( ft != nullptr )
		incTimer( startTime, ft->transferToDev );

	// time to start computing!
	if( usingGPU() ) {
#ifdef __NVCC__

		rejection_linbin_gpu calc( params.lhg.count, lhg_gpu, params.samples, output, 
				misses, params.seed, params.binCount, bounds_gpu );
		hemi::launch(calc);

	        hemi::deviceSynchronize();              // force all memory activity to complete
#else
		cout << "ERROR: How did you manage to enable the GPU without using nvcc?" << endl;
		return nullptr;
#endif
		}
	else {

		rejection_linbin_cpu calc( params.lhg.count, params.lhg.data, params.samples, output, 
				misses, params.seed, params.binCount, bounds_cpu );
		launch( calc );

		// finally, consolidate the miss counts to unify GPU and CPU handling 
		for( uint i = 1; i < getThreadCount(); i++ )
			misses[0]       += misses[i];

		}

	if( ft != nullptr )
		incTimer( startTime, ft->computation );

	// copy results and return
	if( usingGPU() && !retrieveResultsFromGPU( &output, &misses ) ) {

		cout << "ERROR: Could not copy the results back from the GPU." << endl;
		ft->misses	= -1;
		return nullptr;
		}

	if( ft != nullptr ) {

		incTimer( startTime, ft->transferFromDev );
		ft->misses = (ull) *misses;
		}

	if( misses != nullptr )
		delete[] misses;
	return output;

	} // RejectionLinearBinned::sample

