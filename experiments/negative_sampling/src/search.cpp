/**************************************************************************
 * INCLUDES
 */

#include "shared.h"
using namespace Shared;		// preserve our sanity

#include <cmath>
using std::isfinite;

#include <boost/math/tools/roots.hpp>
using boost::math::tools::bisect;
using boost::math::tools::eps_tolerance;
using boost::math::tools::toms748_solve;



/**************************************************************************
 * VARIABLES / TYPES
 */

REGISTER(BisectionSampler)
REGISTER(MyBisectionSampler)

REGISTER(TOMS748sampler)
REGISTER(MyTOMS748sampler)


/**************************************************************************
 * FUNCTORS
 */


struct bisection_uni : public unified_functor {

	const fp epsilon;

	bisection_uni(const uint a, fp* b, const uint c, fp* d, fp e) : 
		unified_functor(a, b, c, d), epsilon{e} {}

	// no RNG = the same code across GPU and CPU!
	HEMI_DEV_CALLABLE void operator()(const uint offset, const uint stride) const;

	};

struct bisection_boost : public cpu_functor {

	const fp epsilon;

	bisection_boost(const uint a, fp* b, const uint c, fp* d, const fp e) : 
		cpu_functor(a, b, c, d), epsilon{e} {}

	void operator()(const uint offset, const uint stride) const;

	};

struct toms748 : public unified_functor {

	const fp epsilon;
	const fp ep2;
	const fp ep5;
	const fp ep32;

	const uint maxIt;
	const fp mu;

	LinearHG stored;	// it makes some functions easier

	toms748(const uint a, fp* b, const uint c, fp* d, 
			const fp e, const uint f, const fp g) : 
		unified_functor(a, b, c, d), 
		epsilon{e}, ep2{e*2.f}, ep5{e*5.f}, ep32{e*32.f}, maxIt{f}, mu{g} {
		
		stored	= { a, b };
		}

	HEMI_DEV_CALLABLE void operator()(const uint offset, const uint stride) const;

	HEMI_DEV_CALLABLE bool tol( const fp a, const fp b ) const;
	HEMI_DEV_CALLABLE bool tol( const fp a, const fp b, const fp ep ) const;

	HEMI_DEV_CALLABLE void bracket( fp& a, fp& b, fp c, fp& fa, fp& fb, 
		fp& d, fp& fd, const fp scaledTarget, const fp scalar ) const;

	HEMI_DEV_CALLABLE fp secant( const fp& a, const fp& b, 
			const fp& fa, const fp& fb ) const;

	HEMI_DEV_CALLABLE fp quadratic( const fp& a, const fp& b, const fp& d, const fp& fa,
		const fp& fb, const fp& fd, const uint count ) const;

	HEMI_DEV_CALLABLE fp cubic( const fp& a, const fp& b, const fp& d, const fp& e,
		const fp& fa, const fp& fb, const fp& fd, const fp& fe ) const;

	HEMI_DEV_CALLABLE fp sample( const fp target, const fp CDFmax, const fp scalar ) const;
	};

struct toms748_boost : public cpu_functor {

	const fp epsilon;
	const uint max_iter;

	toms748_boost(const uint a, fp* b, const uint c, fp* d, const fp e, const uint f) : 
		cpu_functor(a, b, c, d), epsilon{e}, max_iter{f} {}

	void operator()(const uint offset, const uint stride) const;

	};


// now fill out those functors
HEMI_DEV_CALLABLE void bisection_uni::operator()(const uint o, const uint s) const {

	// use stride to flag GPU operation
	uint offset;
	uint stride;

	if( s == 0 ) {

		offset	= hemi::globalThreadIndex();
		stride	= hemi::globalThreadCount();
		}
	else {
		offset	= o;
		stride	= s;
		}


	// make this easier to access
	LinearHG stored         = { lhg_count, lhg_data };

	// set up the scalar	
	fp CDFmax	= 0;
	fp CDFmin	= 0;
	hengreen1Idata( stored, CDFmax, CDFmin );
	fp scalar	= CDFmax - CDFmin;

	while( offset < samples ) {      // while we're within bounds

		// for each sample, pick a target
		fp target	= output[offset] * scalar;	// adjust this to the proper range
		fp left		= 1.;
		fp right	= -1.;

		while( (left-right) > epsilon ) {		// binary search!

			fp middle	= 0.5*( left + right );
			fp val		= CDFmax - hengreen1I( middle, stored );

			if( target < val )
				right	= middle;
			else
				left	= middle;
			}

		output[offset]	 = 0.5*(left + right);
		offset		+= stride;
		}

	} // bisection_uni

void bisection_boost::operator()(const uint o, const uint stride) const {

	// make this easier to access
	const LinearHG stored         = { lhg_count, lhg_data };

	// set up the scalar	
	fp CDFmax	= 0;
	fp CDFmin	= 0;
	hengreen1Idata( stored, CDFmax, CDFmin );
	const fp scalar	= CDFmax - CDFmin;

	// set up the required precision
	int digits = -log(epsilon)/log(2.);
	eps_tolerance<fp> tol(digits);

	uint offset	= o;

	while( offset < samples ) {      // while we're within bounds

		// for each sample, pick a target
		fp target	= output[offset] * scalar;	// adjust this to the proper range
		std::pair<fp,fp> bracket	= bisect( [=](const fp cos_x) {
				
			return (CDFmax - hengreen1I( cos_x, stored )) - target;
			}, 
			(fp)-1., (fp)1., tol );

		output[offset]	 = 0.5*(bracket.first + bracket.second);
		offset		+= stride;
		}

	} // bisection_boost


// TOMS748
HEMI_DEV_CALLABLE bool toms748::tol( const fp a, const fp b ) const {			

	return fabs(a-b) < epsilon; 
	}

HEMI_DEV_CALLABLE bool toms748::tol( const fp a, const fp b, const fp ep ) const {

	return fabs(a-b) < ep; 
	}

/* TOMS748 code is based on Boost 1.69, thus is under the Boost Software License.
 *  See the LICENSE text for more details. */
HEMI_DEV_CALLABLE void toms748::bracket( fp& a, fp& b, fp c, fp& fa, fp& fb, 
		fp& d, fp& fd, const fp scaledTarget, const fp CDFmax ) const {

	// adjust c to avoid the interval's ends
	if( (b-a) < 2 * ep2 * a )
		c = a + (b-a) * .5;
	else if( c <= a + fabs(a)*ep2 )
		c = a + fabs(a)*ep2;
	else if( c >= b - fabs(b)*ep2 )
		c = b - fabs(b)*ep2;

	fp fc	= (CDFmax - hengreen1I( c, stored )) - scaledTarget;
	if( tol(fc,0.) ) {	// check for a trivial zero
		
		a 	= c;
		fa	= 0.;
		d	= 0.;
		fd	= 0.;

		return;
		}

	// otherwise, branch based on sign
	if( (fa > 0.) != (fc > 0.) ) {

		d	= b;
		fd	= fb;
		b	= c;
		fb	= fc;
		}
	else {

		d	= a;
		fd	= fa;
		a	= c;
		fa	= fc;
		}

	} // bracket

HEMI_DEV_CALLABLE fp toms748::secant( const fp& a, const fp& b, const fp& fa, const fp& fb ) const {

	fp c	= a - (fa / (fb - fa)) * (b - a);
	if( (c <= a + fabs(a)*ep5) || (c >= b - fabs(b)*ep5) )
		return (a + b) * .5;
	return c;
	}

HEMI_DEV_CALLABLE fp toms748::quadratic( const fp& a, const fp& b, const fp& d, const fp& fa,
		const fp& fb, const fp& fd, const uint count ) const {

	// determine the coeffecients of the quadratic
	fp B	= (fb - fa) / (b - a);
	fp A	= (fd - fb) / (d - b);
	A	= (A - B) / (d - a);

	if( !isfinite(A) )		// TODO: are we ever infinite?
		return secant(a, b, fa, fb);

	fp c;				// the starting point
	if( (A > 0.) == (fa > 0.) )
		c	= a;
	else
		c	= b;

	// iterate Newton's method
	for( uint i = 1; i <= count; ++i) {

		fp temp	= (fa + (B + A*(c-b))*(c-a)) / (B + A*(2*c - a - b));
		if( isfinite(temp) )	// TODO: are we infinite?
			c -= temp;
		else
			c = a - 1.;
		}

	// catch an iteration failure
	if( (c <= a) || (c >= b) )
		c	= secant(a, b, fa, fb);

	return c;

	}

HEMI_DEV_CALLABLE fp toms748::cubic( const fp& a, const fp& b, const fp& d, const fp& e,
		const fp& fa, const fp& fb, const fp& fd, const fp& fe ) const {

	// gather together the coefficients
	fp Q11	= (d-e) * fd / (fe - fd);
	fp Q21	= (b-d) * fb / (fd - fb);
	fp Q31	= (a-b) * fa / (fb - fa);
	fp D21	= (b-d) * fd / (fd - fb);
	fp D31	= (a-b) * fb / (fb - fa);

	fp Q22	= (D21-Q11) * fb / (fe - fb);
	fp Q32	= (D31-Q21) * fa / (fd - fa);
	fp D32	= (D31-Q21) * fd / (fd - fa);
	fp Q33	= (D32-Q22) * fa / (fe - fa);

	fp c	= a + Q31 + Q32 + Q33;

	// did we go out of bounds?
	if( (c <= a) || (c >= b) )
		c	= quadratic(a, b, d, fa, fb, fd, 3);

	return c;

	}

HEMI_DEV_CALLABLE fp toms748::sample( const fp target, const fp CDFmax, const fp scalar ) const {

	fp a, b, fa, fb, c, u, fu, a0, b0, d, fd, e, fe;

	// initialize these
	a	= -1.;
	b	=  1.;

	fa	= scalar;		// we already know these
	fb	= 0.;
	fe	= e = fd = 1e5f;	// dummy values
	
					// fixed first step
	c	= secant( a, b, fa, fb );
	bracket(a, b, c, fa, fb, d, fd, target, CDFmax);
	
					// if not successful, try a quadratic next
	if( !(tol(fa,0.) || tol(a,b,ep2)) ) {

		c	= quadratic( a, b, d, fa, fb, fd, 2);
		e	= d;
		fe	= fd;
		bracket( a, b, c, fa, fb, d, fd, target, CDFmax );

		}

	// iterate until we hit our hardwired cutoff
	int count	= maxIt;
	while( !(tol(fa,0.) || tol(a,b,ep2)) && (count-- > 0) ) {

		a0	= a;
		b0	= b;

		bool prof	= tol(fa,fb,ep32) || tol(fa,fd,ep32) || tol(fa,fe,ep32) ||
			tol(fb,fd,ep32) || tol(fb,fe,ep32) ||
			tol(fd,fe,ep32);

		if( prof )		// check if cubic is even possible
			c = quadratic( a, b, d, fa, fb, fd, 2 );
		else
			c = cubic( a, b, d, e, fa, fb, fd, fe );

		e = d;			// readjust brackets
		fe = fd;
		bracket(a, b, c, fa, fb, d, fd, target, CDFmax);

		if( tol(fa, 0.) || tol(a,b,ep2) )
			break;		// we found the zero

					// do another loop
		prof	= tol(fa,fb,ep32) || tol(fa,fd,ep32) || tol(fa,fe,ep32) ||
			tol(fb,fd,ep32) || tol(fb,fe,ep32) ||
			tol(fd,fe,ep32);

		if( prof )		// check if cubic is even possible
			c = quadratic( a, b, d, fa, fb, fd, 3 );	// TODO: why not 2?
		else
			c = cubic( a, b, d, e, fa, fb, fd, fe );

					// readjust brackets
		bracket(a, b, c, fa, fb, d, fd, target, CDFmax);
		if( tol(fa,0.) || tol(a,b,ep2) )
			break;		// we found the zero

		// double-length secant step
		if( fabs(fa) < fabs(fb) ) {

			u	= a;
			fu	= fa;
			}
		else {

			u	= b;
			fu	= fb;
			}

		c = u - 2.*(fu / (fb - fa)) * (b - a);

		if( fabs(c - u) > ((b - a) * .5) )
			c = a + (b - a)*.5;

		e = d;			// readjust brackets
		fe = fd;
		bracket(a, b, c, fa, fb, d, fd, target, CDFmax);

		if( tol(fa,0.) || tol(a,b,ep2) )
			break;		// we found the zero

					// if we beat bisection, skip ahead
		if( (b - a) < (mu * (b0 - a0)) )
			continue;

		e = d;			// otherwise, fall back to it
		fe = fd;
		bracket(a, b, a + (b-a)*.5, fa, fb, d, fd, target, CDFmax);

		} // while (no convergence)

	if( tol(fa,0.) )
		return a;
	else if( tol(fb,0.) )
		return b;

	return (a + b)*.5;		// CHEAT, assume the zero is exactly the mean

	} // toms748::sample

HEMI_DEV_CALLABLE void toms748::operator()(const uint o, const uint s) const {

	// use stride to flag GPU operation
	uint offset;
	uint stride;

	if( s == 0 ) {

		offset	= hemi::globalThreadIndex();
		stride	= hemi::globalThreadCount();
		}
	else {
		offset	= o;
		stride	= s;
		}

	// set up the scalar	
	fp CDFmax	= 0;
	fp CDFmin	= 0;
	hengreen1Idata( stored, CDFmax, CDFmin );
	fp scalar	= CDFmax - CDFmin;

	while( offset < samples ) {      // while we're within bounds

		fp target	= output[offset];

		// check for an early exit (TODO: worth it?)
		if( target < epsilon )
			output[offset]	= 1.;
		else if( (1. - target) < epsilon )
			output[offset]	= -1.;
		else
			output[offset]	= sample( target * scalar, CDFmax, scalar );

		offset          += stride;
		}

	} // toms748

void toms748_boost::operator()(const uint o, const uint stride) const {

	// make this easier to access
	const LinearHG stored         = { lhg_count, lhg_data };

	// set up the scalar	
	fp CDFmax	= 0;
	fp CDFmin	= 0;
	hengreen1Idata( stored, CDFmax, CDFmin );
	const fp scalar	= CDFmax - CDFmin;

	// set up the required precision
	int digits = -log(epsilon)/log(2.);
	eps_tolerance<fp> tol(digits);

	uint offset	= o;

	while( offset < samples ) {      // while we're within bounds

		// for each sample, pick a target
		fp target	= output[offset] * scalar;	// adjust this to the proper range
		uintmax_t iter	= max_iter;
		std::pair<fp,fp> bracket	= toms748_solve( [=](const fp cos_x) {
				
			return (CDFmax - hengreen1I( cos_x, stored )) - target;
			}, 
			(fp)-1., (fp)1., tol, iter );

		assert( iter > 0 );		// sanity check

		output[offset]	 = 0.5*(bracket.first + bracket.second);
		offset		+= stride;
		}

	} // toms748_boost

/**************************************************************************
 * SIMPLE FUNCTIONS
 */

uint BisectionSampler::id() const {		return (3 << 24) | 1; }
uint MyBisectionSampler::id() const {		return (3 << 24) | 2; }
uint TOMS748sampler::id() const {		return (3 << 24) | 16; }
uint MyTOMS748sampler::id() const {		return (3 << 24) | 17; }

string BisectionSampler::name() const {		return "root-finding via bisection, Boost"; }
string MyBisectionSampler::name() const {	return "root-finding via bisection"; }
string TOMS748sampler::name() const {		return "root-finding via TOMS748, Boost"; }
string MyTOMS748sampler::name() const {		return "root-finding via TOMS748"; }



/**************************************************************************
 * MyBisectionSampler
 */

bool MyBisectionSampler::setup( const Parameters& p, FineTiming* ft ) {

	if( !registered ) {

		cout << "ERROR: Why isn't MyBisectionSampler registered?!" << endl;
		return false;
		}

	// store some key variables
	params	= p;

	// capture some basic timing data
	ull startTime;
	if( ft != nullptr )
		startTime	= setTimer();

	// transfer data to the GPU, if necessary
	if( usingGPU() ) {

		freeGPU( lhg_gpu );
		lhg_gpu = mallocToGPU( params.lhg.data, params.lhg.count*2 );
		if( !testPointer(lhg_gpu, "ERROR: Could not allocate memory for the LHG on the GPU!") )
			return false;

		hemi::deviceSynchronize();
		}

	if( ft != nullptr )
		incTimer( startTime, ft->transferToDev );

	return true;

	} // MyBisectionSampler::setup

fp* MyBisectionSampler::sample( FineTiming* t ) const {

	// some preliminary timing setup
	ull startTime;
	if( t != nullptr )
		startTime	= setTimer();

	fp* output      = nullptr;
	if( !allocResults( &output, nullptr ) )
		return nullptr;

	if( t != nullptr )
		incTimer( startTime, t->transferToDev );

	// time to start computing!
	if( usingGPU() ) {
#ifdef __NVCC__
		bisection_uni calc( params.lhg.count, lhg_gpu, params.samples, output, 
				params.epsilon );
		hemi::launch( calc, 0, 0 );

		hemi::deviceSynchronize();
#else
		cout << "ERROR: How did you manage to enable the GPU without using nvcc?" << endl;
		return nullptr;
#endif
		}
	else {

		bisection_uni calc( params.lhg.count, params.lhg.data, params.samples, output, 
				params.epsilon );
		launch( calc );
		}

	if( t != nullptr )
		incTimer( startTime, t->computation );

	if( usingGPU() && !retrieveResultsFromGPU( &output ) ) {

		cout << "ERROR: Could not copy the results back from the GPU." << endl;
		return nullptr;
		}

	if( t != nullptr )
		incTimer( startTime, t->transferFromDev );

	return output;

	} // MyBisectionSampler::sample


/**************************************************************************
 * MyTOMS748sampler
 */

bool MyTOMS748sampler::setup( const Parameters& p, FineTiming* ft ) {

	if( !registered ) {

		cout << "ERROR: Why isn't MyTOMS748sampler registered?!" << endl;
		return false;
		}

	// store some key variables
	params	= p;

	// capture some basic timing data
	ull startTime;
	if( ft != nullptr )
		startTime	= setTimer();

	// transfer data to the GPU, if necessary
	if( usingGPU() ) {

		freeGPU( lhg_gpu );
		lhg_gpu = mallocToGPU( params.lhg.data, params.lhg.count*2 );
		if( !testPointer(lhg_gpu, "ERROR: Could not allocate memory for the LHG on the GPU!") )
			return false;

		hemi::deviceSynchronize();
		}

	if( ft != nullptr )
		incTimer( startTime, ft->transferToDev );

	return true;

	} // MyTOMS748sampler::setup

fp* MyTOMS748sampler::sample( FineTiming* t ) const {

	// some preliminary timing setup
	ull startTime;
	if( t != nullptr )
		startTime	= setTimer();

	fp* output      = nullptr;
	if( !allocResults( &output, nullptr ) )
		return nullptr;

	if( t != nullptr )
		incTimer( startTime, t->transferToDev );

	// time to start computing!
	if( usingGPU() ) {
#ifdef __NVCC__
		toms748 calc( params.lhg.count, lhg_gpu, params.samples, output, 
				params.epsilon, params.maxIt, params.mu );
		hemi::launch(calc, 0, 0);

		hemi::deviceSynchronize();
#else
		cout << "ERROR: How did you manage to enable the GPU without using nvcc?" << endl;
		return nullptr;
#endif
		}
	else {

		toms748 calc( params.lhg.count, params.lhg.data, params.samples, output, 
				params.epsilon, params.maxIt, params.mu );
		launch( calc );
		}

	if( t != nullptr )
		incTimer( startTime, t->computation );

	if( usingGPU() && !retrieveResultsFromGPU( &output ) ) {

		cout << "ERROR: Could not copy the results back from the GPU." << endl;
		return nullptr;
		}

	if( t != nullptr )
		incTimer( startTime, t->transferFromDev );

	return output;

	} // MyTOMS748sampler::sample

/**************************************************************************
 * BisectionSampler
 */

bool BisectionSampler::setup( const Parameters& p, FineTiming* ft ) {

	if( !registered ) {

		cout << "ERROR: Why isn't BisectionSampler registered?!" << endl;
		return false;
		}

	// store some key variables
	params	= p;

	// why bother with the timing data? There's no GPU transfer going on

	return true;
	}

fp* BisectionSampler::sample( FineTiming* t ) const {

	// some preliminary timing setup
	ull startTime;
	if( t != nullptr )
		startTime	= setTimer();

	fp* output      = nullptr;
	if( !allocResults( &output, nullptr ) )
		return nullptr;

	if( t != nullptr )
		incTimer( startTime, t->transferToDev );

	bisection_boost calc( params.lhg.count, params.lhg.data, params.samples, output, 
				params.epsilon );
	launch( calc );

	if( t != nullptr )
		incTimer( startTime, t->computation );

	return output;

	} // BisectionSampler::sample

/**************************************************************************
 * TOMS748sampler
 */

bool TOMS748sampler::setup( const Parameters& p, FineTiming* ft ) {

	if( !registered ) {

		cout << "ERROR: Why isn't BisectionSampler registered?!" << endl;
		return false;
		}

	// store some key variables
	params	= p;

	return true;

	} // TOMS748sampler::setup

fp* TOMS748sampler::sample( FineTiming* t ) const {

	// some preliminary timing setup
	ull startTime;
	if( t != nullptr )
		startTime	= setTimer();

	fp* output      = nullptr;
	if( !allocResults( &output, nullptr ) )
		return nullptr;

	if( t != nullptr )
		incTimer( startTime, t->transferToDev );

	toms748_boost calc( params.lhg.count, params.lhg.data, params.samples, output, 
				params.epsilon, params.maxIt );
	launch( calc );

	if( t != nullptr )
		incTimer( startTime, t->computation );

	return output;

	} // TOMS748sampler::sample

