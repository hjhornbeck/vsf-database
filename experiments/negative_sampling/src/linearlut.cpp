/**************************************************************************
 * INCLUDES
 */

#include "shared.h"

#ifdef __NVCC__
#include <curand_kernel.h>
#endif

#include <algorithm>
using std::shuffle;



/**************************************************************************
 * VARIABLES / TYPES
 */

REGISTER(LinearLUTsamplerV2)


/**************************************************************************
 * FUNCTORS
 */

struct linearLUTv2 : public unified_functor {

	const uint total;
	const uint LUToff;
	const fp* data	= nullptr;

	linearLUTv2(const uint a, fp* b, const uint c, fp* d,
			const uint e, const uint f, const fp* g ) :
		unified_functor(a, b, c, d), total{e}, LUToff{f}, data{g} {}

	// no RNG = the same code across GPU and CPU!
	HEMI_DEV_CALLABLE void operator()(const uint offset = 0, const uint stride = 1) const;

	};

HEMI_DEV_CALLABLE void linearLUTv2::operator()(const uint o, const uint s) const {

	uint offset;
	uint stride;

	if( s == 0 ) {

		offset	= hemi::globalThreadIndex();
		stride	= hemi::globalThreadCount();
		}
	else {
		offset	= o;
		stride	= s;
		}

	// pick a spot in the LUT
	uint i 		= offset + LUToff;	// localize memory access
	while( i >= total )
       		i -= total;

	while( offset < samples ) {

		output[offset]	 = data[i];
		offset		+= stride;
		i		+= stride;
		if( i >= total )
			i -= total;

		}

	} // linearLUTv2

/**************************************************************************
 * SIMPLE FUNCTIONS
 */

uint LinearLUTsamplerV2::id() const {		return (4 << 24) | 129; }

string LinearLUTsamplerV2::name() const {
	return "linearized look-up tables, version 2"; }

LinearLUTsamplerV2::~LinearLUTsamplerV2() {

	if( TABLE_gpu != nullptr )
		freeGPU( TABLE_gpu );
	}


/**************************************************************************
 * LinearLUTsamplerV2
 */

bool LinearLUTsamplerV2::setup( const Parameters& p, FineTiming* ft ) {

	if( !registered ) {

		cout << "ERROR: Why isn't LinearLUTsampler registered?!" << endl;
		return false;
		}

	// capture some basic timing data
	ull startTime;
	if( ft != nullptr )
		startTime	= setTimer();

	// store some key variables
	params		= p;
	LUTdim		= params.binSize * params.binCount;
	LUToff		= Shared::fpDist( Shared::RNGsource ) * LUTdim;
	TABLE_cpu.reserve( LUTdim );
	fp* output;

	// hijack another sampler to generate the table
	MyTOMS748sampler generator;

	Parameters genP	 = params;
	genP.samples	 = params.binSize;
	genP.seed	+= 2*params.CPUthreads + 1;	// ensure there's no dupes
	genP.stratified	 = true;

	// for each bin
	auto begin	= TABLE_cpu.begin();
	auto end	= TABLE_cpu.begin();	// this'll change
	for( uint bin = 0; bin < params.binCount; bin++ ) {

		// fire up the generator
		generator.setup( genP );
		genP.seed	+= 2*params.CPUthreads + 1;
		output		 = generator.sample();

		// insert and scramble the sample
		end 		 = TABLE_cpu.insert( TABLE_cpu.end(), 
				output, output + params.binSize );
		shuffle(	begin, TABLE_cpu.end(), Shared::RNGsource );
		begin		= end;
		}

	if( ft != nullptr )
		incTimer( startTime, ft->computation );

	if( usingGPU() ) {

		freeGPU( TABLE_gpu );
		TABLE_gpu = mallocToGPU( TABLE_cpu.data(), LUTdim );
		if( !testPointer(TABLE_gpu, "ERROR: Could not allocate memory for the LUT on the GPU!") )
			return false;

		hemi::deviceSynchronize();
		}

	if( ft != nullptr )
		incTimer( startTime, ft->transferToDev );

	return true;

	} // LinearLUTsampler::setup

fp* LinearLUTsamplerV2::sample( FineTiming* t ) const {

	// some preliminary timing setup
	ull startTime;
	if( t != nullptr )
		startTime	= setTimer();

	fp* output	= nullptr;
	if( !allocResults( &output, nullptr ) )
		return nullptr;

	if( t != nullptr )
		incTimer( startTime, t->transferToDev );

	// time to start computing!
	if( usingGPU() ) {
#ifdef __NVCC__
		linearLUTv2 calc( 0, nullptr, params.samples, output, 
				LUTdim, LUToff, TABLE_gpu );
		hemi::launch(calc, 0, 0);

		hemi::deviceSynchronize();
#else
		cout << "ERROR: How did you manage to enable the GPU without using nvcc?" << endl;
		return nullptr;
#endif
		}
	else {
		linearLUTv2 calc( 0, nullptr, params.samples, output, 
				LUTdim, LUToff, TABLE_cpu.data() );
		launch( calc );

		}

	if( t != nullptr )
		incTimer( startTime, t->computation );

	if( usingGPU() && !retrieveResultsFromGPU( &output ) ) {

		cout << "ERROR: Could not copy the results back from the GPU." << endl;
		return nullptr;
		}

	if( t != nullptr )
		incTimer( startTime, t->transferFromDev );

	return output;

	} // LinearLUTsamplerV2::sample

