/**************************************************************************
 * INCLUDES
 */

#include "shared.h"
using std::ref;

/**************************************************************************
 * VARIABLES
 */


/**************************************************************************
 * FUNCTIONS
 */

// basic memory work
bool Sampler::allocResults( fp** output, ull** misses ) const {

	// basic sanity check first
	if( output == nullptr )
		return false;

	// seed the output with values between [0:1]
	fp* temp	= new fp[ params.samples ];
	if( params.stratified ) {

		fp invBase = 1. / (fp)params.samples;
		for( uint it = 0; it < params.samples; it++ )
			temp[it]	= ((fp)it + Shared::fpDist( Shared::RNGsource )) * invBase;
		}
	else
		for( uint it = 0; it < params.samples; it++ )
			temp[it]	= Shared::fpDist( Shared::RNGsource );
	
	*output = temp;
	
	if( !testPointer( *output, "ERROR: Could not allocate the results array!" ) )
		return false;
	
	// only create the miss data storage if asked for it
	if( misses != nullptr ) {

		*misses = new ull[ getThreadCount() ];
	
		if( !testPointer( *misses, "ERROR: Could not allocate space to track misses!" ) )
			return false;
		}
	
	return true;

	} // Sampler::allocResults 

uint Sampler::getThreadCount() const {

	if( params.CPUthreads < 1 )
		return thread::hardware_concurrency();
	else
		return params.CPUthreads;

	}

void Sampler::incTimer( ull& start, ull& target ) {

	ull end	= Sampler::setTimer();
	target	= end - start;
	start	= end;
	}

void Sampler::launch( cpu_functor& obj ) const {

	uint threadCount	= getThreadCount();
	thread* threads		= new thread[threadCount];

	// launch, then wait
	for( uint i = 0; i < threadCount; i++ )
		threads[i]	= thread( ref(obj), i, threadCount );

	for( uint i = 0; i < threadCount; i++ )
		threads[i].join();

	}

// wasteful, but it prevents inheritance issues
void Sampler::launch( unified_functor& obj ) const {

	uint threadCount	= getThreadCount();
	thread* threads		= new thread[threadCount];

	// launch, then wait
	for( uint i = 0; i < threadCount; i++ )
		threads[i]	= thread( ref(obj), i, threadCount );

	for( uint i = 0; i < threadCount; i++ )
		threads[i].join();

	}

ull Sampler::setTimer() {

	return duration_cast<nanoseconds>(
			high_resolution_clock::now().time_since_epoch() ).count();
	}

bool Sampler::testPointer( void* source, string msg ) const {

	if( source == nullptr )
		cout << msg << endl;

	return (source != nullptr);
	}

