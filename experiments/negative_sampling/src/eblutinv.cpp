/**************************************************************************
 * INCLUDES
 */

#include "shared.h"

#ifdef __NVCC__
#include <curand_kernel.h>
#endif



/**************************************************************************
 * VARIABLES / TYPES
 */

REGISTER(ApproxLUTsampler)
REGISTER(ApproxPartLUTsampler)


/**************************************************************************
 * FUNCTORS
 */

struct approx_lut : public unified_functor {

	const uint LUTdim;
	fp* TABLE	= nullptr;

	approx_lut(const uint a, fp* b, const uint c, fp* d,
			const uint e, fp* f ) :
		unified_functor(a, b, c, d), LUTdim{e}, TABLE{f} {}

	// no RNG = the same code across GPU and CPU!
	HEMI_DEV_CALLABLE void operator()(const uint offset, const uint stride) const;

	};

struct approx_part_lut : public unified_functor {

	fp* LUTmeta	= nullptr;
	uint* LUToff	= nullptr;
	fp* TABLE	= nullptr;

	approx_part_lut(const uint a, fp* b, const uint c, fp* d,
			fp* e, uint* f, fp* g ) :
		unified_functor(a, b, c, d), LUTmeta{e}, LUToff{f}, TABLE{g} {}

	HEMI_DEV_CALLABLE void operator()(const uint offset, const uint stride) const;

	};


// now fill out those functors
HEMI_DEV_CALLABLE void approx_lut::operator()(const uint o, const uint s) const {

	// use stride to flag GPU operation
	uint offset;
	uint stride;

	if( s == 0 ) {

		offset	= hemi::globalThreadIndex();
		stride	= hemi::globalThreadCount();
		}
	else {
		offset	= o;
		stride	= s;
		}

	while( offset < samples ) {

		fp frac		 = output[offset] * (fp)(LUTdim + 1);	// split into bin and fraction of bin
		int bin		 = (int)frac;
		frac		-= (fp)bin;

		fp left;
		fp right;

		// compensate for any missing values
		if( bin == 0 )
			left	= 1.f;
		else
			left	= TABLE[bin-1];

		if( bin >= LUTdim )
			right	= -1.f;
		else
			right	= TABLE[bin];

		output[offset]	 = (1.f-frac)*left + frac*right;
		offset 		+= stride;
		}

	} // approx_lut

HEMI_DEV_CALLABLE void approx_part_lut::operator()(const uint o, const uint s) const {

	// use stride to flag GPU operation
	uint offset;
	uint stride;

	if( s == 0 ) {

		offset	= hemi::globalThreadIndex();
		stride	= hemi::globalThreadCount();
		}
	else {
		offset	= o;
		stride	= s;
		}

	while( offset < samples ) {

		/* a handy translation guide:
		  LUTbounds	= LUTmeta;
		  LUTscale 	= LUTmeta + 2;
		  LUTrange	= LUTmeta + 5;
		*/

		fp left;			// fill these in, based on which fraction we're in
		fp right;
		int index;
		fp target	= output[offset];

		if( target < LUTmeta[0] ) {		// forward scatter?

			left		= 1.f;		// fill in defaults
			right		= 0.5f;
			index		= 0;
			}

		else if( target > LUTmeta[1] ) {	// backward scatter?

			target		-= LUTmeta[1];

			left		= -0.5f;
			right		= -1.f;
			index		= 2;
			}
		else {

			target		-= LUTmeta[0];		// LUTbounds

			left		= 0.5f;
			right		= -0.5f;
			index		= 1;
			}

		fp frac		 = target*LUTmeta[index + 2];	// LUTscale
		uint bin	 = (uint)frac;
		frac		-= (fp)bin;

		if( bin > 0 )
			left	= TABLE[LUToff[index] + bin - 1];	// LUToff
		if( bin < LUTmeta[index+5] )				// LUTrange
			right	= TABLE[LUToff[index] + bin];

		output[offset]	 = (1.f-frac)*left + frac*right;
		offset 		+= stride;
		}

	} // approx_part_lut


/**************************************************************************
 * SIMPLE FUNCTIONS
 */

uint ApproxLUTsampler::id() const {		return (4 << 24) | 1; }
uint ApproxPartLUTsampler::id() const {		return (4 << 24) | 2; }

string ApproxLUTsampler::name() const {
	return "equal-bin approximate look-up tables"; }
string ApproxPartLUTsampler::name() const {
	return "partitioned equal-bin approximate look-up tables"; }

// tidy up any leftover memory
ApproxLUTsampler::~ApproxLUTsampler() {

	if( TABLE_gpu != nullptr )
		freeGPU( TABLE_gpu );

	if( TABLE_cpu != nullptr )
		delete[] TABLE_cpu;

	}

ApproxPartLUTsampler::~ApproxPartLUTsampler() {

	if( LUTmeta_gpu != nullptr )
		freeGPU( LUTmeta_gpu );
	if( LUToff_gpu != nullptr )
		freeGPU( LUToff_gpu );

	if( LUTmeta_cpu != nullptr )
		delete[] LUTmeta_cpu;
	if( LUToff_cpu != nullptr )
		delete[] LUToff_cpu;

	}



/**************************************************************************
 * ApproxLUTsampler
 */

// a helper to make clipping to the data easier
int ApproxLUTsampler::clip( const int index ) const {

	// sanity checks
	assert( data.size > 0 );

	if( index < 0 )
		return 0;
	else if( index >= data.size )
		return (data.size - 1);

	return index;

	}

// convert an index to a cosine (allow sloppy bounds)
fp ApproxLUTsampler::index2val( const int index ) const {	
	
	return 1.f - 2.f * (fp)index / (fp)(LUTdim-1); 
	}

// calculate the weight via Cox-de Boor's algorithm
fp ApproxLUTsampler::coxDeBoor( const uint degree, const fp cos_x, const int index ) const {

	// sanity checks
	assert( data.size > 0 );
	assert( data.angle != nullptr );

	fp left		= 0.f;
	fp right	= 0.f;

	// terminate one step earlier than usual to save 50% of all function calls
	if( degree == 1 ) {

		if( cos_x > data.angle[clip( index+1 )] ) {
			
			if( data.angle[clip( index )] >= cos_x )
				left	= 1.f;
			}
		else if( cos_x > data.angle[clip( index+2 )] )
			right = 1.f;

		} // if (degree == 1)
	else {

		left	= coxDeBoor( degree-1, cos_x, index );
		right	= coxDeBoor( degree-1, cos_x, index+1 );
		}

	if( left > 0.f ) {

		int tempInd	= clip(index);
		left	*= (cos_x - data.angle[tempInd]) / (data.angle[clip( index+degree )] - data.angle[tempInd]);
		}

	if( right > 0.f ) {

		int tempInd	= clip( index+degree+1 );
		right	*= (data.angle[tempInd] - cos_x) / (data.angle[tempInd] - data.angle[clip( index+1 )]);
		}

	return left + right;
	}

// evaluate the CDF of the cubic B-spline basis function at the given value
fp ApproxLUTsampler::cubic_cdf( const uchar type, const fp x ) const {

	if ( (x < 0) || (x > 1) )
		return 0;

	switch( type ) {

		case 0: {
			
			fp x2	= x*x;
			return x2 * x2 * (1.f/24.f);
			}

		case 1:

			return x*( (1.f/6.f) + x*( 0.25 + x*( (1.f/6.f) - x*0.125 )));

		case 2:

			return x*( (2.f/3.f) + x*x*( (-1.f/3.f) + x*0.125 ));

		case 3: {

			if (x < 1e-7)	// via http://herbie.uwplse.org/demo/
				return (x/24.f) * ((x*4.f)*x - (x*6.f - 4.f));
			else {
				fp xo = (x - 1);
				fp x2 = xo*xo;
				return (-1.f/24.f)*x2*x2 + (1.f/24.f);
				}
			}
		}
	
	assert( 1 == 0 );	// the remaining code doesn't error check, so make this fatal
	return -1.f;		//  but if it did, here's an error flag
	}

// sample the incoming PDF. A linear search is faster, but random-access is more useful for an implementation
fp ApproxLUTsampler::orderedSamplePDF( const fp cos_x ) const {

	// sanity checks
	assert( data.size > 0 );
	assert( data.angle != nullptr );
	assert( data.likelihood != nullptr );

	if( cos_x >= 1.f )
		return data.likelihood[0];

	else if( cos_x <= -1.f )
		return data.likelihood[data.size-1];

	// yep, binary search. SHOCKER, I know.
	int left	= 0;
	int right	= data.size - 1;
	while( (right - left) > 1 ) {

		int middle	= (left + right) >> 1;		// KLUDGE: dies for PDFs with billions of entries
		if( data.angle[middle] > cos_x )
			left	= middle;
		else
			right	= middle;

		}

	// ok, we're somewhere between left and right. Scan down until our weights disappear
	fp retVal	 = 0.f;			// obvs
	fp weight	 = 0.f;

	for( int i = 0; i < OFFSET; i++ ) {	// tuned for odd degrees, may need tweaking for even ones

		weight	 = coxDeBoor( params.DEGREE, cos_x, left-OFFSET );
		retVal	+= data.likelihood[clip( left-- )] * weight;

		weight	 = coxDeBoor( params.DEGREE, cos_x, right-OFFSET );
		retVal	+= data.likelihood[clip( right++ )] * weight;
		}

	// double-check this
	// assert( abs(retVal - bruteSamplePDF( cos_x )) < params.epsilon );

	return retVal;

	} // orderedSamplePDF()

// fix the PDF buffer, if necessary
void ApproxLUTsampler::fixBuffer( const int index ) {

	// handle the lesser case first
	if( index < pdfIndex ) {

		int diff	= pdfIndex - index;
		for( int i = params.DEGREE; i >= 0; i-- ) {

			if( (i - diff) >= 0 )	// if we can copy rather than calculate, do so
				pdfBuffer[i]	= pdfBuffer[i-diff];
			else
				pdfBuffer[i]	= orderedSamplePDF( index2val(index + i) );

			}

		pdfIndex = index;

		}
	// otherwise, if the upper case needs working
	else if( index > pdfIndex ) {

		int diff	= index - pdfIndex;
		for( int i = 0; i <= params.DEGREE; i++ ) {

			if( (i + diff) <= params.DEGREE )
				pdfBuffer[i]	= pdfBuffer[i+diff];
			else
				pdfBuffer[i]	= orderedSamplePDF( index2val(index + i) );

			}

		pdfIndex = index;

		}

	// don't touch the index if we don't need to

	}
	
// sample the full CDF at the given bootstrap PDF index
fp ApproxLUTsampler::full_cubic_cdf( int index ) {

	// check assumptions
	assert( params.DEGREE >= 3 );

	index -= (params.DEGREE - OFFSET);		// move into the proper position

	if( index != pdfIndex )		// ensure the buffer is in place
		fixBuffer( index );

	return	(pdfBuffer[0] + pdfBuffer[3]) * ( 1.f/24.f) +
		(pdfBuffer[1] + pdfBuffer[2]) * (11.f/24.f);
	
	}

// same as above, but this time we're doing a partial sample
fp ApproxLUTsampler::partial_cubic_cdf( const fp x, int index ) {

	// check assumptions
	assert( params.DEGREE >= 3 );

	index -= (params.DEGREE - OFFSET);		// shift to the proper position

	if( index != pdfIndex )
		fixBuffer( index );

	if( (x < 0.f) || (x > 1.f) )	// catch a corner case
		return 0.f;

	fp sum	= 0.f;			// otherwise, tally up
	for( uchar c = 0; c < 4; c++ )
		sum	+= pdfBuffer[c] * cubic_cdf(3-c, x);

	return sum;

	}

// generate the incoming PDF from the provided HG function
void ApproxLUTsampler::generateRawPDF( const LinearHG& lhg ) {

	// size/allocate the incoming dataset
	data.size	= dataSize;		// c++11 for CUDA
	data.angle	= new fp[data.size];
	data.likelihood	= new fp[data.size];

	// assumption check
	assert( data.size > 0 );
	assert( data.angle != nullptr );
	assert( data.likelihood != nullptr );

	for( int it = 0; it < data.size; it++ ) {

		// this could be made more efficient, but it stresses more of the codepath
		fp angle		= cos( (fp)it / (fp)(data.size - 1) * PI );
		data.angle[it]		= angle;
		data.likelihood[it]	= Shared::hengreen0( angle, lhg );
		}

	// DEBUG
	if( params.verbose & 0x1 ) {

		cout << "DEBUG: raw PDF follows" << endl;
		for( int i = 0; i < data.size; i++ )
			cout << data.angle[i] << "\t" << data.likelihood[i] << endl;
		cout << endl;
		}

	} // generateRawPDF()

// create a partition in the LUT (NOTE: LUTend is EXCLUSIVE)
fp ApproxLUTsampler::createPartition( const uint LUTstart, uint& LUTend, const uint intPDFstart, const uint intPDFend ) {

	// check our assumptions
	assert( params.DEGREE	== 3 );
	assert( data.angle	!= nullptr );
	assert( data.likelihood	!= nullptr );

	// generate a sum of the underlying PDF
	fp retVal	= 0.f;
	fp error	= 0.f;		// see http://alex.uwplse.org/2015/10/16/improving-accuracy-summation.html
					// NOTE: this technique doubles the accuracy of fp, which is actually slower than 
					//  using doubles if fp == float.

	for( uint i = intPDFstart; i < intPDFend; i++ ) {

		fp current	= full_cubic_cdf( i );
		fp newSum	= retVal + current + error;
		error		= current - ((newSum - retVal) - error);
		retVal		= newSum;
		}

	// now use that sum to create and normalize the inverse CDF LUT
	fp accum		= 0.f;
	uint LUToffset		= 0;
	uint LUTdelta		= LUTend - LUTstart;
	uint intPDFindex	= intPDFstart;
	fp lastX		= 0.f;
	
	fp next		= full_cubic_cdf( intPDFindex );

	while( LUToffset < LUTdelta ) {

		// the bin boundary we're aiming for
		fp target	= (fp)(LUToffset+1) * retVal / (fp)(LUTdelta+1);

		// if we won't cross the boundary
		while( (accum + next) < target ) {

			accum	+= next;
			intPDFindex++;
			lastX	 = 0.f;	// no need to divide
			next	 = full_cubic_cdf( intPDFindex );
			}

		// if we will, binary search to find the exact location
		fp left		= lastX;
		fp right	= 1.f;
		while( (right-left) > params.epsilon ) {

			fp middle	= 0.5*(left + right);
			fp partNext	= partial_cubic_cdf( middle, intPDFindex );
			if( (accum + partNext) < target )
				left	= middle;
			else
				right	= middle;

			} // while( we haven't found the location )

		// estimate the true crossing point
		fp leftT	= accum + partial_cubic_cdf( left, intPDFindex );
		fp rightT	= accum + partial_cubic_cdf( right, intPDFindex );
		fp frac		= (rightT - target) / (rightT - leftT);

		// combine the fractional and index part
		lastX		= frac*left + (1.f-frac)*right;
		fp index	= (fp)intPDFindex + lastX;

		// then translate that into a cosine and save it
		TABLE_cpu[LUTstart + LUToffset]	= 1.f - 2. * (fp)index / (fp)(LUTdim-1);

		// sanity check: did we go out of bounds, due to numeric precision?
		if( TABLE_cpu[LUTstart + LUToffset] < -1.f ) {

			LUTend = LUTstart + LUToffset;		// shoot, then readjust our end point and bail
			break;
			}

		LUToffset++;

		} // while( there's more to add to the LUT )

	// any cleanup?
	// DEBUG
	if( params.verbose & 0x2 ) {

		cout << "DEBUG: LUT follows" << endl;
		for( uint i = LUTstart; i <= LUTend; i++ )
			cout << TABLE_cpu[i] << endl;
		cout << endl;
		}

	return retVal;

	} // createPartition

// set up the sampler
bool ApproxLUTsampler::setup( const Parameters& p, FineTiming* ft ) {

	if( !registered ) {

		cout << "ERROR: Why isn't ApproxLUTsampler registered?!" << endl;
		return false;
		}

	// capture some basic timing data
	ull startTime;
	if( ft != nullptr )
		startTime       = setTimer();

	// store some key variables
	params		= p;
       	LUTdim		= p.binSize;

	TABLE_cpu	= new fp[params.binSize];	// reserve some space
	pdfBuffer	= new fp[params.DEGREE + 1];

	generateRawPDF( p.lhg );
	createPartition( 0, LUTdim, 0, LUTdim-1 );	// sums are only needed for partitioned tables

	// sanity checks
	assert( TABLE_cpu[0]		> 0.f );
	assert( TABLE_cpu[LUTdim-1]	< 0.f );

	if( ft != nullptr )
		incTimer( startTime, ft->computation );

	// transfer data to the GPU, if necessary
	if( usingGPU() ) {

		freeGPU( TABLE_gpu );
		TABLE_gpu = mallocToGPU( TABLE_cpu, LUTdim );
		if( !testPointer(TABLE_gpu, "ERROR: Could not allocate memory for the LUT on the GPU!") )
			return false;

		hemi::deviceSynchronize();
		}

	if( ft != nullptr )
		incTimer( startTime, ft->transferToDev );

	return true;
	}

fp* ApproxLUTsampler::sample( FineTiming* t ) const {

	// some preliminary timing setup
	ull startTime;
	if( t != nullptr )
		startTime	= setTimer();

	fp* output	= nullptr;

	if( !allocResults( &output, nullptr ) )
		return nullptr;

	if( t != nullptr )
		incTimer( startTime, t->transferToDev );

	// time to start computing!
	if( usingGPU() ) {
#ifdef __NVCC__
		// some of these aren't needed
		approx_lut calc(0, nullptr, params.samples, output, LUTdim, TABLE_gpu);
		hemi::launch(calc, 0, 0);

		hemi::deviceSynchronize();		// force all memory activity to complete
#else
		cout << "ERROR: How did you manage to enable the GPU without using nvcc?" << endl;
		return nullptr;
#endif
		}
	else {

		approx_lut calc(0, nullptr, params.samples, output, LUTdim, TABLE_cpu);
		launch( calc );
		}

	if( t != nullptr )
		incTimer( startTime, t->computation );

	if( usingGPU() && !retrieveResultsFromGPU( &output ) ) {

		cout << "ERROR: Could not copy the results back from the GPU." << endl;
		return nullptr;
		}

	if( t != nullptr )
		incTimer( startTime, t->transferFromDev );

	return output;

	} // ApproxLUTsampler::sample

/**************************************************************************
 * ApproxPartLUTsampler
 */

bool ApproxPartLUTsampler::setup( const Parameters& p, FineTiming* ft ) {

	if( !registered ) {

		cout << "ERROR: Why isn't ApproxPartLUTsampler registered?!" << endl;
		return false;
		}

	// capture some basic timing data
	ull startTime;
	if( ft != nullptr )
		startTime       = setTimer();

	// store some key variables
	params		= p;
	LUTdim 		= p.binSize;

	TABLE_cpu	= new fp[params.binSize];	// reserve some space
	LUTmeta_cpu	= new fp[ 2 + 3*3 ];
	LUToff_cpu	= new uint[3];
	pdfBuffer	= new fp[params.DEGREE + 1];

        generateRawPDF( p.lhg );

	/* a handy translation guide:
	  LUTbounds	= LUTmeta;
	  LUTscale 	= LUTmeta + 2;
	  LUTrange	= LUTmeta + 5;
	*/

	LUToff_cpu[0]	= 0;			// use these to accelerate LUT lookup
	LUToff_cpu[1]	= LUTdim/3;
	LUToff_cpu[2]	= (2*LUTdim)/3;

					// cos(theta) \in [1,0.5]
	fp sum		= createPartition(	LUToff_cpu[0], LUToff_cpu[1], 
						0, LUTdim >> 2 );
	LUTmeta_cpu[0]	= sum;

					// cos(theta) \in [0.5,-0.5]
	sum		+= createPartition(	LUToff_cpu[1], LUToff_cpu[2], 
						(LUTdim >> 2), (3*LUTdim) >> 2 );
	LUTmeta_cpu[1]	 = sum;

					// cos(theta) \in [-0.5,-1]
	sum		+= createPartition(	LUToff_cpu[2], LUTdim,
						(3*LUTdim) >> 2, LUTdim-1 );


	LUTmeta_cpu[0]	/= sum;			// renormalize
	LUTmeta_cpu[1]	/= sum;

						// use these to accelerate LUT lookup
	LUTmeta_cpu[5 + 0]	= LUToff_cpu[1] - LUToff_cpu[0];
	LUTmeta_cpu[5 + 1]	= LUToff_cpu[2] - LUToff_cpu[1];
	LUTmeta_cpu[5 + 2]	= LUTdim -  LUToff_cpu[2];

	LUTmeta_cpu[2 + 0]	= (fp)LUTmeta_cpu[5 + 0] / LUTmeta_cpu[0];
	LUTmeta_cpu[2 + 1]	= (fp)LUTmeta_cpu[5 + 1] / (LUTmeta_cpu[1] - LUTmeta_cpu[0]);
	LUTmeta_cpu[2 + 2]	= (fp)LUTmeta_cpu[5 + 2] / (1.f - LUTmeta_cpu[1]);

        // sanity checks
	assert( TABLE_cpu[0]		> 0.f );
	assert( TABLE_cpu[LUTdim-1]	< 0.f );

	if( ft != nullptr )
		incTimer( startTime, ft->computation );

	// transfer data to the GPU, if necessary
	if( usingGPU() ) {

		freeGPU( TABLE_gpu );
		TABLE_gpu = mallocToGPU( TABLE_cpu, LUTdim );
		if( !testPointer(TABLE_gpu, "ERROR: Could not allocate memory for the LUT on the GPU!") )
			return false;

		freeGPU( LUTmeta_gpu );
		LUTmeta_gpu = mallocToGPU( LUTmeta_cpu, 2 + 2*3 );
		if( !testPointer(LUTmeta_gpu, "ERROR: Could not allocate memory for the LUT metadata on the GPU!") )
			return false;

		freeGPU( LUToff_gpu );
		LUToff_gpu = (uint*) mallocToGPU( (fp*)LUToff_cpu, 3 * sizeof(uint)/sizeof(fp) );
		if( !testPointer(LUToff_gpu, "ERROR: Could not allocate memory for the LUT metadata on the GPU!") )
			return false;


		hemi::deviceSynchronize();
		}

	if( ft != nullptr )
		incTimer( startTime, ft->transferToDev );

	return true;
	}

fp* ApproxPartLUTsampler::sample( FineTiming* t ) const {

	// some preliminary timing setup
	ull startTime;
	if( t != nullptr )
		startTime	= setTimer();

	fp* output	= nullptr;

	if( !allocResults( &output, nullptr ) )
		return nullptr;

	if( t != nullptr )
		incTimer( startTime, t->transferToDev );

	// time to start computing!
	if( usingGPU() ) {
#ifdef __NVCC__
		// some of these aren't needed
		approx_part_lut calc(0, nullptr, params.samples, output, 
				LUTmeta_gpu, LUToff_gpu, TABLE_gpu);
		hemi::launch(calc, 0, 0);

		hemi::deviceSynchronize();		// force all memory activity to complete
#else
		cout << "ERROR: How did you manage to enable the GPU without using nvcc?" << endl;
		return nullptr;
#endif
		}
	else {

		approx_part_lut calc(0, nullptr, params.samples, output, 
				LUTmeta_cpu, LUToff_cpu, TABLE_cpu);
		launch( calc );
		}

	if( t != nullptr )
		incTimer( startTime, t->computation );

	if( usingGPU() && !retrieveResultsFromGPU( &output ) ) {

		cout << "ERROR: Could not copy the results back from the GPU." << endl;
		return nullptr;
		}

	if( t != nullptr )
		incTimer( startTime, t->transferFromDev );

	return output;

	} // ApproxLUTsampler::sample
