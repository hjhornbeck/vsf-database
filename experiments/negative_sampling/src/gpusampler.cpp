/**************************************************************************
 * INCLUDES
 */

#include "shared.h"


/**************************************************************************
 * VARIABLES
 */


/**************************************************************************
 * FUNCTIONS
 */

// basic GPU memory work
bool GPUsampler::allocResults( fp** output, ull** misses ) const {

	// basic sanity check first
	if( output == nullptr )
		return false;

	// get our parent to do the grunt work
	if( !Sampler::allocResults( output, misses ) )
		return false;

	// branch, depending on whether or not we're on the GPU
        if( usingGPU() ) {

		fp* temp	= mallocToGPU( *output, params.samples );
		if( !testPointer( temp, "ERROR: Could not allocate the GPU results array!" ) )
			return false;

		delete[] *output;	// no need for the old memory pool
		*output	= temp;

		// only create the miss data storage if asked for it
		if( misses != nullptr ) {

			delete[] *misses;
			*misses	= (ull*) mallocGPU( sizeof(ull) / sizeof(fp), (fp*) misses );
	
			if( !testPointer( *misses, "ERROR: Could not allocate GPU space to track misses!" ) )
				return false;
			}

		hemi::deviceSynchronize();              // force all memory activity to complete
	
		}
	
	return true;

	} // GPUsampler::allocResults 

bool GPUsampler::freeGPU( void* old_pointer ) const {

	if( old_pointer == nullptr )
		return true;

#ifdef __NVCC__

	return (cudaFree(old_pointer) == cudaSuccess);
#else
	return true;		// no-op for CPU
#endif

	}

fp* GPUsampler::mallocGPU( const uint c, void* old_pointer ) const {

#ifdef __NVCC__

	if( old_pointer != nullptr )
		cudaFree(old_pointer);		// just assume it worked

	void* target		= nullptr;
	cudaError_t result	= cudaMalloc( &target, c*sizeof(fp) );
	if( result == cudaSuccess )
		return (fp*)target;
	else
#endif
		return nullptr;
	}

fp* GPUsampler::mallocToGPU( const fp* source, const uint c ) const {

	fp* target	= mallocGPU( c );
	if( (target != nullptr) && memToGPU( source, target, c ) )
		return target;
	
	return nullptr;
	}

bool GPUsampler::memFromGPU( const fp* source, const fp* target, const uint c ) const {

	if( (source == nullptr) || (target == nullptr) )
		return false;

#ifdef __NVCC__

	cudaError_t result	= cudaMemcpy( (void*)target, (void*)source, 
			c*sizeof(fp), cudaMemcpyDeviceToHost );
	if( result == cudaSuccess )
		return true;
	else
#endif
		return false;

	}

bool GPUsampler::memToGPU( const fp* source, const fp* target, const uint c ) const {

	if( (source == nullptr) || (target == nullptr) )
		return false;

#ifdef __NVCC__

	cudaError_t result	= cudaMemcpy( (void*)target, (void*)source, 
			c*sizeof(fp), cudaMemcpyHostToDevice );
	if( result == cudaSuccess )
		return true;
	else
#endif
		return false;

	}

bool GPUsampler::retrieveResultsFromGPU( fp** output, ull** misses ) const {
	
	// invalid output == immediate failure
	if ( (output == nullptr) || (*output == nullptr) )
		return false;

	// CPU = nothing to transfer
	if ( !usingGPU() )
       		return true;

	fp* host        = new fp[ params.samples ];
	if( !memFromGPU(*output, host, params.samples) ) {

		cout << "ERROR: Could not copy the results back from the GPU." << endl;
		return false;
		}

#ifdef __NVCC__
	assert( cudaFree(*output) == cudaSuccess );
#endif
	*output  = host;

	// GPU routines drop everything into one variable via atomics
	if( misses != nullptr ) {

		ull* miss_host	= new ull[ 1 ];
		if( !memFromGPU( (fp*)*misses, (fp*)miss_host, sizeof(ull)/sizeof(fp)) ) {

			cout << "ERROR: Could not copy the misses back from the GPU." << endl;
			return false;
			}
#ifdef __NVCC__
		assert( cudaFree(*misses) == cudaSuccess );
#endif
		*misses = miss_host;
		}

	hemi::deviceSynchronize();              // force all memory activity to complete

	return true;

	} // GPUsampler::retrieveResultsFromGPU

bool GPUsampler::setCompute( const int d ) {

	if( d == -1 ) {		// CPU is always valid

		device = -1;
		return true;
		}
#ifdef __NVCC__

	int deviceCount	= 0;
	cudaError_t result;
	result = cudaGetDeviceCount( &deviceCount );
	if (result != cudaSuccess) {			// simple check of GPU status

		cout << "ERROR: CUDA error found, " << cudaGetErrorString(result) << endl;
		return false;
		}

	if ( (d >= 0) && (d < deviceCount) ) {

		cudaSetDevice( d );
		device	= d;
		return true;
		}
#endif

	return false;		// default to failure

	}
