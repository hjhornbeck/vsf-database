/**************************************************************************
 * INCLUDES
 */

#include "shared.h"

#ifdef __NVCC__
#include <curand_kernel.h>
#endif


/**************************************************************************
 * VARIABLES / TYPES
 */

// register this class
REGISTER(ConvexSampler)
	

/**************************************************************************
 * FUNCTORS
 */

// set up a base class shared by both versions
struct convex_cpu : public cpu_functor {

	fp* cdf;
	const ull seed;

	convex_cpu( const uint a, fp* b, const uint c, fp* d, fp* e,
				const ull f ) :
		cpu_functor(a,b,c,d), cdf{e}, seed{f} {}

	void operator()(const uint offset, const uint stride) const;

	}; // struct convex_cpu

void convex_cpu::operator()(const uint offset, const uint stride) const {

	// we need to create a local RNG source (add 1 to separate offset 0 from the main thread)
	mt19937 RNGsource( seed + offset + 1 );

        // other variables we'll need
        const LinearHG stored         = { lhg_count, lhg_data };

	fp target;		// make things a bit easier later
	uint o	= offset;	// (this is writable)

        while( o < samples ) {      // while we're within bounds

		// pick a value from the CDF
		target	= output[o];

		uint index	= 0;		// linear, as binary's might have less divergence
		while( index < stored.count )	// TODO: does it?
			if( cdf[index] >= target )
				break;
			else
				index++;

		// got it, time to sample!
		target		= Shared::fpDist( RNGsource );
		output[o]	= Shared::hengreenSamp( target, lhg_data[index + lhg_count] );
		o		+= stride;
		}

	} // convex_cpu

// now create the GPU-specific version
struct convex_gpu : public gpu_functor {	
	
	fp* cdf;
	const ull seed;

	convex_gpu( const uint a, fp* b, const uint c, fp* d, fp* e,
				const ull f ) :
		gpu_functor(a,b,c,d), cdf{e}, seed{f} {}

	HEMI_LAMBDA void operator()() const;
	};

HEMI_LAMBDA void convex_gpu::operator()() const {

#ifdef __NVCC__
	// set up the RNG
	curandState_t RNGstate;		// curand keeps us from merging these two
        curand_init( seed, hemi::globalThreadIndex(), 0, &RNGstate );

        // other variables we'll need
        const LinearHG stored         = { lhg_count, lhg_data };

	// set up our location within the output
        uint o          = hemi::globalThreadIndex();
	uint stride     = hemi::globalThreadCount();

	fp target;		// make things a bit easier later

        while( o < samples ) {      // while we're within bounds

		// pick a value from the CDF
		target	= output[o];

		uint index	= 0;		// linear, as binary's might have less divergence
		while( index < stored.count )	// TODO: does it?
			if( cdf[index] >= target )
				break;
			else
				index++;

		// got it, time to sample!
		target		= curand_uniform(&RNGstate);
		output[o]	= Shared::hengreenSamp( target, lhg_data[index + lhg_count] );
		o		+= stride;
		}
#endif
	} // convex_gpu


/**************************************************************************
 * FUNCTIONS
 */

// we should clean up our allocated memory
ConvexSampler::~ConvexSampler() {

	if( cdf_cpu )
		delete[] cdf_cpu;

	freeGPU(cdf_gpu);
	freeGPU(lhg_gpu);

	}

// the usual ID functions
uint ConvexSampler::id() const {	return 0; }	// always first!
string ConvexSampler::name() const {	return "stock convex sampling"; }

// unified setup 
bool ConvexSampler::setup( const Parameters& p, FineTiming* t ) {

	if( !registered ) {

		cout << "ERROR: Why wasn't ConvexSampler registered?" << endl;
		return false;
		}

	params		= p;

	if( cdf_cpu )			// don't lose existing memory
		delete[] cdf_cpu;

	cdf_cpu		= new fp[ params.lhg.count ];
	fp accum	= 0;		// set up a stored CDF to sample from the weights

        ull startTime;			// might as well capture some timing data
        if( t != nullptr )
		startTime       = setTimer();

	// build the CDF
	for( int i = 0; i < params.lhg.count; i++ ) {

		accum		+= params.lhg.data[i];
		cdf_cpu[i]	 = accum;
		}

	// renormalize
	for( int i = 0; i < params.lhg.count; i++ )
		cdf_cpu[i]	/= accum;

	cdf_cpu[ params.lhg.count - 1 ] = 1.00001;		// ensure precision doesn't cause issues

        if( t != nullptr )
		incTimer( startTime, t->computation );

	// transfer the array and LinearHG to the GPU, if necessary

	if( usingGPU() ) {

		freeGPU(cdf_gpu);
		cdf_gpu	= mallocToGPU( cdf_cpu, params.lhg.count );
	       	if( !testPointer(cdf_gpu, "ERROR: Could not allocate memory for the CDF on the GPU!") )
			return false;

		freeGPU(lhg_gpu);
		lhg_gpu	= mallocToGPU( params.lhg.data, params.lhg.count*2 );
	       	if( !testPointer(lhg_gpu, "ERROR: Could not allocate memory for the LHG on the GPU!") )
			return false;

		hemi::deviceSynchronize();              // force all memory activity to complete
		}

	if( t != nullptr )
		incTimer( startTime, t->transferToDev );

        return true;

	}

// do the actual sample
fp* ConvexSampler::sample( FineTiming* t ) const {

	// some preliminary timing setup
        ull startTime;
        if( t != nullptr )
                startTime       = setTimer();

	// make room for the return
	bool useGPU		= usingGPU();

	fp* output		= nullptr;
	if( !allocResults( &output ) )
		return nullptr;

        if( t != nullptr )
		incTimer( startTime, t->transferToDev );

	// time to start computing!
	if( useGPU ) {
#ifdef __NVCC__
		convex_gpu calc( params.lhg.count, lhg_gpu, params.samples, 
				output, cdf_gpu, params.seed );
		hemi::launch(calc);

		hemi::deviceSynchronize();      // force all computation to complete
#else
		cout << "ERROR: How did you managed to enable the GPU without using nvcc?" << endl;
		return nullptr;
#endif
		}
	else {

		convex_cpu calc( params.lhg.count, params.lhg.data, params.samples, 
				output, cdf_cpu, params.seed );
		launch( calc );
		}

        if( t != nullptr )
		incTimer( startTime, t->computation );

	// copy results and return
	if( useGPU && !retrieveResultsFromGPU( &output ) ) {

			cout << "ERROR: Could not copy the results back from the GPU." << endl;
			return nullptr;
			}

        if( t != nullptr )
		incTimer( startTime, t->transferFromDev );

        return output;                  // return the goods

        }
