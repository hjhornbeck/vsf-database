The Volume Scattering Function Database
=======================================

# Charts
This directory contains the database's collection of charts, which allow you to qualitatively examine each material.

## chroma
These charts show off the quality of [individual fits](chroma). Full chromatic data is shown. Combinations with convex or positive weights) are kept separate from those with both positive and negative weights.

## compare
These combine all the fits for a given material into [one chart](compare), so you can see how adding more components improves the fit. To reduce clutter, these are monochromatic.

## progress
Each fit file contains a status update of how the fit is progressing. This provides valuable information about convergence. The best result is a squiggle with no net upward or downward trend; the second best is a rapid climb to a plateau; and anything else could be a sign that the fit needs more processing time.

Rather than eyeball each fitness result, those values have been [converted into charts](progress). Be wary of the linear trend lines on each; it was intended to double-check a human guesstimate of how flat the chart was, but with shorter burn-in phases that are at maximal convergence it can exaggerate small fluctuations into faux trends.

## scatter
These chart the [posterior of every fit](scatter), with weight along the vertical axis and anisotropy (g) along the horizontal. Chromatic data is shown, as well as maximal likelihoods. Arguably, these are the most important of all the charts.