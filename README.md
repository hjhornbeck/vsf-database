The Volume Scattering Function Database
=======================================

# Introduction
Surface scattering has seen significant advances over the last few decades. We've gone from Cook-Torrence[^7] to multiscatter GGX[^8] and Disney's Principled shader[^9]. Volume scattering has not seen the same progress; it's telling that the primary scattering function we use, the [Henyey-Greenstein](https://www.astro.umd.edu/~jph/HG_note.pdf), is over 60 years old. One possible reason for this is the MERL database[^2], a collection of measured surface reflectances from a wide variety of materials. Easy access to that kind of data helped fuel the study of some of the above bidirectional scattering distribution functions (BSDFs).

Volume scattering could use a similar database. So as a suppliment to our paper,[^1] we've gathered as many volume scattering function (VSF) samples as we can find in one place, in the hope that it triggers the same spark of innovation. 

We're also willing to go beyond the MERL template. Here you will also find several Open Shading Language approximations of each VSF, which you can directly plug into Blender's Cycles, Appleseed, prman, Arnold, Clairisse, and other renderers. For those without OSL support, you can always use the code as a template. There is also code you can use to generate your own VSFs, based on Mie scattering with specific particle sizes, as well as code for generating your own fits.

# Results
Here's the [Walt Disney Animation Studios cloud dataset](https://www.technology.disneyanimation.com/clouds) (CC-BY-SA 3.0), imported into Blender, and with Material 33 applied. The top-most reference is [the photo this cloud was based on](https://coclouds.com/436/cumulus/2012-07-26/), copyright Kevin Udy (CC-BY-SA 3.0).

![A variety of VSFs applied to the cloud, with lighting from three seperate angles. Reference images are supplied, too.](images/wdas_octaves.cropped.jpg)

Why is this shader so bright? There's a significant amount of backscatter in clouds, which isn't well-captured by a single Henyey-Greenstein function. When the primary light source is from behind the viewer, then, the solo Henyey-Greenstein bounces back less light and appears darker. Conversely, the narrow forward scatter isn't well captured by an isotropic distribution either. Light paths in the former tend to be less twisted and more direct, so once a ray path bounces towards a light source it is likely to hit. As a result, this shader looks good no matter where the light is placed.

# A Quick Tour
An [index of the materials](index.ods) is available, and includes statistics on how well the shaders fit the data.

The OSL shaders themselves are available [here](osl_code). If you'd like the point samples of the scattering distributions, you can find them [here](originals). If you'd like some charts which show off those originals as well as the shaders fitted to them, [we have you covered](charts) too. Various utilities used to generate a few of these distributions and maintain this database can be found [here](utilities).

# How the Shaders Work
The basic concept behind the OSL shaders is simple: rather than invent a new type of scattering function, we're making better use of code that already exists. Most ray and path tracers support blending together several VSFs, in the same way you can blend together several BSDFs. Most ray and path tracers support the Henyey-Greenstein distribution. So why not fit a linear combination of Henyey-Greenstein functions to a more complicated scattering function?

![Comparing various fits for Material 33](images/hengreen_positives.png)

This isn't perfect, by any means; it tends to have too much side scattering, and it has difficulty replicating the shape of the forward scattering region (let alone the [fogbow](https://en.wikipedia.org/wiki/Fog_bow#Direction) and [glory](https://en.wikipedia.org/wiki/Glory_(optical_phenomenon))). On the other hand, it is a net improvement, it is already supported in most ray and path tracers, and it isn't much slower than a solo Henyey-Greenstein function.

If you are worried about performance anyway, we've got you covered. As the chart implies, each of the materials in the database has been fitted to a varying number of Henyey-Greenstein combinations. Pick the one that suits your needs the best.

## Chromatic Scattering
These shaders also come in two variations. Many VSFs have some level of spectral dependence; take a look at a cloud that's partly covering the Moon, for instance, and you'll probably see [a faint coloured halo](https://www.bbc.co.uk/programmes/p037f0rx/p037f0pz). Unfortunately, most ray and path tracers don't do spectral scattering, and some of those that do don't implement it for volumes. In some renderers we can split the colour channels apart and apply a VSF to each, but that tends to inject a tonne of noise while slowing things down. It's also prone to banding.

A lot of this spectral dependence is pretty slight, though. Rather than split apart the channels, we can keep them together but apply a colour tint to each Henyey-Greenstein component. This gives us something pseudo-spectral, without having to modify the renderer.

![A spectral fit for Material 33. Only the red and blue channels are shown.](images/chromatic_comparison.png)

In practice, the limitations of Henyey-Greenstein functions tend to get in the way of a good fit. We've found that the cool end of the spectrum tends to be attenuated, leading to a bluish cast in most cases. There may be room for artistic tweaking of the fit, but in the meantime we'll also offer an achromatic fit for each material.

## Some Caveats
We've had to cut some corners to make this project managable. The scattering functions we derived from the work of Gkioulekas _et_ _al._[^3] lack any chromatic component. While OPAC[^4] contains an excellent collection of VSFs, of which only a small portion are reproduced here, that program fixes the spectral channels and angles you can sample. Extrapolating one intermediate spectral sample from two others is usually non-trivial, so instead we've used them as-is. Consequently, the spectral component is likely too narrow within your renderer of choice and doesn't line up with reality.

The VSFs based on the summaries of Bouthers _et_ _al._[^5] and Cornette _et_ _al._[^6] have a perfect chromatic fit, but unlike OPAC they only incorporate scattering due to water droplets. Readers may have to use our program `deirmendjian` to [add more components](utilities/deirmendjian).

# Helping Contribute
This database in general is released under a [Creative Commons Attribution-ShareAlike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/) license, though parts of it may be covered by other licenses.

This database is also a living document. By using version control, anyone can submit patches which add to or correct it. We welcome contributions from other users, and have set up an email account for the sole purpose of handling them, ![protected](images/address.png). If you don't like our curation or moderation, you are free to fork the database and do it your way. so long as you respect all licenses.

# Citations and References
[^1]: Hornbeck H., Alim U. (2019) “[Improved Volume Scattering.](https://doi.org/10.1007/978-3-030-22514-8_6)” In: Gavrilova M., Chang J., Thalmann N., Hitzer E., Ishikawa H. (eds) Advances in Computer Graphics. CGI 2019. Lecture Notes in Computer Science, vol 11542. Springer, Cham

[^2]: Matusik, Wojciech. “[A Data-Driven Reflectance Model.](https://doi.org/10.1145/882262.882343)” Thesis, Massachusetts Institute of Technology, 2003. https://www.merl.com/brdf/

[^3]: Gkioulekas, Ioannis, Shuang Zhao, Kavita Bala, Todd Zickler, and Anat Levin. “[Inverse Volume Rendering with Material Dictionaries.](https://doi.org/10.1145/2508363.2508377)” ACM Trans. Graph. 32, no. 6 (November 2013): 162:1–162:13.

[^4]: Hess, M., P. Koepke, and I. Schult. “[Optical Properties of Aerosols and Clouds: The Software Package OPAC.](https://doi.org/10.1175/1520-0477%281998%29079%3C0831%3AOPOAAC%3E2.0.CO%3B2)” Bulletin of the American Meteorological Society 79, no. 5 (May 1, 1998): 831–44.

[^5]: Bouthors, Antoine, Fabrice Neyret, Nelson Max, Eric Bruneton, and Cyril Crassin. “[Interactive Multiple Anisotropic Scattering in Clouds,](https://doi.org/10.1145/1342250.1342277)” 173. ACM Press, 2008.

[^6]: Cornette, William M., and Joseph G. Shanks. “[Physically Reasonable Analytic Expression for the Single-Scattering Phase Function.](https://doi.org/10.1364/AO.31.003152)” Applied Optics 31, no. 16 (June 1, 1992): 3152–60.

[^7]: Cook, Robert L., and Kenneth E. Torrance. “[A Reflectance Model for Computer Graphics](https://www.cs.virginia.edu/~mjh7v/bib/Cook82.pdf).” In Proceedings of the 8th Annual Conference on Computer Graphics and Interactive Techniques, 307–316. SIGGRAPH ’81. New York, NY, USA: ACM, 1981. https://doi.org/10.1145/800224.806819.

[^8]: Heitz, Eric, Johannes Hanika, Eugene d’Eon, and Carsten Dachsbacher. “[Multiple-Scattering Microfacet BSDFs with the Smith Model](https://eheitzresearch.wordpress.com/240-2/).” ACM Transactions on Graphics (TOG) 35, no. 4 (2016): 58.

[^9]: Burley, Brent. “[Physically-Based Shading at Disney](https://disney-animation.s3.amazonaws.com/uploads/production/publication_asset/48/asset/s2012_pbs_disney_brdf_notes_v3.pdf),” 2012.
